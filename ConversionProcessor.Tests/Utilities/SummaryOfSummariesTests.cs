﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static ConversionProcessorWPF.Utilities.SummaryOfSummaries;

namespace ConversionProcessor.Tests.Utilities
{
    [TestClass]
    public class SummaryOfSummariesTests
    {
        [TestMethod]
        public void SyncSummaryLists_ShouldSyncLists()
        {
            var summaryLists = new List<SummaryItemList>();

            var list1 = new SummaryItemList
            {
                new SummaryItem() {Name = "A", Value = "A"},
                new SummaryItem() {Name = "B", Value = "B"},
                new SummaryItem() {Name = "C", Value = "C"}
            };

            var list2 = new SummaryItemList
            {
                new SummaryItem() {Name = "A", Value = "A"},
                new SummaryItem() {Name = "A", Value = "A"},
                new SummaryItem() {Name = "B", Value = "B"},
                new SummaryItem() {Name = "B", Value = "B"}
            };

            var list3 = new SummaryItemList
            {
                new SummaryItem() {Name = "D", Value = "D"},
            };

            summaryLists.Add(list1);
            summaryLists.Add(list2);
            summaryLists.Add(list3);

            SyncSummaryLists(summaryLists);

            Assert.AreEqual(6, list1.Count);
            Assert.AreEqual(6, list2.Count);
            Assert.AreEqual(6, list3.Count);

            Assert.IsTrue(list1[0].Name == "A" && list1[0].Value == "A");
            Assert.IsTrue(list1[1].Name == "A" && string.IsNullOrEmpty(list1[1].Value));
            Assert.IsTrue(list1[2].Name == "B" && list1[2].Value == "B");
            Assert.IsTrue(list1[3].Name == "B" && string.IsNullOrEmpty(list1[3].Value));
            Assert.IsTrue(list1[4].Name == "C" && list1[4].Value == "C");
            Assert.IsTrue(list1[5].Name == "D" && string.IsNullOrEmpty(list1[5].Value));

            Assert.IsTrue(list2[0].Name == "A" && list2[0].Value == "A");
            Assert.IsTrue(list2[1].Name == "A" && list2[1].Value == "A");
            Assert.IsTrue(list2[2].Name == "B" && list2[2].Value == "B");
            Assert.IsTrue(list2[3].Name == "B" && list2[3].Value == "B");
            Assert.IsTrue(list2[4].Name == "C" && string.IsNullOrEmpty(list2[4].Value));
            Assert.IsTrue(list2[5].Name == "D" && string.IsNullOrEmpty(list2[5].Value));

            Assert.IsTrue(list3[0].Name == "A" && string.IsNullOrEmpty(list3[0].Value));
            Assert.IsTrue(list3[1].Name == "A" && string.IsNullOrEmpty(list3[1].Value));
            Assert.IsTrue(list3[2].Name == "B" && string.IsNullOrEmpty(list3[2].Value));
            Assert.IsTrue(list3[3].Name == "B" && string.IsNullOrEmpty(list3[3].Value));
            Assert.IsTrue(list3[4].Name == "C" && string.IsNullOrEmpty(list3[4].Value));
            Assert.IsTrue(list3[5].Name == "D" && list3[5].Value == "D");
        }

        [TestMethod]
        public void OrganizeLists_ShouldMoveInsuranceItems_WhenFeeTotalExists()
        {
            var summaryLists = new List<SummaryItemList>();

            var list1 = new SummaryItemList
            {
                new SummaryItem() {Name = "Insurance Provider", Value = ""},
                new SummaryItem() {Name = "Insurance Integration #", Value = ""},
                new SummaryItem() {Name = "Fee Total", Value = ""}
            };

            var list2 = new SummaryItemList
            {
                new SummaryItem() {Name = "Insurance Provider", Value = ""},
                new SummaryItem() {Name = "Insurance Integration #", Value = ""},
                new SummaryItem() {Name = "AAA", Value = ""},
                new SummaryItem() {Name = "AAA", Value = ""},
                new SummaryItem() {Name = "Fee Total", Value = ""},
                new SummaryItem() {Name = "AAA", Value = ""},
                new SummaryItem() {Name = "AAA", Value = ""},
                new SummaryItem() {Name = "AAA", Value = ""},
            };

            summaryLists.Add(list1);
            summaryLists.Add(list2);

            OrganizeSummaryLists(summaryLists);

            Assert.AreEqual("Fee Total", list1[0].Name);
            Assert.AreEqual(null, list1[1].Name);
            Assert.AreEqual("Insurance Provider", list1[2].Name);
            Assert.AreEqual("Insurance Integration #", list1[3].Name);

            Assert.AreEqual("Fee Total", list2[2].Name);
            Assert.AreEqual(null, list2[3].Name);
            Assert.AreEqual("Insurance Provider", list2[4].Name);
            Assert.AreEqual("Insurance Integration #", list2[5].Name);
        }

        [TestMethod]
        public void OrganizeLists_ShouldAddBlankLineBeforeUnitType()
        {
            var summaryLists = new List<SummaryItemList>();

            var list1 = new SummaryItemList
            {
                new SummaryItem() {Name = "Fee Total", Value = ""},
                new SummaryItem() {Name = "AAA", Value = ""},
                new SummaryItem() {Name = "Unit Type", Value = ""}
            };
         
            summaryLists.Add(list1);

            OrganizeSummaryLists(summaryLists);

            Assert.AreEqual("Fee Total", list1[0].Name);
            Assert.AreEqual(null, list1[1].Name);
            Assert.AreEqual("AAA", list1[2].Name);
            Assert.AreEqual(null, list1[3].Name);
            Assert.AreEqual("Unit Type", list1[4].Name);
        }
    }
}

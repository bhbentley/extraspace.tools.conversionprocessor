﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ConversionDataValidator.Tests
{
    [TestClass]
    public class CLG_STAGE_APTests
    {
        [TestMethod]
        public void Account_Name_ShouldValidateValue()
        {
            var clg_stage_ap = new CLG_STAGE_AP();
            clg_stage_ap.ACCOUNT_NAME = "Test".PadRight(101, 'a');

            Assert.AreEqual(1, clg_stage_ap.InvalidProperties.Count());


        }
    }
}

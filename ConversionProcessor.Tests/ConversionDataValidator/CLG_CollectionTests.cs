﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ConversionProcessorWPF.Model.Tests
{
    [TestClass]
    public class CLG_CollectionTests
    {
        [TestMethod]
        public void ToDataTable_ShouldConvertToDataTable()
        {
            var op_rentals = new RentalCollection();

            op_rentals.Add(new Rental()
            {
                AccountId = 1000,
                CommonKey = "commonKey".PadRight(101, 'a'),
            });

            var dt = op_rentals.ToDataTable();

            Assert.AreEqual(1, dt.Rows.Count);

            var dr = dt.Rows[0];

            Assert.AreEqual(1000L, dr["ACCOUNT_ID"]);
            Assert.AreEqual("commonKey".PadRight(101, 'a'), dr["COMMONKEY"]);
        }

    

    }
}

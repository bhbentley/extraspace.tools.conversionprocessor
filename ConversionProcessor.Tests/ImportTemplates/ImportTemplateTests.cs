﻿using System.IO;
using System.Linq;
using ConversionProcessorWPF.ImportTemplates;
using ConversionProcessorWPF.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ConversionProcessor.Tests.ImportTemplates
{
    [TestClass]
    public class ImportTemplateTests
    {

        #region  ValidateUniqueRentalUnitKeyPerRental
        [TestMethod]
        public void ValidateUniqueRentalUnitKeyPerRental_ShouldReturnTrue_WhenNoDuplicates()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Rentals.Add(new Rental() { CommonKey = "1", RentalUnitKey = "2" });
            importTemplate.Rentals.Add(new Rental() { CommonKey = "1", RentalUnitKey = "3" });
            importTemplate.Rentals.Add(new Rental() { CommonKey = "1", RentalUnitKey = "4" });

            var result = importTemplate.ValidateUniqueRentalUnitKeyPerRental();

            Assert.IsTrue(result);
            Assert.AreEqual(3, importTemplate.Rentals.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        [TestMethod]
        public void ValidateUniqueRentalUnitKeyPerRental_ShouldReturnFalse_WhenDuplicatesExist()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Rentals.Add(new Rental() { CommonKey = "1", RentalUnitKey = "2" });
            importTemplate.Rentals.Add(new Rental() { CommonKey = "1", RentalUnitKey = "2" });
            importTemplate.Rentals.Add(new Rental() { CommonKey = "1", RentalUnitKey = "3" });

            var result = importTemplate.ValidateUniqueRentalUnitKeyPerRental();

            Assert.IsFalse(result);
            Assert.AreEqual(2, importTemplate.Rentals.Count());
            Assert.AreEqual(1, importTemplate.ImportErrors.Count());

            // Running it again should now return true.
            Assert.IsTrue(importTemplate.ValidateUniqueRentalUnitKeyPerRental());
        }

        [TestMethod]
        public void ValidateUniqueRentalUnitKeyPerRental_ShouldReturnFalse_WhenManyDuplicatesExist()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Rentals.Add(new Rental() { CommonKey = "1", RentalUnitKey = "2" });
            importTemplate.Rentals.Add(new Rental() { CommonKey = "2", RentalUnitKey = "2" });
            importTemplate.Rentals.Add(new Rental() { CommonKey = "3", RentalUnitKey = "2" });
            importTemplate.Rentals.Add(new Rental() { CommonKey = "4", RentalUnitKey = "2" });
            importTemplate.Rentals.Add(new Rental() { CommonKey = "5", RentalUnitKey = "2" });
            importTemplate.Rentals.Add(new Rental() { CommonKey = "6", RentalUnitKey = "2" });

            var result = importTemplate.ValidateUniqueRentalUnitKeyPerRental();

            Assert.IsFalse(result);
            Assert.AreEqual(1, importTemplate.Rentals.Count());
            Assert.AreEqual(5, importTemplate.ImportErrors.Count());

            Assert.AreEqual("1", importTemplate.Rentals.First().CommonKey);

            // Running it again should now return true.
            Assert.IsTrue(importTemplate.ValidateUniqueRentalUnitKeyPerRental());
        }

        [TestMethod]
        public void ValidateUniqueRentalUnitKeyPerRental_ShouldReturnTrue_WhenRentalsIsEmpty()
        {
            var importTemplate = new TestImportTemplate();

            var result = importTemplate.ValidateUniqueRentalUnitKeyPerRental();

            Assert.IsTrue(result);
            Assert.AreEqual(0, importTemplate.Rentals.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        #endregion

        #region ValidateNoDuplicateUnitNumbers

        [TestMethod]
        public void ValidateNoDuplicateUnitNumbers_ShouldReturnTrue_WhenNoDuplicates()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Units.Add(new Unit() { CommonKey = "1", RentalUnitKey = "2", UnitNumber = "A1" });
            importTemplate.Units.Add(new Unit() { CommonKey = "1", RentalUnitKey = "3", UnitNumber = "A2" });
            importTemplate.Units.Add(new Unit() { CommonKey = "1", RentalUnitKey = "4", UnitNumber = "A3" });

            var result = importTemplate.ValidateNoDuplicateUnitNumbers();

            Assert.IsTrue(result);
            Assert.AreEqual(3, importTemplate.Units.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        [TestMethod]
        public void ValidateNoDuplicateUnitNumbers_ShouldReturnFalse_WhenDuplicatesExist()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Units.Add(new Unit() { CommonKey = "1", RentalUnitKey = "2", UnitNumber = "A1" });
            importTemplate.Units.Add(new Unit() { CommonKey = "1", RentalUnitKey = "3", UnitNumber = "A1" });
            importTemplate.Units.Add(new Unit() { CommonKey = "1", RentalUnitKey = "4", UnitNumber = "A3" });

            var result = importTemplate.ValidateNoDuplicateUnitNumbers();

            Assert.IsFalse(result);
            Assert.AreEqual(2, importTemplate.Units.Count());
            Assert.AreEqual(1, importTemplate.ImportErrors.Count());

            // Running it again should now return true.
            Assert.IsTrue(importTemplate.ValidateNoDuplicateUnitNumbers());
        }

        [TestMethod]
        public void ValidateNoDuplicateUnitNumbers_ShouldReturnFalse_WhenManyDuplicatesExist()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Units.Add(new Unit() { CommonKey = "1", RentalUnitKey = "2", UnitNumber = "A1" });
            importTemplate.Units.Add(new Unit() { CommonKey = "1", RentalUnitKey = "3", UnitNumber = "A1" });
            importTemplate.Units.Add(new Unit() { CommonKey = "1", RentalUnitKey = "4", UnitNumber = "A1" });
            importTemplate.Units.Add(new Unit() { CommonKey = "1", RentalUnitKey = "5", UnitNumber = "A1" });
            importTemplate.Units.Add(new Unit() { CommonKey = "1", RentalUnitKey = "6", UnitNumber = "A1" });
            importTemplate.Units.Add(new Unit() { CommonKey = "1", RentalUnitKey = "7", UnitNumber = "A1" });

            var result = importTemplate.ValidateNoDuplicateUnitNumbers();

            Assert.IsFalse(result);
            Assert.AreEqual(1, importTemplate.Units.Count());
            Assert.AreEqual(5, importTemplate.ImportErrors.Count());

            Assert.AreEqual("2", importTemplate.Units.First().RentalUnitKey);

            // Running it again should now return true.
            Assert.IsTrue(importTemplate.ValidateNoDuplicateUnitNumbers());
        }

        [TestMethod]
        public void ValidateNoDuplicateUnitNumbers_ShouldReturnTrue_WhenUnitsIsEmpty()
        {
            var importTemplate = new TestImportTemplate();

            var result = importTemplate.ValidateNoDuplicateUnitNumbers();

            Assert.IsTrue(result);
            Assert.AreEqual(0, importTemplate.Units.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        #endregion

        #region ValidatePhonesWithNoContacts

        [TestMethod]
        public void ValidatePhonesWithNoContacts_ShouldReturnTrue_WhenPhonesHaveContacts()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Contacts.Add(new Contact() { ContactKey = "1" });
            importTemplate.Contacts.Add(new Contact() { ContactKey = "2" });
            importTemplate.Phones.Add(new Phone() { ContactKey = "1", PhoneNumber = "1" });
            importTemplate.Phones.Add(new Phone() { ContactKey = "1", PhoneNumber = "2" });
            importTemplate.Phones.Add(new Phone() { ContactKey = "2", PhoneNumber = "3" });

            var result = importTemplate.ValidatePhonesWithNoContacts();

            Assert.IsTrue(result);
            Assert.AreEqual(3, importTemplate.Phones.Count());
            Assert.AreEqual(2, importTemplate.Contacts.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        [TestMethod]
        public void ValidatePhonesWithNoContacts_ShouldReturnFalse_WhenPhonesHaveNoContacts()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Contacts.Add(new Contact() { ContactKey = "3" });
            importTemplate.Contacts.Add(new Contact() { ContactKey = "4" });
            importTemplate.Phones.Add(new Phone() { ContactKey = "1", PhoneNumber = "1" });
            importTemplate.Phones.Add(new Phone() { ContactKey = "1", PhoneNumber = "2" });
            importTemplate.Phones.Add(new Phone() { ContactKey = "2", PhoneNumber = "3" });

            var result = importTemplate.ValidatePhonesWithNoContacts();

            Assert.IsFalse(result);
            Assert.AreEqual(0, importTemplate.Phones.Count());
            Assert.AreEqual(2, importTemplate.Contacts.Count());
            Assert.AreEqual(3, importTemplate.ImportErrors.Count());

            // Running it again should now return true.
            Assert.IsTrue(importTemplate.ValidatePhonesWithNoContacts());
        }

        [TestMethod]
        public void ValidatePhonesWithNoContacts_ShouldReturnTrue_WhenPhonesIsEmpty()
        {
            var importTemplate = new TestImportTemplate();

            var result = importTemplate.ValidatePhonesWithNoContacts();

            Assert.IsTrue(result);
            Assert.AreEqual(0, importTemplate.Phones.Count());
            Assert.AreEqual(0, importTemplate.Contacts.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        #endregion

        #region ValidatePrimaryContactsWithNoPhones

        [TestMethod]
        public void ValidatePrimaryContactsWithNoPhones_ShouldReturnTrue_WhenPrimaryHasPhone()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Contacts.Add(new Contact() { ContactKey = "1", ContactType = 1 });
            importTemplate.Contacts.Add(new Contact() { ContactKey = "2", ContactType = 2 });
            importTemplate.Phones.Add(new Phone() { ContactKey = "1" });

            var result = importTemplate.ValidatePrimaryContactsWithNoPhones();

            Assert.IsTrue(result);
            Assert.AreEqual(1, importTemplate.Phones.Count());
            Assert.AreEqual(2, importTemplate.Contacts.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        [TestMethod]
        public void ValidatePrimaryContactsWithNoPhones_ShouldReturnFalse_WhenPrimaryHasNoPhone()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Contacts.Add(new Contact() { ContactKey = "1", ContactType = 1 });
            importTemplate.Contacts.Add(new Contact() { ContactKey = "2", ContactType = 2 });
            importTemplate.Phones.Add(new Phone() { ContactKey = "2" });

            var result = importTemplate.ValidatePrimaryContactsWithNoPhones();

            Assert.IsFalse(result);

            // One phone should have been added for the primary contact.
            Assert.AreEqual(2, importTemplate.Phones.Count());
            Assert.AreEqual(2, importTemplate.Contacts.Count());
            Assert.AreEqual(1, importTemplate.ImportErrors.Count());

            // Running it again should now return true.
            Assert.IsTrue(importTemplate.ValidatePrimaryContactsWithNoPhones());
        }

        [TestMethod]
        public void ValidatePrimaryContactsWithNoPhones_ShouldReturnTrue_WhenContactsIsEmpty()
        {
            var importTemplate = new TestImportTemplate();

            var result = importTemplate.ValidatePrimaryContactsWithNoPhones();

            Assert.IsTrue(result);
            Assert.AreEqual(0, importTemplate.Phones.Count());
            Assert.AreEqual(0, importTemplate.Contacts.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        [TestMethod]
        public void ValidatePrimaryContactsWithNoPhones_ShouldReturnTrue_WhenNoPrimaryContactsExist()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Contacts.Add(new Contact() { ContactKey = "2", ContactType = 2 });

            var result = importTemplate.ValidatePrimaryContactsWithNoPhones();

            Assert.IsTrue(result);
            Assert.AreEqual(0, importTemplate.Phones.Count());
            Assert.AreEqual(1, importTemplate.Contacts.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        #endregion

        #region ValidateAccountsWithNoRentals

        [TestMethod]
        public void ValidateAccountsWithNoRentals_ShouldReturnTrue_WhenAccountHasRentals()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Accounts.Add(new Account() { CommonKey = "1" });
            importTemplate.Rentals.Add(new Rental() { CommonKey = "1" });

            var result = importTemplate.ValidateAccountsWithNoRentals();

            Assert.IsTrue(result);
            Assert.AreEqual(1, importTemplate.Accounts.Count());
            Assert.AreEqual(1, importTemplate.Rentals.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        [TestMethod]
        public void ValidateAccountsWithNoRentals_ShouldReturnFalse_WhenAccountHasNoRentals()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Accounts.Add(new Account() { CommonKey = "1" });

            var result = importTemplate.ValidateAccountsWithNoRentals();

            Assert.IsFalse(result);

            // Account should have been removed.
            Assert.AreEqual(0, importTemplate.Accounts.Count());
            Assert.AreEqual(0, importTemplate.Rentals.Count());
            Assert.AreEqual(1, importTemplate.ImportErrors.Count());

            // Running it again should now return true.
            Assert.IsTrue(importTemplate.ValidateAccountsWithNoRentals());
        }

        [TestMethod]
        public void ValidateAccountsWithNoRentals_ShouldReturnTrue_WhenAccountsIsEmpty()
        {
            var importTemplate = new TestImportTemplate();

            var result = importTemplate.ValidateAccountsWithNoRentals();

            Assert.IsTrue(result);
            Assert.AreEqual(0, importTemplate.Accounts.Count());
            Assert.AreEqual(0, importTemplate.Rentals.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        #endregion

        #region ValidateRentalsWithNoAccount

        [TestMethod]
        public void ValidateRentalsWithNoAccount_ShouldReturnTrue_WhenRentalHasAccount()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Accounts.Add(new Account() { CommonKey = "1" });
            importTemplate.Rentals.Add(new Rental() { CommonKey = "1" });

            var result = importTemplate.ValidateRentalsWithNoAccount();

            Assert.IsTrue(result);
            Assert.AreEqual(1, importTemplate.Accounts.Count());
            Assert.AreEqual(1, importTemplate.Rentals.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        [TestMethod]
        public void ValidateRentalsWithNoAccount_ShouldReturnFalse_WhenRentalHasNoAccount()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Accounts.Add(new Account() { CommonKey = "1" });
            importTemplate.Rentals.Add(new Rental() { CommonKey = "1" });
            importTemplate.Rentals.Add(new Rental() { CommonKey = "2" });

            var result = importTemplate.ValidateRentalsWithNoAccount();

            Assert.IsFalse(result);

            // Rental should have been removed.
            Assert.AreEqual(1, importTemplate.Accounts.Count());
            Assert.AreEqual(1, importTemplate.Rentals.Count());
            Assert.AreEqual(1, importTemplate.ImportErrors.Count());

            // Running it again should now return true.
            Assert.IsTrue(importTemplate.ValidateRentalsWithNoAccount());
        }

        [TestMethod]
        public void ValidateRentalsWithNoAccount_ShouldReturnTrue_WhenRentalsIsEmpty()
        {
            var importTemplate = new TestImportTemplate();

            var result = importTemplate.ValidateRentalsWithNoAccount();

            Assert.IsTrue(result);
            Assert.AreEqual(0, importTemplate.Accounts.Count());
            Assert.AreEqual(0, importTemplate.Rentals.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        #endregion

        #region ValidateAddressesWithNoContacts

        [TestMethod]
        public void ValidateAddressesWithNoContacts_ShouldReturnTrue_WhenAddressHasAContact()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Contacts.Add(new Contact() { ContactKey = "1" });
            importTemplate.Addresses.Add(new Address() { ContactKey = "1" });

            var result = importTemplate.ValidateAddressesWithNoContacts();

            Assert.IsTrue(result);
            Assert.AreEqual(1, importTemplate.Addresses.Count());
            Assert.AreEqual(1, importTemplate.Contacts.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        [TestMethod]
        public void ValidateAddressesWithNoContacts_ShouldReturnFalse_WhenAddressHasNoContact()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Contacts.Add(new Contact() { ContactKey = "1" });
            importTemplate.Addresses.Add(new Address() { ContactKey = "1" });
            importTemplate.Addresses.Add(new Address() { ContactKey = "2" });

            var result = importTemplate.ValidateAddressesWithNoContacts();

            Assert.IsFalse(result);

            // Rental should have been removed.
            Assert.AreEqual(1, importTemplate.Addresses.Count());
            Assert.AreEqual(1, importTemplate.Contacts.Count());
            Assert.AreEqual(1, importTemplate.ImportErrors.Count());

            // Running it again should now return true.
            Assert.IsTrue(importTemplate.ValidateAddressesWithNoContacts());
        }

        [TestMethod]
        public void ValidateAddressesWithNoContacts_ShouldReturnTrue_WhenAddressesIsEmpty()
        {
            var importTemplate = new TestImportTemplate();

            var result = importTemplate.ValidateAddressesWithNoContacts();

            Assert.IsTrue(result);
            Assert.AreEqual(0, importTemplate.Addresses.Count());
            Assert.AreEqual(0, importTemplate.Contacts.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        #endregion

        #region ValidatePrimaryContactsWithNoAddresses

        [TestMethod]
        public void ValidatePrimaryContactsWithNoAddresses_ShouldReturnTrue_WhenPrimaryHasAddress()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Contacts.Add(new Contact() { ContactKey = "1", ContactType = 1 });
            importTemplate.Contacts.Add(new Contact() { ContactKey = "2", ContactType = 2 });
            importTemplate.Addresses.Add(new Address() { ContactKey = "1" });

            var result = importTemplate.ValidatePrimaryContactsWithNoAddresses();

            Assert.IsTrue(result);
            Assert.AreEqual(1, importTemplate.Addresses.Count());
            Assert.AreEqual(2, importTemplate.Contacts.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        [TestMethod]
        public void ValidatePrimaryContactsWithNoAddresses_ShouldReturnFalse_WhenPrimaryHasNoAddress()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Contacts.Add(new Contact() { ContactKey = "1", ContactType = 1 });
            importTemplate.Contacts.Add(new Contact() { ContactKey = "2", ContactType = 2 });
            importTemplate.Addresses.Add(new Address() { ContactKey = "2" });

            var result = importTemplate.ValidatePrimaryContactsWithNoAddresses();

            Assert.IsFalse(result);

            // One Address should have been added for the primary contact.
            Assert.AreEqual(2, importTemplate.Addresses.Count());
            Assert.AreEqual(2, importTemplate.Contacts.Count());
            Assert.AreEqual(1, importTemplate.ImportErrors.Count());

            // Running it again should now return true.
            Assert.IsTrue(importTemplate.ValidatePrimaryContactsWithNoAddresses());
        }

        [TestMethod]
        public void ValidatePrimaryContactsWithNoAddresses_ShouldReturnTrue_WhenContactsIsEmpty()
        {
            var importTemplate = new TestImportTemplate();

            var result = importTemplate.ValidatePrimaryContactsWithNoAddresses();

            Assert.IsTrue(result);
            Assert.AreEqual(0, importTemplate.Addresses.Count());
            Assert.AreEqual(0, importTemplate.Contacts.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        [TestMethod]
        public void ValidatePrimaryContactsWithNoAddresses_ShouldReturnTrue_WhenNoPrimaryContactsExist()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Contacts.Add(new Contact() { ContactKey = "2", ContactType = 2 });

            var result = importTemplate.ValidatePrimaryContactsWithNoAddresses();

            Assert.IsTrue(result);
            Assert.AreEqual(0, importTemplate.Addresses.Count());
            Assert.AreEqual(1, importTemplate.Contacts.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        #endregion

        #region ValidateContactWithNoAccounts

        [TestMethod]
        public void ValidateContactWithNoAccounts_ShouldReturnTrue_WhenContactHasAccount()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Accounts.Add(new Account() { CommonKey = "1" });
            importTemplate.Contacts.Add(new Contact() { CommonKey = "1" });

            var result = importTemplate.ValidateContactsWithNoAccount();

            Assert.IsTrue(result);
            Assert.AreEqual(1, importTemplate.Accounts.Count());
            Assert.AreEqual(1, importTemplate.Contacts.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        [TestMethod]
        public void ValidateContactWithNoAccounts_ShouldReturnFalse_WhenContactHasNoAccount()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Accounts.Add(new Account() { CommonKey = "1" });
            importTemplate.Contacts.Add(new Contact() { CommonKey = "1" });
            importTemplate.Contacts.Add(new Contact() { CommonKey = "2" });

            var result = importTemplate.ValidateContactsWithNoAccount();

            Assert.IsFalse(result);

            // One Contact should have been removed.
            Assert.AreEqual(1, importTemplate.Accounts.Count());
            Assert.AreEqual(1, importTemplate.Contacts.Count());
            Assert.AreEqual(1, importTemplate.ImportErrors.Count());

            // Running it again should now return true.
            Assert.IsTrue(importTemplate.ValidateContactsWithNoAccount());
        }

        [TestMethod]
        public void ValidateContactWithNoAccounts_ShouldReturnTrue_WhenContactsIsEmpty()
        {
            var importTemplate = new TestImportTemplate();

            var result = importTemplate.ValidateContactsWithNoAccount();

            Assert.IsTrue(result);
            Assert.AreEqual(0, importTemplate.Accounts.Count());
            Assert.AreEqual(0, importTemplate.Contacts.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        #endregion

        #region ValidateSinglePrimaryContact

        [TestMethod]
        public void ValidateSinglePrimaryContact_ShouldReturnTrue_WhenAccountHasOnePrimaryContact()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Accounts.Add(new Account() { CommonKey = "1" });
            importTemplate.Contacts.Add(new Contact() { CommonKey = "1", ContactType = 1 });
            importTemplate.Contacts.Add(new Contact() { CommonKey = "1", ContactType = 2 });

            var result = importTemplate.ValidateSinglePrimaryContact();

            Assert.IsTrue(result);
            Assert.AreEqual(1, importTemplate.Accounts.Count());
            Assert.AreEqual(2, importTemplate.Contacts.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        [TestMethod]
        public void ValidateSinglePrimaryContact_ShouldReturnFalse_WhenAccountHasMultiplePrimaryContacts()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Accounts.Add(new Account() { CommonKey = "1" });
            importTemplate.Contacts.Add(new Contact() { CommonKey = "1", ContactType = 1 });
            importTemplate.Contacts.Add(new Contact() { CommonKey = "1", ContactType = 1 });

            var result = importTemplate.ValidateSinglePrimaryContact();

            Assert.IsFalse(result);

            // One Contact should have been updated to be a secondary.
            Assert.AreEqual(1, importTemplate.Accounts.Count());
            Assert.AreEqual(2, importTemplate.Contacts.Count());
            Assert.AreEqual(1, importTemplate.Contacts.Count(c => c.ContactType == 1));
            Assert.AreEqual(1, importTemplate.ImportErrors.Count());

            // Running it again should now return true.
            Assert.IsTrue(importTemplate.ValidateSinglePrimaryContact());
        }

        [TestMethod]
        public void ValidateSinglePrimaryContact_ShouldReturnTrue_WhenContactsIsEmpty()
        {
            var importTemplate = new TestImportTemplate();

            var result = importTemplate.ValidateSinglePrimaryContact();

            Assert.IsTrue(result);
            Assert.AreEqual(0, importTemplate.Accounts.Count());
            Assert.AreEqual(0, importTemplate.Contacts.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        #endregion

        #region ValidateDuplicateContacts

        [TestMethod]
        public void ValidateDuplicateContacts_ShouldReturnTrue_WhenAccountHasNoDuplicateContacts()
        {
            var importTemplate = new TestImportTemplate();

            // Contact are considered duplicates if First and Last Names match.  The other properties are not considered here.
            importTemplate.Accounts.Add(new Account() { CommonKey = "1" });
            importTemplate.Contacts.Add(new Contact() { CommonKey = "1", ContactType = 1, FirstName = "FN1", LastName = "LN1", ContactKey = "1" });
            importTemplate.Contacts.Add(new Contact() { CommonKey = "1", ContactType = 2, FirstName = "FN2", LastName = "LN2", ContactKey = "2" });

            var result = importTemplate.ValidateDuplicateContacts();

            Assert.IsTrue(result);
            Assert.AreEqual(1, importTemplate.Accounts.Count());
            Assert.AreEqual(2, importTemplate.Contacts.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        [TestMethod]
        public void ValidateDuplicateContacts_ShouldReturnFalse_WhenAccountHasDuplicateContacts()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Accounts.Add(new Account() { CommonKey = "1" });
            importTemplate.Contacts.Add(new Contact() { CommonKey = "1", ContactType = 1, FirstName = "FN1", LastName = "LN1", ContactKey = "1" });
            importTemplate.Contacts.Add(new Contact() { CommonKey = "1", ContactType = 2, FirstName = "FN1", LastName = "LN1", ContactKey = "2" });
            importTemplate.Addresses.Add(new Address() { Line1 = "L1", ContactKey = "1" });
            importTemplate.Addresses.Add(new Address() { Line1 = "L1", ContactKey = "2" });
            importTemplate.Phones.Add(new Phone() { PhoneNumber = "P1", PhoneType = 1, ContactKey = "1" });
            importTemplate.Phones.Add(new Phone() { PhoneNumber = "P1", PhoneType = 1, ContactKey = "2" });

            var result = importTemplate.ValidateDuplicateContacts();

            Assert.IsFalse(result);

            // One Contact should have been removed.
            Assert.AreEqual(1, importTemplate.Accounts.Count());
            Assert.AreEqual(1, importTemplate.Contacts.Count());

            // The second address should have been removed.
            Assert.AreEqual(1, importTemplate.Addresses.Count());

            // The second phone should have been removed.
            Assert.AreEqual(1, importTemplate.Phones.Count());

            // There should be 1 log entries (for removing the contact)
            Assert.AreEqual(1, importTemplate.ImportErrors.Count());

            // Running it again should now return true.
            Assert.IsTrue(importTemplate.ValidateDuplicateContacts());
        }

        [TestMethod]
        public void ValidateDuplicateContacts_ShouldReturnTrue_WhenContactsIsEmpty()
        {
            var importTemplate = new TestImportTemplate();

            var result = importTemplate.ValidateDuplicateContacts();

            Assert.IsTrue(result);
            Assert.AreEqual(0, importTemplate.Accounts.Count());
            Assert.AreEqual(0, importTemplate.Contacts.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        #endregion

        #region ValidateNoDuplicateAccounts

        [TestMethod]
        public void ValidateNoDuplicateAccounts_ShouldReturnTrue_WhenNoDuplicates()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Accounts.Add(new Account() { CommonKey = "1"});
            importTemplate.Accounts.Add(new Account() { CommonKey = "2"});
            importTemplate.Accounts.Add(new Account() { CommonKey = "3"});

            var result = importTemplate.ValidateNoDuplicateAccounts();

            Assert.IsTrue(result);
            Assert.AreEqual(3, importTemplate.Accounts.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        [TestMethod]
        public void ValidateNoDuplicateAccounts_ShouldReturnFalse_WhenDuplicatesExist()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Accounts.Add(new Account() { CommonKey = "1"});
            importTemplate.Accounts.Add(new Account() { CommonKey = "1"});
            importTemplate.Accounts.Add(new Account() { CommonKey = "3"});

            var result = importTemplate.ValidateNoDuplicateAccounts();

            Assert.IsFalse(result);
            Assert.AreEqual(2, importTemplate.Accounts.Count());
            Assert.AreEqual(1, importTemplate.ImportErrors.Count());

            // Running it again should now return true.
            Assert.IsTrue(importTemplate.ValidateNoDuplicateAccounts());
        }

        [TestMethod]
        public void ValidateNoDuplicateAccounts_ShouldReturnFalse_WhenManyDuplicatesExist()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Accounts.Add(new Account() { CommonKey = "1", AccountName = "A1" });
            importTemplate.Accounts.Add(new Account() { CommonKey = "1", AccountName = "A2" });
            importTemplate.Accounts.Add(new Account() { CommonKey = "1", AccountName = "A3" });
            importTemplate.Accounts.Add(new Account() { CommonKey = "1", AccountName = "A4" });
            importTemplate.Accounts.Add(new Account() { CommonKey = "1", AccountName = "A5" });
            importTemplate.Accounts.Add(new Account() { CommonKey = "1", AccountName = "A6" });

            var result = importTemplate.ValidateNoDuplicateAccounts();

            Assert.IsFalse(result);
            Assert.AreEqual(1, importTemplate.Accounts.Count());
            Assert.AreEqual(5, importTemplate.ImportErrors.Count());

            Assert.AreEqual("A1", importTemplate.Accounts.First().AccountName);

            // Running it again should now return true.
            Assert.IsTrue(importTemplate.ValidateNoDuplicateAccounts());
        }

        [TestMethod]
        public void ValidateNoDuplicateAccounts_ShouldReturnTrue_WhenAccountsIsEmpty()
        {
            var importTemplate = new TestImportTemplate();

            var result = importTemplate.ValidateNoDuplicateAccounts();

            Assert.IsTrue(result);
            Assert.AreEqual(0, importTemplate.Accounts.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        #endregion

        #region ValidateAutoPaysWithNoAccount

        [TestMethod]
        public void ValidateAutoPaysWithNoAccount_ShouldReturnTrue_WhenAutoPayHasAccount()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Accounts.Add(new Account() { CommonKey = "1" });
            importTemplate.AutoPays.Add(new AutoPay() { CommonKey = "1" });

            var result = importTemplate.ValidateAutoPaysWithNoAccount();

            Assert.IsTrue(result);
            Assert.AreEqual(1, importTemplate.AutoPays.Count());
            Assert.AreEqual(1, importTemplate.Accounts.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        [TestMethod]
        public void ValidateAutoPaysWithNoAccount_ShouldReturnFalse_WhenAutoPayHasNoAccount()
        {
            var importTemplate = new TestImportTemplate();

            importTemplate.Accounts.Add(new Account() { CommonKey = "1" });
            importTemplate.AutoPays.Add(new AutoPay() { CommonKey = "1" });
            importTemplate.AutoPays.Add(new AutoPay() { CommonKey = "2" });

            var result = importTemplate.ValidateAutoPaysWithNoAccount();

            Assert.IsFalse(result);

            // One AutoPay should have been removed.
            Assert.AreEqual(1, importTemplate.AutoPays.Count());
            Assert.AreEqual(1, importTemplate.Accounts.Count());
            Assert.AreEqual(1, importTemplate.ImportErrors.Count());

            // Running it again should now return true.
            Assert.IsTrue(importTemplate.ValidateAutoPaysWithNoAccount());
        }

        [TestMethod]
        public void ValidateAutoPaysWithNoAccount_ShouldReturnTrue_WhenAutoPaysIsEmpty()
        {
            var importTemplate = new TestImportTemplate();

            var result = importTemplate.ValidateAutoPaysWithNoAccount();

            Assert.IsTrue(result);
            Assert.AreEqual(0, importTemplate.AutoPays.Count());
            Assert.AreEqual(0, importTemplate.Accounts.Count());
            Assert.AreEqual(0, importTemplate.ImportErrors.Count());
        }

        #endregion

        #region Helper Classes

        private class TestImportTemplate : ImportTemplate
        {
            public TestImportTemplate() : base(Path.GetFullPath("Test"), 1000000, "")
            {
            }

            protected override void LoadTemplateData()
            {
                // do nothing
            }

            protected override void ValidateTemplateData()
            {
                // Do nothing
            }
        }

        #endregion 
    }
}

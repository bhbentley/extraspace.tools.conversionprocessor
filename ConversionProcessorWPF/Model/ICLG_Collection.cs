﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace ConversionProcessorWPF.Model
{
    [ContractClass(typeof(ICLG_CollectionContracts))]
    public interface ICLG_Collection
    {
        string TableName { get; }
        string InsertStatement { get; }
        List<CLG_Object> GetItems();
    }

    [ContractClassFor(typeof(ICLG_Collection))]
    internal abstract class ICLG_CollectionContracts : ICLG_Collection
    {
        public string TableName { get { throw new NotImplementedException(); } }
        public string InsertStatement { get { throw new NotImplementedException(); } }
        public List<CLG_Object> GetItems()
        {
            Contract.Ensures(Contract.Result<List<CLG_Object>>() != null);
            throw new System.NotImplementedException();
        }
    }
}

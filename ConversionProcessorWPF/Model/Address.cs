using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ConversionProcessorWPF.Model.Validation;

namespace ConversionProcessorWPF.Model
{
    public class Address : CLG_Object
    {
        [DataColumn("ADDRESS_TYPE")]
        [Display(Name = "Address Type", Order = 2)]
        [SiphonRequired]
        [Range(1, 5, ErrorMessage = "Invalid Address Type.  1=Home 2=Office 3=Mailing 4=Shipping 5=Other")]
        public decimal? AddressType { get; set; }


        [DataColumn("LINE1")]
        [Display(Name = "Line1", Order = 3)]
        [SiphonRequired]
        [MaxLength(55, ErrorMessage = "Length must be less than 55.")]
        public string Line1
        {
            get { return string.IsNullOrWhiteSpace(_line1) ? "NEED ADDRESS" : _line1; }
            set { _line1 = value; }
        }
        private string _line1;


        [DataColumn("LINE2")]
        [Display(Name = "Line2", Order = 4)]
        [MaxLength(55, ErrorMessage = "Length must be less than 55.")]
        public string Line2 { get; set; }


        [DataColumn("LINE3")]
        [Display(Name = "Line3", Order = 5)]
        [MaxLength(55, ErrorMessage = "Length must be less than 55.")]
        public string Line3 { get; set; }


        [DataColumn("LINE4")]
        public string Line4 => null;

        [DataColumn("CITY")]
        [Display(Name = "City", Order = 7)]
        [SiphonRequired]
        [MaxLength(65, ErrorMessage = "Length must be less than 65.")]
        public string City
        {
            get { return string.IsNullOrWhiteSpace(_city) ? "NEED CITY" : _city; }
            set { _city = value; }
        }
        private string _city;


        [DataColumn("STATE")]
        [Display(Name = "State", Order = 8)]
        [SiphonRequired]
        [MaxLength(25, ErrorMessage = "Length must be less than 25.")]
        public string State
        {
            get { return string.IsNullOrWhiteSpace(_state) ? "NEED STATE" : _state; }
            set { _state = value; }
        }
        private string _state;


        [DataColumn("COUNTRY")]
        [Display(Name = "Country", Order = 9)]
        [MaxLength(35, ErrorMessage = "Length must be less than 35.")]
        public string Country { get; set; }


        [DataColumn("POSTAL_CODE")]
        [Display(Name = "Postal Code", Order = 10)]
        [SiphonRequired]
        [MaxLength(25, ErrorMessage = "Length must be less than 25.")]
        public string PostalCode
        {
            get { return string.IsNullOrWhiteSpace(_postalCode) ? "NEED POSTAL CODE" : _postalCode; }
            set { _postalCode = value; }
        }
        private string _postalCode;


        [DataColumn("CSTAGE")]
        public decimal? Cstage { get; set; }


        [DataColumn("SITE_ID")]
        [Display(Name = "Site Id", Order = 12)]
        [ReadOnly(true)]
        public decimal? SiteId { get; set; }


        [DataColumn("PARENT_ID")]
        public long? ParentId => null;


        [DataColumn("CONTACT_KEY")]
        [Display(Name = "Contact Key", Order = 0)]
        [ReadOnly(true)]
        [SiphonRequired]
        public string ContactKey { get; set; }
    }
}
namespace ConversionProcessorWPF.Model
{
    public class AddressCollection : CLG_Collection<Address>
    {
        public override string TableName => "APPL.CLG_STAGE_OP_ADDRESSES";
        public override string InsertStatement => $@"INSERT INTO {TableName}
                                                     VALUES(act.act_addr_seq.nextval, :ADDRESS_TYPE, :LINE1, :LINE2, :LINE3, :LINE4, :CITY, :STATE, :COUNTRY, :POSTAL_CODE, :CSTAGE, :SITE_ID, :PARENT_ID, :CONTACT_KEY)";
    }
}
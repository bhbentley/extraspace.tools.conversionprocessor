namespace ConversionProcessorWPF.Model
{
    public class NoteCollection : CLG_Collection<Note>
    {
        public override string TableName => "APPL.CLG_STAGE_OP_NOTES";
        public override string InsertStatement => $@"INSERT INTO {TableName}
                                                     VALUES(act.act_notes_seq.nextval, :ACCT_ID, :SYS_NOTE, :NOTE, :FLAGGED, :SITE_ID, :FLAG_EXP, :COMMONKEY, :CSTAGE)";
    }
}
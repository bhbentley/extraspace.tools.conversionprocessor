using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ConversionProcessorWPF.Model.Validation;

namespace ConversionProcessorWPF.Model
{
    public class Phone : CLG_Object
    {
        [DataColumn("PHONE_TYPE")]
        [Display(Name = "Phone Type", Order = 2)]
        [Range(1, 5, ErrorMessage = "Invalid Phone Type. 1=Home 2=Office 3=Mobile 4=Fax 5=Other")]
        public decimal? PhoneType { get; set; }

        [DataColumn("PHONE_NUMBER")]
        [Display(Name = "Phone Number", Order = 3)]
        [MaxLength(55, ErrorMessage = "Length must be less than 55.")]
        public string PhoneNumber
        {
            get { return string.IsNullOrWhiteSpace(_phoneNumber) ? "NEED PHONE" : _phoneNumber; }
            set { _phoneNumber = value; }
        }
        private string _phoneNumber;

        [DataColumn("UNIT_NUMBER")]
        public string UnitNumber => null;

        [DataColumn("CSTAGE")]
        public decimal? Cstage { get; set; }

        [DataColumn("SITE_ID")]
        [Display(Name = "Site Id", Order = 6)]
        [SiphonRequired]
        public decimal? SiteId { get; set; }

        [DataColumn("PARENT_ID")]
        public long? ParentId => null;

        [DataColumn("CONTACT_KEY")]
        [Display(Name = "Contact Key", Order = 0)]
        [ReadOnly(true)]
        [SiphonRequired]
        public string ContactKey { get; set; }
    }
}
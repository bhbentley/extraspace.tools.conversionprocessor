namespace ConversionProcessorWPF.Model
{
    public class AutoPayCollection : CLG_Collection<AutoPay>
    {
        public override string TableName => "APPL.CLG_STAGE_AP";
        public override string InsertStatement => @"INSERT INTO appl.clg_stage_ap 
                                                    VALUES(tran.tran_ap_data_seq.nextval, :ACCOUNT_NAME, :NAME_ON_CARD, :STREET_ADDR, :CITY_ADDR, :STATE_ADDR,
                                                           :POSTAL_ADDR, :CARD_NO, :CARD_EXP, :ACCT_ID, :RENTAL_ID, :COMMONKEY, 
                                                           :SITE_ID, :CSTAGE, :ACTIVE)";
    }
}
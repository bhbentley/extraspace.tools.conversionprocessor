using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ConversionProcessorWPF.Model.Validation;

namespace ConversionProcessorWPF.Model
{
    public class Unit : CLG_Object
    {

        [DataColumn("DIMENSIONS")]
        public string Dimensions => $"{Width}x{Depth}";

        [Display(Name = "Width", Order = 1)]
        [SiphonRequired]
        public decimal? Width
        {
            get { return _width; }
            set { _width = Math.Truncate(value ?? 0); }
        }
        private decimal? _width;

        [Display(Name = "Depth", Order = 2)]
        [SiphonRequired]
        public decimal? Depth
        {
            get { return _depth; }
            set { _depth = Math.Truncate(value ?? 0); }
        }
        private decimal? _depth;


        [DataColumn("RENT_RATE")]
        [Display(Name = "Rent Rate", Order = 3)]
        [SiphonRequired]
        public decimal? RentRate { get; set; }


        [DataColumn("UNIT_NUMBER")]
        [Display(Name = "Unit Number", Order = 4)]
        [SiphonRequired]
        [MaxLength(20, ErrorMessage = "Length must be less than 20.")]
        public string UnitNumber { get; set; }


        [DataColumn("ATTRIBUTES")]
        [Display(Name = "Attributes", Order = 5)]
        [ReadOnly(true)]
        public decimal? Attributes { get; set; } = 1;


        [DataColumn("ATTRIBUTE_MEANING")]
        public string AttributeMeaning => null;


        [DataColumn("COMMONKEY")]
        [Display(Name = "CommonKey", Order = 0)]
        [ReadOnly(true)]
        public string CommonKey { get; set; }


        [DataColumn("CSTAGE")]
        public decimal? Cstage { get; set; }


        [DataColumn("STREET_RATE")]
        [Display(Name = "Street Rate", Order = 9)]
        [SiphonRequired]
        public decimal? StreetRate { get; set; }


        [DataColumn("RENTAL_UNIT_KEY")]
        [Display(Name = "Rental Unit Key", Order = 0)]
        [SiphonRequired]
        [MaxLength(100, ErrorMessage = "Length must be less than 100.")]
        public string RentalUnitKey { get; set; }


        [DataColumn("RATE_EFF_DATE")]
        [Display(Name = "Rate Eff Date", Order = 11)]
        [SiphonRequired]
        public DateTime? RateEffDate { get; set; }


        [DataColumn("FUTURE_RATE")]
        public decimal? FutureRate => null;


        [DataColumn("FUTURE_DATE")]
        public DateTime? FutureDate => null;


        [DataColumn("ATTRIBUTE2")]
        [Display(Name = "Attribute2", Order = 14)]
        public long? Attribute2 { get; set; }


        [DataColumn("ATTRIBUTE2_MEANING")]
        public string Attribute2Meaning => null;

        [DataColumn("DOOR")]
        [Display(Name = "Door", Order = 16)]
        public long? Door { get; set; }


        [DataColumn("DOOR_MEANING")]
        public string DoorMeaning => null;


        [DataColumn("AKSESS")]
        [Display(Name = "Access", Order = 18)]
        public long? Access { get; set; }


        [DataColumn("AKSESS_MEANING")]
        public string AksessMeaning => null;


        [DataColumn("CLIMATE")]
        public long? Climate { get; set; }


        [DataColumn("CLIMATE_MEANING")]
        public string ClimateMeaning => null;


        [DataColumn("MAPX")]
        public long? Mapx { get; set; }


        [DataColumn("MAPY")]
        public long? Mapy => null;


        [DataColumn("BUILDING_LABEL")]
        public string BuildingLabel => null;


        [DataColumn("FLOOR")]
        public string Floor => null;


        [DataColumn("BUILDING_ID")]
        public long? BuildingId => null;


        [DataColumn("STATUS")]
        [Display(Name = "Status", Order = 27)]
        [RegularExpression(@"^([1-5]|[8-9])$", ErrorMessage = "Invalid Status.  1=Available 2=Reserved 3=Rented 4=Company Use 5=Damaged 8=Other 9=Unavailable")]
        public short? Status { get; set; }


        [DataColumn("UNIT_MAP_ORIENTATION")]
        public string UnitMapOrientation => null;


        [DataColumn("REV_CLASS_ID")]
        [Display(Name = "Rev Class Id", Order = 29)]
        [MaxLength(100, ErrorMessage = "Length must be less than 100.")]
        public string RevClassId { get; set; }
    }
}
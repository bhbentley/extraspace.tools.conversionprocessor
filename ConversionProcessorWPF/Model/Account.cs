using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ConversionProcessorWPF.Model.Validation;

namespace ConversionProcessorWPF.Model
{
    public class Account : CLG_Object
    {
        [DataColumn("ACCOUNT_NAME")]
        [Display(Name = "Account Name", Order = 2)]
        [SiphonRequired]
        [MaxLength(250, ErrorMessage = "Length must be less than 250.")]
        public string AccountName
        {
            get { return string.IsNullOrWhiteSpace(_accountName) ? "NEED ACCOUNT NAME" : _accountName; }
            set { _accountName = value; }
        }
        private string _accountName;


        [DataColumn("ACCOUNT_CLASS")]
        [Display(Name = "Account Class", Order = 3)]
        [SiphonRequired]
        [Range(1, 2, ErrorMessage = "Invalid Account Class.  1=Personal 2=Business")]
        public decimal? AccountClass { get; set; }


        [DataColumn("ACCOUNT_STATUS")]
        [Display(Name = "Account Status", Order = 4)]
        [ReadOnly(true)]
        public decimal? AccountStatus => 1;


        [DataColumn("ACCOUNT_TYPE")]
        public decimal? AccountType => 1;


        [DataColumn("BILL_TO_ADDRESS")]
        public decimal? BillToAddress => null;


        [DataColumn("BILLING_DATE")]
        public decimal? BillingDate => null;

        [DataColumn("GATE_PIN")]
        [Display(Name = "Gate Pin", Order = 8)]
        [MaxLength(10, ErrorMessage = "Length must be less than 10.")]
        public string GatePin { get; set; }


        [DataColumn("SITE_ID")]
        public decimal? SiteId { get; set; }


        [DataColumn("SEND_INVOICE")]
        public bool? SendInvoice => null;


        [DataColumn("GATE24HR")]
        [Display(Name = "Gate24hr", Order = 11)]
        public bool? Gate24hr { get; set; }


        [DataColumn("GATE_TIME_ZONE")]
        [Display(Name = "Gate Time Zone", Order = 12)]
        [MaxLength(2, ErrorMessage = "Length must be less than 2.")]
        public string GateTimeZone { get; set; }


        [DataColumn("GATE_KEYPAD_ZONE")]
        [Display(Name = "Gate Keypad Zone", Order = 13)]
        [MaxLength(2, ErrorMessage = "Length must be less than 2.")]
        public string GateKeypadZone { get; set; }


        [DataColumn("COMMONKEY")]
        [Display(Name = "CommonKey", Order = 0)]
        [SiphonRequired]
        public string CommonKey { get; set; }


        [DataColumn("CSTAGE")]
        public decimal? Cstage { get; set; }
    }
}
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ConversionProcessorWPF.Model.Validation;

namespace ConversionProcessorWPF.Model
{
    public class AutoPay : CLG_Object
    {

        [DataColumn("ACCOUNT_NAME")]
        [Display(Name = "Account Name", Order = 2)]
        [MaxLength(100, ErrorMessage = "Length must be less than 100.")]
        public string AccountName { get; set; }


        [DataColumn("NAME_ON_CARD")]
        [Display(Name = "Name On Card", Order = 3)]
        [MaxLength(50, ErrorMessage = "Length must be less than 50.")]
        public string NameOnCard { get; set; }


        [DataColumn("STREET_ADDR")]
        [Display(Name = "Street Addr", Order = 4)]
        [MaxLength(50, ErrorMessage = "Length must be less than 50.")]
        public string StreetAddr { get; set; }


        [DataColumn("CITY_ADDR")]
        [Display(Name = "City Addr", Order = 5)]
        [MaxLength(50, ErrorMessage = "Length must be less than 50.")]
        public string CityAddr { get; set; }


        [DataColumn("STATE_ADDR")]
        [Display(Name = "State Addr", Order = 6)]
        [MaxLength(50, ErrorMessage = "Length must be less than 50.")]
        public string StateAddr { get; set; }


        [DataColumn("POSTAL_ADDR")]
        [Display(Name = "Postal Addr", Order = 7)]
        [MaxLength(25, ErrorMessage = "Length must be less than 25.")]
        public string PostalAddr { get; set; }


        [DataColumn("CARD_NO")]
        [Display(Name = "Card No", Order = 8)]
        [SiphonRequired]
        [MaxLength(100, ErrorMessage = "Length must be less than 100.")]
        public string CardNo { get; set; }


        [DataColumn("CARD_EXP")]
        [Display(Name = "Card Exp", Order = 9)]
        [SiphonRequired]
        [MaxLength(25, ErrorMessage = "Length must be less than 25.")]
        [RegularExpression(@"^(\d\d\/\d\d)$", ErrorMessage = "Invalid format.  Must be mm/yy.")]
        public string CardExp
        {
            get { return _cardExp; }
            set
            {
                if (!string.IsNullOrWhiteSpace(value) && value.Length == 4 && !value.Contains("/"))
                {
                    _cardExp = value.Insert(2, "/");
                }
                else
                {
                    _cardExp = value;
                }
            }
        }
        private string _cardExp;


        [DataColumn("ACCT_ID")]
        public long? AcctId => null;


        [DataColumn("RENTAL_ID")]
        public long? RentalId => null;


        [DataColumn("COMMONKEY")]
        [Display(Name = "CommonKey", Order = 0)]
        [ReadOnly(true)]
        [SiphonRequired(ErrorMessage = "No Account Found.")]
        public string CommonKey { get; set; }


        [DataColumn("SITE_ID")]
        [Display(Name = "Site Id", Order = 13)]
        [SiphonRequired]
        public long? SiteId { get; set; }


        [DataColumn("CSTAGE")]
        public decimal? Cstage { get; set; }


        [DataColumn("ACTIVE")]
        public bool? Active => true;
    }
}
namespace ConversionProcessorWPF.Model
{
    public class ServiceCollection : CLG_Collection<Service>
    {
        public override string TableName => "APPL.CLG_STAGE_ORG_SITE_SRVC_OBJ";
        public override string InsertStatement => $@"INSERT INTO {TableName}
                                                     VALUES(:SERVICE_OBJECT_ID, :SERVICE_CLASS_ID, :TAX_GROUP_ID, :LEASE_GROUP_ID, :TANGIBLE, :SERVICE_NAME, :PRICE, :SERVICE_DESC, :ACTIVE, :PART_REFUND, :SERVICE_TYPE, :CSTAGE)";
    }
}
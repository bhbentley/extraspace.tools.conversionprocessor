using System.ComponentModel.DataAnnotations;
using ConversionProcessorWPF.Model.Validation;

namespace ConversionProcessorWPF.Model
{
    public class Service : CLG_Object
    {
        [DataColumn("SERVICE_OBJECT_ID")]
        [Display(Name = "Service Object Id", Order = 1)] 
        [SiphonRequired]
        [MaxLength(100, ErrorMessage="Length must be less than 100.")]
        public string ServiceObjectId { get; set; }


        [DataColumn("SERVICE_CLASS_ID")]
        public long? ServiceClassId => null;


        [DataColumn("TAX_GROUP_ID")]
        [Display(Name = "Tax Group Id", Order = 3)] 
        public long? TaxGroupId { get; set; }


        [DataColumn("LEASE_GROUP_ID")]
        public long? LeaseGroupId => null;


        [DataColumn("TANGIBLE")]
        public bool? Tangible => false;


        [DataColumn("SERVICE_NAME")]
        [Display(Name = "Service Name", Order = 6)] 
        [SiphonRequired]
        [MaxLength(50, ErrorMessage="Length must be less than 50.")]
        public string ServiceName { get; set; }


        [DataColumn("PRICE")]
        [Display(Name = "Price", Order = 7)] 
        [SiphonRequired]
        public decimal? Price { get; set; }


        [DataColumn("SERVICE_DESC")]
        [Display(Name = "Service Desc", Order = 8)] 
        [SiphonRequired]
        [MaxLength(1000, ErrorMessage="Length must be less than 1000.")]
        public string ServiceDesc { get; set; }


        [DataColumn("ACTIVE")]
        [Display(Name = "Active", Order = 9)] 
        [SiphonRequired]
        public bool? Active { get; set; }


        [DataColumn("PART_REFUND")]
        [Display(Name = "Part Refund", Order = 10)] 
        [SiphonRequired]
        public bool? PartRefund { get; set; }


        [DataColumn("SERVICE_TYPE")]
        [Display(Name = "Service Type", Order = 11)] 
        [SiphonRequired]
        [Range(1, 4, ErrorMessage = "Invalid Service Type.  1=Mail Invoicing 2=24 Hour Access 3=Other Service/Amenity 4=EMail Invoicing")]
        public short? ServiceType { get; set; }


        [DataColumn("CSTAGE")]
        public decimal? Cstage { get; set; }
    }
}
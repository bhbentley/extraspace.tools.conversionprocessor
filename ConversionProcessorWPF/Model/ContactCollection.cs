namespace ConversionProcessorWPF.Model
{
    public class ContactCollection : CLG_Collection<Contact>
    {
        public override string TableName => "APPL.CLG_STAGE_OP_CONTACTS";
        public override string InsertStatement => $@"INSERT INTO {TableName}
                                                     VALUES(act.act_contacts_seq.nextval, :FIRST_NAME, :LAST_NAME, :KNOWN_AS, :CONTACT_TYPE, :UNIT_ACCESS, :EMPLOYER, :ACTIVE, :EMAIL_ADDRESS, :DRIVERS_LICENSE_NO
                                                           ,:DRIVERS_LICENSE_STATE, :DATE_OF_BIRTH, :SSN, :DRIVERS_LICENSE_EXPIRES, :MIDDLE_NAME, :COMMONKEY, :CSTAGE, :PARENT_ID, :CONTACT_KEY, :PASSWORD)";
    }
}
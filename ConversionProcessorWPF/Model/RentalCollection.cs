namespace ConversionProcessorWPF.Model
{
    public class RentalCollection : CLG_Collection<Rental>
    {
        public override string TableName => "APPL.CLG_STAGE_OP_RENTALS";
        public override string InsertStatement => $@"INSERT INTO {TableName}
                                                     VALUES(tran.tran_rentals_seq.nextval, :RENTAL_START_DATE, :RENTAL_FINISH_DATE, :RESERVATION_DATE, :STATUS, :INSURANCE, :PAID_THRU_DATE, :SECURITY_DEPOSIT,
                                                           :INVOICED, :FEES_BALANCE, :CREDITS_BALANCE, :COMMONKEY, :AUTOPAY_INFO, :CSTAGE, :UNIT_ID, :ACCOUNT_ID, :INSURANCE_START_DATE, :RENTAL_UNIT_KEY,
                                                           :LEASE_THRU_DATE, :OVERLOCK_FLAG, :SERVICE_ID, :EMAIL_SERV_ID)";
    }
}
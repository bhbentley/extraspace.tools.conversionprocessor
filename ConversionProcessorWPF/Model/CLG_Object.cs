﻿using System;
using System.ComponentModel;
using ConversionProcessorWPF.Model.Validation;
using PropertyChanged;

namespace ConversionProcessorWPF.Model
{
    [ImplementPropertyChanged]
    public abstract class CLG_Object : ValidationBase, IDataErrorInfo
    {
        public bool Filtered { get; set; }

        #region IDataErrorInfo Members

        public string Error => null;

        public string this[string columnName]
        {
            get
            {
                ValidateProperty(columnName);
                return string.Empty;
            }
        }

        #endregion
    }
}

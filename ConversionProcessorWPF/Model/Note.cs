using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ConversionProcessorWPF.Model.Validation;

namespace ConversionProcessorWPF.Model
{
    public class Note : CLG_Object
    {
        [DataColumn("ACCT_ID")]
        public long? AcctId => null;

        [DataColumn("SYS_NOTE")]
        public string SysNote => null;

        [DataColumn("NOTE")]
        [Display(Name = "Note", Order = 4)] 
        [SiphonRequired]
        [MaxLength(2500, ErrorMessage="Length must be less than 2500.")]
        public string Text { get; set; }

        [DataColumn("FLAGGED")]
        public bool? Flagged { get; set; }

        [DataColumn("SITE_ID")]
        [Display(Name = "Site Id", Order = 7)] 
        [SiphonRequired]
        public long? SiteId { get; set; }

        [DataColumn("FLAG_EXP")]
        [Display(Name = "Flag Exp", Order = 6)]
        public DateTime? FlagExp { get; set; }

        [DataColumn("COMMONKEY")]
        [Display(Name = "CommonKey", Order = 0)]
        [ReadOnly(true)]
        [SiphonRequired(ErrorMessage = "No Account Found." )]
        public string CommonKey { get; set; }

        [DataColumn("CSTAGE")]
        public decimal? Cstage { get; set; }
    }
}
namespace ConversionProcessorWPF.Model
{
    public class PhoneCollection : CLG_Collection<Phone>
    {
        public override string TableName => "APPL.CLG_STAGE_OP_PHONES";
        public override string InsertStatement => $@"INSERT INTO {TableName}
                                                     VALUES(act.act_phones_seq.nextval, :PHONE_TYPE, :PHONE_NUMBER, :UNIT_NUMBER, :CSTAGE, :SITE_ID, :PARENT_ID, :CONTACT_KEY)";
    }
}
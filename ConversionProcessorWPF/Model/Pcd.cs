using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ConversionProcessorWPF.Model
{
    public class Pcd : CLG_Object
    {
        [DataColumn("RENTAL_ID")]
        [Display(Name = "Rental Id", Order = 2)] 
        public long? RentalId { get; set; }


        [DataColumn("GIVEN_DATE")]
        [Display(Name = "Given Date", Order = 3)] 
        public DateTime? GivenDate { get; set; }


        [DataColumn("EFF_DATE")]
        [Display(Name = "Eff Date", Order = 4)] 
        public DateTime? EffDate { get; set; }


        [DataColumn("END_DATE")]
        [Display(Name = "End Date", Order = 5)] 
        public DateTime? EndDate { get; set; }


        [DataColumn("STATUS")]
        [Display(Name = "Status", Order = 6)] 
        public short? Status { get; set; }


        [DataColumn("DISC_AMT")]
        [Display(Name = "Disc Amt", Order = 7)] 
        public decimal? DiscAmt { get; set; }


        [DataColumn("BALANCE")]
        [Display(Name = "Balance", Order = 8)] 
        public decimal? Balance { get; set; }


        [DataColumn("REASON_CODE")]
        [Display(Name = "Reason Code", Order = 9)] 
        public short? ReasonCode { get; set; }


        [DataColumn("REASON_DESC")]
        [Display(Name = "Reason Desc", Order = 10)] 
        [MaxLength(250, ErrorMessage="Length must be less than 250.")]
        public string ReasonDesc { get; set; }


        [DataColumn("REV_CAT")]
        [Display(Name = "Rev Cat", Order = 11)] 
        public short? RevCat { get; set; }


        [DataColumn("REFILLS")]
        [Display(Name = "Refills", Order = 12)] 
        public int? Refills { get; set; }


        [DataColumn("DISC_PERC")]
        [Display(Name = "Disc Perc", Order = 13)] 
        public decimal? DiscPerc { get; set; }


        [DataColumn("DISC_AMT_TYPE")]
        [Display(Name = "Disc Amt Type", Order = 14)] 
        public short? DiscAmtType { get; set; }


        [DataColumn("ROLLOVER")]
        [Display(Name = "Rollover", Order = 15)] 
        public bool? Rollover { get; set; }


        [DataColumn("CSTAGE")]
        public decimal? Cstage { get; set; }


        [DataColumn("COMMONKEY")]
        [Display(Name = "CommonKey", Order = 0)]
        [ReadOnly(true)]
        public string CommonKey { get; set; }


        [DataColumn("RENTAL_UNIT_KEY")]
        [Display(Name = "Rental Unit Key", Order = 0)] 
        [MaxLength(100, ErrorMessage="Length must be less than 100.")]
        public string RentalUnitKey { get; set; }
    }
}
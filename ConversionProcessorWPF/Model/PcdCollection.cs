namespace ConversionProcessorWPF.Model
{
    public class PcdCollection : CLG_Collection<Pcd>
    {
        public override string TableName => "APPL.CLG_STAGE_OP_PCD";
        public override string InsertStatement => $@"INSERT INTO {TableName}
                                                     VALUES(tran.tran_disc_master_seq.nextval, :RENTAL_ID, :GIVEN_DATE, :EFF_DATE, :END_DATE, :STATUS, :DISC_AMT, :BALANCE, :REASON_CODE,
                                                            :REASON_DESC, :REV_CAT, :REFILLS, :DISC_PERC, :DISC_AMT_TYPE, :ROLLOVER, :CSTAGE, :COMMONKEY, :RENTAL_UNIT_KEY)";
    }
}
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ConversionProcessorWPF.Model.Validation;

namespace ConversionProcessorWPF.Model
{
    public class Rental : CLG_Object
    {
        [DataColumn("RENTAL_START_DATE")]
        [Display(Name = "Rental Start Date", Order = 2)] 
        [SiphonRequired]
        [LessThanOrEqualToToday]
        [LessThan("RentalFinishDate")]
        public DateTime? RentalStartDate
        {
            get
            {
                return _rentalStartDate;
            }
            set
            {
                _rentalStartDate = value;
               
                ValidateProperty("RentalFinishDate");
            }
        }
        private DateTime? _rentalStartDate; 
       

        [DataColumn("RENTAL_FINISH_DATE")]
        [Display(Name = "Rental Finish Date", Order = 3)]
        [GreaterThan("RentalStartDate")]
        public DateTime? RentalFinishDate
        {
            get
            {
                return _rentalFinishDate;
            }
            set
            {
                _rentalFinishDate = value;
               
                ValidateProperty("RentalStartDate");
            }
        }
        private DateTime? _rentalFinishDate; 
       

        [DataColumn("RESERVATION_DATE")]
        [Display(Name = "Reservation Date", Order = 4)] 
        public DateTime? ReservationDate { get; set; }


        [DataColumn("STATUS")]
        public string Status => IsReserved ? "R" : DelinquencyStep.ToString();


        [DataColumn("INSURANCE")]
        [Display(Name = "Insurance", Order = 16)] 
        public decimal? Insurance { get; set; }


        [DataColumn("PAID_THRU_DATE")]
        [Display(Name = "Paid Thru Date", Order = 7)] 
        [SiphonRequired]
        [LessThanMaxPtd] 
        public DateTime? PaidThruDate { get; set; }


        [DataColumn("SECURITY_DEPOSIT")]
        [Display(Name = "Security Deposit", Order = 8)] 
        public decimal? SecurityDeposit { get; set; }

        [DataColumn("INVOICED")]
        public short? Invoiced => null;


        [DataColumn("FEES_BALANCE")]
        [Display(Name = "Fees Balance", Order = 10)] 
        public decimal? FeesBalance { get; set; }


        [DataColumn("CREDITS_BALANCE")]
        [Display(Name = "Credits Balance", Order = 11)] 
        public decimal? CreditsBalance { get; set; }

        [DataColumn("COMMONKEY")]
        [Display(Name = "CommonKey", Order = 0)]
        [ReadOnly(true)]
        [SiphonRequired(ErrorMessage = "No Account Found." )]
        public string CommonKey { get; set; }


        [DataColumn("AUTOPAY_INFO")]
        public string AutopayInfo => null;


        [DataColumn("CSTAGE")]
        public decimal? Cstage { get; set; }


        [DataColumn("UNIT_ID")]
        public long? UnitId { get; set; }


        [DataColumn("ACCOUNT_ID")]
        public long? AccountId { get; set; }


        [DataColumn("INSURANCE_START_DATE")]
        [Display(Name = "Insurance Start Date", Order = 17)] 
        public DateTime? InsuranceStartDate { get; set; }


        [DataColumn("RENTAL_UNIT_KEY")]
        [Display(Name = "Rental Unit Key", Order = 0)] 
        [SiphonRequired]
        [MaxLength(100, ErrorMessage="Length must be less than 100.")]
        public string RentalUnitKey { get; set; }


        [DataColumn("LEASE_THRU_DATE")]
        public DateTime? LeaseThruDate => null;

        [DataColumn("OVERLOCK_FLAG")]
        public bool? OverlockFlag => false;


        [DataColumn("SERVICE_ID")]
        [Display(Name = "Service Id", Order = 21)] 
        [MaxLength(100, ErrorMessage="Length must be less than 100.")]
        public string ServiceId { get; set; }


        [DataColumn("EMAIL_SERV_ID")]
        public string EmailServId => null;

        [Display(Name = "Reserved", Order = 5)]
        [ReadOnly(true)]
        public bool IsReserved { get; set; }

        [Display(Name="Del Step", Order = 6)]
        [ReadOnly(true)]
        public int DelinquencyStep { get; set; }
    }
}
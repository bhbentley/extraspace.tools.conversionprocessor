namespace ConversionProcessorWPF.Model
{
    public class UnitCollection : CLG_Collection<Unit>
    {
        public override string TableName => "APPL.CLG_STAGE_OP_SITE_UNITS";
        public override string InsertStatement => $@"INSERT INTO {TableName}
                                                     VALUES(org.org_site_rent_objects_seq.nextval, :DIMENSIONS, :RENT_RATE, :UNIT_NUMBER, :ATTRIBUTES, :ATTRIBUTE_MEANING, :COMMONKEY, :CSTAGE,
                                                           :STREET_RATE, :RENTAL_UNIT_KEY, :RATE_EFF_DATE, :FUTURE_RATE, :FUTURE_DATE, :ATTRIBUTE2, :ATTRIBUTE2_MEANING, :DOOR, :DOOR_MEANING, 
                                                           :AKSESS, :AKSESS_MEANING, :CLIMATE, :CLIMATE_MEANING, :MAPX, :MAPY, :BUILDING_LABEL, :FLOOR, :BUILDING_ID, :STATUS, :UNIT_MAP_ORIENTATION, :REV_CLASS_ID)";
    }
}
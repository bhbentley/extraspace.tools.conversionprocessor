﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.Contracts;

namespace ConversionProcessorWPF.Model.Validation
{
    /// <summary>
    ///     Validation attribute to indicate that a field is required if another field has a value.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class RequiredDependsOnAttribute : RequiredAttribute
    {
        public RequiredDependsOnAttribute(string otherProperty)
        {
            Contract.Requires(!string.IsNullOrEmpty(otherProperty));

            ErrorMessage = "{0} is required because {1} has a value.";
            OtherProperty = otherProperty;
        }

        public string OtherProperty { get; set; }

        public string FormatErrorMessage(string name, string otherName)
        {
            Contract.Assume(ErrorMessageString != null);
            return string.Format(ErrorMessageString, name, otherName);
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            Contract.Assume(validationContext != null);

            var otherValue = GetOtherValue(validationContext);

            // if the second value is valid, then we need to require this field as well.
            if (IsValid(otherValue) && !IsValid(value))
            {
                var displayName = GetDisplayName(validationContext.MemberName, validationContext);
                var otherDisplayName = GetDisplayName(OtherProperty, validationContext);
                var memberNames = validationContext.MemberName != null ? new[] {validationContext.MemberName} : null;

                return new ValidationResult(
                    FormatErrorMessage(displayName,
                        (!string.IsNullOrEmpty(otherDisplayName) ? otherDisplayName : OtherProperty)), memberNames);
            }

            return ValidationResult.Success;
        }

        protected object GetOtherValue(ValidationContext validationContext)
        {
            Contract.Requires(validationContext != null);
            Contract.Assume(validationContext.ObjectType != null);

            var propertyInfo = validationContext.ObjectType.GetProperty(OtherProperty);

            if (propertyInfo != null)
            {
                var secondValue = propertyInfo.GetValue(validationContext.ObjectInstance, null);
                return secondValue;
            }
            return null;
        }

        private string GetDisplayName(string propertyName, ValidationContext validationContext)
        {
            Contract.Requires(validationContext != null);
            Contract.Requires(propertyName != null);
            
            Contract.Assume(validationContext.ObjectInstance != null);
            var obj = validationContext.ObjectInstance;
            var thing = obj.GetType().GetProperty(propertyName);
            var displayName = (DisplayAttribute) GetCustomAttribute(thing, typeof (DisplayAttribute));

            return (displayName != null ? displayName.Name : null);
        }
    }
}
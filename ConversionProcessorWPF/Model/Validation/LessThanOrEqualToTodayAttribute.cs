﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.Contracts;
using ConversionProcessorWPF.Utilities;

namespace ConversionProcessorWPF.Model.Validation
{
    [AttributeUsage(AttributeTargets.Property)]
    public class LessThanOrEqualToTodayAttribute : ValidationAttribute
    {
        public LessThanOrEqualToTodayAttribute()
            : base("Date must be in the past.")
        { }

        public override bool IsValid(object value)
        {
            Contract.Assume(value != null);

            if (!value.GetType().IsDate())
                return false;

            return (DateTime) value <= DateTime.Today;
        }
    }
}

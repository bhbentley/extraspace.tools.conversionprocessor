﻿using System;
using System.Diagnostics.Contracts;

namespace ConversionProcessorWPF.Model.Validation
{
    [AttributeUsage(AttributeTargets.Property)]
    public class LessThanAttribute : ComparableAttribute
    {
        public LessThanAttribute(string otherProperty)
            : base(otherProperty, "{0} must be less than {1}")
        {
            Contract.Requires(!string.IsNullOrEmpty(otherProperty));
        }

        public override bool Compare(IComparable value, IComparable otherValue)
        {
            if (value == null || otherValue == null) return true;
            return value.CompareTo(otherValue) < 0;
        }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ConversionProcessorWPF.Model.Validation
{
    [AttributeUsage(AttributeTargets.Property)]
    public class SiphonRequiredAttribute : RequiredAttribute 
    {
        public SiphonRequiredAttribute()
        {
            ErrorMessage = "Field is Required.";
        }
    }
}

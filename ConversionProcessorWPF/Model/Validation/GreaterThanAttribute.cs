﻿using System;
using System.Diagnostics.Contracts;

namespace ConversionProcessorWPF.Model.Validation
{
    [AttributeUsage(AttributeTargets.Property)]
    public class GreaterThanAttribute : ComparableAttribute 
    {
        public GreaterThanAttribute(string otherProperty)
            : base(otherProperty, "{0} must be greater than {1}")
        {
            Contract.Requires(!string.IsNullOrEmpty(otherProperty));
        }

        public override bool Compare(IComparable value, IComparable otherValue)
        {
            if (value == null || otherValue == null) return true;
            return value.CompareTo(otherValue) > 0;
        }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.Contracts;

namespace ConversionProcessorWPF.Model.Validation
{
    [AttributeUsage(AttributeTargets.Property)]
    public abstract class ComparableAttribute : ValidationAttribute
    {
        protected ComparableAttribute(string otherProperty, string errorMessage)
            : base(errorMessage)
        {
            Contract.Requires(!string.IsNullOrEmpty(otherProperty));

            OtherProperty = otherProperty;
        }

        public string OtherProperty { get; set; }

        public string FormatErrorMessage(string name, string otherName)
        {
            Contract.Assume(ErrorMessageString != null);

            return string.Format(ErrorMessageString, name, otherName);
        }

        public abstract bool Compare(IComparable value, IComparable otherValue);

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            Contract.Assume(validationContext != null);

            var firstComparable = value as IComparable;
            var secondComparable = GetSecondComparable(validationContext);

            if (!Compare(firstComparable, secondComparable))
            {
                var displayName = GetDisplayName(validationContext.MemberName, validationContext);
                var otherDisplayName = GetDisplayName(OtherProperty, validationContext);
                string[] memberNames = validationContext.MemberName != null ? new string[] { validationContext.MemberName } : null;

                return new ValidationResult(
                    FormatErrorMessage(displayName, (!string.IsNullOrEmpty(otherDisplayName) ? otherDisplayName : OtherProperty)), memberNames);
            }

            return ValidationResult.Success;
        }

        protected IComparable GetSecondComparable(ValidationContext validationContext)
        {
            Contract.Requires(validationContext != null);
            Contract.Assume(validationContext.ObjectType != null);

            var propertyInfo = validationContext.ObjectType.GetProperty(OtherProperty);

            if (propertyInfo != null)
            {
                var secondValue = propertyInfo.GetValue(validationContext.ObjectInstance, null);
                return secondValue as IComparable;
            }
            return null;
        }

        private string GetDisplayName(string propertyName, ValidationContext validationContext)
        {
            Contract.Requires(validationContext != null);
            Contract.Requires(propertyName != null);
            
            Contract.Assume(validationContext.ObjectInstance != null);
            object obj = validationContext.ObjectInstance;
            var thing = obj.GetType().GetProperty(propertyName);
            var displayName = (DisplayAttribute)GetCustomAttribute(thing, typeof(DisplayAttribute));

            return (displayName != null ? displayName.Name : null);
        }

    }
}

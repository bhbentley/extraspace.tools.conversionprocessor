﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.Contracts;
using ConversionProcessorWPF.Utilities;

namespace ConversionProcessorWPF.Model.Validation
{
    [AttributeUsage(AttributeTargets.Property)]
    public class LessThanMaxPtdAttribute : ValidationAttribute
    {
        private static readonly DateTime MaxPtd = DateTime.Today.AddYears(5);

        public LessThanMaxPtdAttribute()
            : base($"Date must less than the max Paid Thru Date ({MaxPtd.ToString("MM/dd/yyyy")})")
        { }

        public override bool IsValid(object value)
        {
            Contract.Assume(value != null);

            if (!value.GetType().IsDate())
                return false;

            return (DateTime) value < MaxPtd;
        }

    }
}

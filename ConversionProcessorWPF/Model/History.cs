using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ConversionProcessorWPF.Model
{
    public class History : CLG_Object
    {
        [DataColumn("SITE_ID")]
        [Display(Name = "Site Id", Order = 2)] 
        public long? SiteId { get; set; }


        [DataColumn("RENTAL_ID")]
        [Display(Name = "Rental Id", Order = 3)] 
        public long? RentalId { get; set; }


        [DataColumn("TDATE")]
        [Display(Name = "Tdate", Order = 4)] 
        public DateTime? Tdate { get; set; }


        [DataColumn("TRAN")]
        [Display(Name = "Tran", Order = 5)] 
        [MaxLength(260, ErrorMessage="Length must be less than 260.")]
        public string Tran { get; set; }


        [DataColumn("TTYPE")]
        [Display(Name = "Ttype", Order = 6)] 
        [MaxLength(40, ErrorMessage="Length must be less than 40.")]
        public string Ttype { get; set; }


        [DataColumn("ACCOUNT")]
        [Display(Name = "Account", Order = 7)] 
        [MaxLength(2000, ErrorMessage="Length must be less than 2000.")]
        public string Account { get; set; }


        [DataColumn("DESCRIPTION")]
        [Display(Name = "Description", Order = 8)] 
        [MaxLength(2000, ErrorMessage="Length must be less than 2000.")]
        public string Description { get; set; }


        [DataColumn("ITEM_PRICE")]
        [Display(Name = "Item Price", Order = 9)] 
        public decimal? ItemPrice { get; set; }


        [DataColumn("CHARGE")]
        [Display(Name = "Charge", Order = 10)] 
        public decimal? Charge { get; set; }


        [DataColumn("PAYMENT")]
        [Display(Name = "Payment", Order = 11)] 
        public decimal? Payment { get; set; }

        [DataColumn("RENTALS")]
        [Display(Name = "Rentals", Order = 13)] 
        [MaxLength(4, ErrorMessage="Length must be less than 4.")]
        public string Rentals { get; set; }


        [DataColumn("BALANCE")]
        [Display(Name = "Balance", Order = 14)] 
        public decimal? Balance { get; set; }


        [DataColumn("CREDBAL")]
        [Display(Name = "Credbal", Order = 15)] 
        public decimal? Credbal { get; set; }


        [DataColumn("PMT")]
        [Display(Name = "Pmt", Order = 16)] 
        public decimal? Pmt { get; set; }


        [DataColumn("PTD")]
        [Display(Name = "Ptd", Order = 17)] 
        public DateTime? Ptd { get; set; }


        [DataColumn("ROW_NUMBER")]
        [Display(Name = "Row Number", Order = 18)] 
        public long? RowNumber { get; set; }


        [DataColumn("CSTAGE")]
        public decimal? Cstage { get; set; }


        [DataColumn("COMMON_KEY")]
        [Display(Name = "CommonKey", Order = 0)]
        [ReadOnly(true)]
        public string CommonKey { get; set; }


        [DataColumn("RENTAL_UNIT_KEY")]
        [Display(Name = "Rental Unit Key", Order = 0)] 
        [MaxLength(100, ErrorMessage="Length must be less than 100.")]
        public string RentalUnitKey { get; set; }
    }
}
﻿using System;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using System.Runtime.CompilerServices;
using ConversionProcessorWPF.ImportTemplates;

namespace ConversionProcessorWPF.Model
{
    public class Site : INotifyPropertyChanged
    {
        public string ConversionType { get; set; }
        public string SiteNumber { get; set; }
        public string SiteName { get; set; }
        public long SiteId { get; set; }
        public bool HasPreviousData { get; set; }
        public bool HasSiteId => SiteId > 0;
        public string DataPath { get; set; }

        public bool Selected
        {
            get { return _selected; }
            set
            {
                _selected = value;
                OnPropertyChanged();
            }
        }
        private bool _selected;

        public ImportTemplate DataImporter
        {
            get
            {
                Contract.Ensures(Contract.Result<ImportTemplate>() != null);
                if (_dataImporter == null)
                {
                    _dataImporter = ImportTemplateFactory.Create(ConversionType, DataPath, SiteId, SiteNumber);
                    _dataImporter.LoadData();
                }
                return _dataImporter;
            }
        }
        private ImportTemplate _dataImporter;

        public AutoPayCollection AutoPays => (DataImporter.AutoPays);

        public AccountCollection Accounts => (DataImporter.Accounts);

        public AddressCollection Addresses => (DataImporter.Addresses);

        public ContactCollection Contacts => (DataImporter.Contacts);

        public NoteCollection Notes => (DataImporter.Notes);

        public PcdCollection Pcds => (DataImporter.Pcds);

        public PhoneCollection Phones => (DataImporter.Phones);

        public RentalCollection Rentals => (DataImporter.Rentals);

        public ServiceCollection Services => (DataImporter.Services);

        public UnitCollection Units => (DataImporter.Units);

        public HistoryCollection History => (DataImporter.History);

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event 
        protected void OnPropertyChanged([CallerMemberName]string name = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}
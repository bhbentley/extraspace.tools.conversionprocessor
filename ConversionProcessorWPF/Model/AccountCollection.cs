namespace ConversionProcessorWPF.Model
{
    public class AccountCollection : CLG_Collection<Account>
    {
        public override string TableName => "APPL.CLG_STAGE_OP_ACCOUNTS";
        public override string InsertStatement => $@"INSERT INTO {TableName}
                                                    VALUES(act.act_accounts_seq.nextval, :ACCOUNT_NAME, :ACCOUNT_CLASS, :ACCOUNT_STATUS, :ACCOUNT_TYPE, :BILL_TO_ADDRESS,
                                                          :BILLING_DATE, :GATE_PIN, :SITE_ID, :SEND_INVOICE, :GATE24HR, :GATE_TIME_ZONE, :GATE_KEYPAD_ZONE, :COMMONKEY, :CSTAGE)";
    }
}
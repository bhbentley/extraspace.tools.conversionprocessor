namespace ConversionProcessorWPF.Model
{
    public class HistoryCollection : CLG_Collection<History>
    {
        public override string TableName => "APPL.CLG_STAGE_TRAN_LEGACY";
        public override string InsertStatement => $@"INSERT INTO {TableName}
                                                    VALUES(tran.TLH_ID_S.nextval, :SITE_ID, :RENTAL_ID, :TDATE, :TRAN, :TTYPE, :ACCOUNT, :DESCRIPTION, :ITEM_PRICE,
                                                    :CHARGE, :PAYMENT, NULL, :RENTALS, :BALANCE, :CREDBAL, :PMT, :PTD, :ROW_NUMBER, :CSTAGE, :COMMON_KEY, :RENTAL_UNIT_KEY)";
    }
}
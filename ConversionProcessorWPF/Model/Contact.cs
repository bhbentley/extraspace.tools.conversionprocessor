using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ConversionProcessorWPF.Model.Validation;

namespace ConversionProcessorWPF.Model
{
    public class Contact : CLG_Object
    {
        [DataColumn("FIRST_NAME")]
        [Display(Name = "First Name", Order = 2)]
        [SiphonRequired]
        [MaxLength(50, ErrorMessage = "Length must be less than 50.")]
        public string FirstName
        {
            get { return string.IsNullOrWhiteSpace(_firstName) ? "NEED FIRST NAME" : _firstName; }
            set { _firstName = value; }
        }
        private string _firstName;


        [DataColumn("LAST_NAME")]
        [Display(Name = "Last Name", Order = 4)]
        [SiphonRequired]
        [MaxLength(50, ErrorMessage = "Length must be less than 50.")]
        public string LastName
        {
            get { return string.IsNullOrWhiteSpace(_lastName) ? "NEED LAST NAME" : _lastName; }
            set { _lastName = value; }
        }
        private string _lastName;


        [DataColumn("KNOWN_AS")]
        [Display(Name = "Known As", Order = 5)]
        [MaxLength(50, ErrorMessage = "Length must be less than 50.")]
        public string KnownAs { get; set; }


        [DataColumn("CONTACT_TYPE")]
        [Display(Name = "Contact Type", Order = 6)]
        [SiphonRequired]
        [Range(1, 2, ErrorMessage = "Invalid Contact Type.  1=Primary 2=Secondary")]
        public decimal? ContactType { get; set; }


        [DataColumn("UNIT_ACCESS")]
        public decimal? UnitAccess => null;

        [DataColumn("EMPLOYER")]
        [Display(Name = "Employer", Order = 7)]
        [MaxLength(75, ErrorMessage = "Length must be less than 75.")]
        public string Employer { get; set; }


        [DataColumn("ACTIVE")]
        public bool? Active => true;


        [DataColumn("EMAIL_ADDRESS")]
        [Display(Name = "Email Address", Order = 9)]
        [MaxLength(100, ErrorMessage = "Length must be less than 100.")]
        public string EmailAddress { get; set; }


        [DataColumn("DRIVERS_LICENSE_NO")]
        [Display(Name = "Drivers License No", Order = 11)]
        [MaxLength(35, ErrorMessage = "Length must be less than 35.")]
        public string DriversLicenseNo { get; set; }


        [DataColumn("DRIVERS_LICENSE_STATE")]
        [Display(Name = "Drivers License State", Order = 12)]
        [MaxLength(20, ErrorMessage = "Length must be less than 20.")]
        public string DriversLicenseState { get; set; }


        [DataColumn("DATE_OF_BIRTH")]
        [Display(Name = "Date Of Birth", Order = 10)]
        [MaxLength(20, ErrorMessage = "Length must be less than 20.")]
        public string DateOfBirth { get; set; }


        [DataColumn("SSN")]
        [Display(Name = "Ssn", Order = 14)]
        [MaxLength(20, ErrorMessage = "Length must be less than 20.")]
        public string Ssn { get; set; }


        [DataColumn("DRIVERS_LICENSE_EXPIRES")]
        [Display(Name = "Drivers License Expires", Order = 13)]
        [MaxLength(20, ErrorMessage = "Length must be less than 20.")]
        public string DriversLicenseExpires { get; set; }


        [DataColumn("MIDDLE_NAME")]
        [Display(Name = "Middle Name", Order = 3)]
        [MaxLength(50, ErrorMessage = "Length must be less than 50.")]
        public string MiddleName { get; set; }


        [DataColumn("COMMONKEY")]
        [Display(Name = "CommonKey", Order = 0)]
        [ReadOnly(true)]
        [SiphonRequired]
        public string CommonKey { get; set; }


        [DataColumn("CSTAGE")]
        public decimal? Cstage { get; set; }


        [DataColumn("PARENT_ID")]
        public long? ParentId => null;


        [DataColumn("CONTACT_KEY")]
        [Display(Name = "Contact Key", Order = 0)]
        [ReadOnly(true)]
        public string ContactKey { get; set; }


        [DataColumn("PASSWORD")]
        [Display(Name = "Password", Order = 20)]
        [MaxLength(30, ErrorMessage = "Length must be less than 30.")]
        public string Password { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;

namespace ConversionProcessorWPF.Model
{
    public abstract class CLG_Collection<T> : ObservableCollection<T>, INotifyPropertyChanged, ICLG_Collection where T : CLG_Object
    {
        public abstract string TableName { get; }
        public abstract string InsertStatement { get; }
        public List<CLG_Object> GetItems()
        {
            return this.Select(a => a as CLG_Object).ToList();
        }

        public bool IsValid
        {
            get
            {
                return this.All(c => c.IsValid);
            }
        }

        public int InvalidCount
        {
            get
            {
                return this.Count(c => !c.IsValid);
            }
        }

        public DataTable ToDataTable()
        {
            DataTable table = CreateTable();
            Type entityType = typeof(T);
            var properties = entityType.GetProperties();

            foreach (T item in this)
            {
                DataRow row = table.NewRow();

                foreach (var prop in properties)
                {
                    var dataColumnAttribute = prop.GetCustomAttributes(typeof(DataColumnAttribute), false).FirstOrDefault() as DataColumnAttribute;
                    if (dataColumnAttribute == null) continue;

                    row[dataColumnAttribute.ColumnName] = prop.GetValue(item) ?? DBNull.Value;
                }

                table.Rows.Add(row);
            }

            return table;
        }

        private DataTable CreateTable()
        {
            Type entityType = typeof(T);
            DataTable table = new DataTable(this.TableName);
            var properties = entityType.GetProperties();

            foreach (var prop in properties)
            {
                var dataColumnAttribute = prop.GetCustomAttributes(typeof(DataColumnAttribute), false).FirstOrDefault() as DataColumnAttribute;
                if (dataColumnAttribute == null) continue;

                table.Columns.Add(new DataColumn(dataColumnAttribute.ColumnName, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType));
            }

            return table;
        }

        public void SetFiltered()
        {
            foreach (var item in this)
            {
                item.Filtered = item.IsValid;
            }
        }

        public void ClearFiltered()
        {
            foreach (var item in this)
            {
                item.Filtered = false;
            }
        }

        protected override void InsertItem(int index, T item)
        {
            base.InsertItem(index, item);
            if (item == null) return;
            item.ErrorsChanged += item_ErrorsChanged;
            item.Validate();
            item.Filtered = item.IsValid;
        }

        void item_ErrorsChanged(object sender, DataErrorsChangedEventArgs e)
        {
            OnPropertyChanged("IsValid");
        }

        #region INotifyPropertyChanged Members

        public new event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event 
        protected void OnPropertyChanged([CallerMemberName]string name = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}

﻿using System.Collections.Generic;
using System.Data;
using ConversionProcessorWPF.Utilities;

namespace ConversionProcessorWPF.DataProviders
{
    internal static class UserDataProvider
    {
        public static List<User> GetUsers()
        {
            var retval = new List<User>();
            const string siteInfoQuery = @"SELECT 
                                             person_id
                                           , first_name
                                           , last_name
                                           FROM org.org_users
                                           WHERE STATUS = 5
                                           and org_id = 1000
                                           order by first_name";

            using (var conn = OracleConnectionProvider.GetConnection())
            {
                var cmd = conn.CreateCommand();
                if (ConnectionState.Closed == conn.State)
                {
                    conn.Open();
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = siteInfoQuery;

                retval.Add(new User() {Id = 0, Name = "Select a Store User"});
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var values = new object[reader.FieldCount];
                    reader.GetValues(values);

                    retval.Add(new User
                    {
                        Id = (long)values[0],
                        Name = $"{values[1]} {values[2]}"
                    });
                }
            }
            return retval;
        }

    }
}

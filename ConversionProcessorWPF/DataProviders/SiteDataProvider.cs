﻿using System.Data;
using ConversionProcessorWPF.Utilities;
using Oracle.DataAccess.Client;

namespace ConversionProcessorWPF.DataProviders
{
    internal static class SiteDataProvider
    {
        public static SiteInfo GetSiteInfo(string siteNumber)
        {
            var retval = new SiteInfo();
            const string siteInfoQuery = @"SELECT 
                                             s.site_id
                                           , s.site_name
                                           , s.org_number_xref
                                           , appl.appl_bin.getlookupvalue(1003, ss.app_status, 'VALUE', 1000) AS status
                                           , p.financial_close
                                           FROM org.org_sites s
                                           INNER JOIN org.org_site_status ss ON s.site_id = ss.site_id
                                           INNER JOIN org.org_site_proc p ON s.site_id = p.site_id
                                           WHERE s.org_number_xref = :pv_site_number";

            using (var conn = OracleConnectionProvider.GetConnection())
            {
                var cmd = conn.CreateCommand();
                if (ConnectionState.Closed == conn.State)
                {
                    conn.Open();
                }
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = siteInfoQuery;
                cmd.BindByName = true;
                cmd.Parameters.Add(new OracleParameter("pv_site_number", OracleDbType.Varchar2, siteNumber, ParameterDirection.Input));
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var values = new object[reader.FieldCount];
                    reader.GetValues(values);
                    retval.Id = (long)values[0];
                    retval.Name = values[1].ToString();
                    retval.SiteNumber = values[2].ToString();
                    retval.Status = values[3].ToString();
                    retval.FinancialClose = values[4].ToString() == "Y";
                }
                conn.Close();
            }
            return retval;
        }

        public static string GetSiteState(long siteId)
        {
            const string siteInfoQuery = @"SELECT state 
                                           FROM org.org_addresses 
                                           WHERE site_id = :pv_site_id 
                                           AND rownum = 1";

            using (var conn = OracleConnectionProvider.GetConnection())
            {
                var cmd = conn.CreateCommand();
                if (ConnectionState.Closed == conn.State)
                {
                    conn.Open();
                }
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = siteInfoQuery;
                cmd.BindByName = true;
                cmd.Parameters.Add(new OracleParameter("pv_site_id", OracleDbType.Decimal, siteId, ParameterDirection.Input));
                var result = cmd.ExecuteScalar();
                conn.Close();

                return result?.ToString();
            }
        }

        public static bool SiteHasData(long siteId)
        {
            const string siteInfoQuery = @"SELECT unit_id
                                           FROM org.org_site_rent_objects s
                                           WHERE s.site_id = :pv_site_id
                                           AND rownum = 1";

            using (var conn = OracleConnectionProvider.GetConnection())
            {
                var cmd = conn.CreateCommand();
                if (ConnectionState.Closed == conn.State)
                {
                    conn.Open();
                }
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = siteInfoQuery;
                cmd.BindByName = true;
                cmd.Parameters.Add(new OracleParameter("pv_site_id", OracleDbType.Decimal, siteId, ParameterDirection.Input));
                var result = cmd.ExecuteScalar();
                conn.Close();

                return result != null;
            }
        }


    }
}

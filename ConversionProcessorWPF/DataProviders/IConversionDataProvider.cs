﻿using System.Collections.Generic;
using ConversionProcessorWPF.Model;

namespace ConversionProcessorWPF.DataProviders
{
    public interface IConversionDataProvider
    {
        IEnumerable<Site> GetSiteConversionData();
    }
}

﻿namespace ConversionProcessorWPF.DataProviders
{
    public class SiteInfo
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string SiteNumber { get; set; }
        public string Status { get; set; }
        public bool FinancialClose { get; set; }
    }
}

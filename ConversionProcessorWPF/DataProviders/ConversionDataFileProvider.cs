﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using ConversionProcessorWPF.ImportTemplates;
using ConversionProcessorWPF.Model;
using ConversionProcessorWPF.Properties;

namespace ConversionProcessorWPF.DataProviders
{
    class ConversionDataFileProvider : IConversionDataProvider
    {
        #region IConversionDataProvider Members

        public IEnumerable<Site> GetSiteConversionData()
        {
            Contract.Ensures(Contract.Result<IEnumerable<Site>>() != null);

            if (!Directory.Exists(Settings.Default.ConversionDataFileDirectory))
                Directory.CreateDirectory(Settings.Default.ConversionDataFileDirectory);

            CreateMissingDataFolders();

            var parentDir = new DirectoryInfo(Settings.Default.ConversionDataFileDirectory);

            return (from templateDir in parentDir.GetDirectories().Where(d => ImportTemplateFactory.IsImplemented(d.Name))
                    from siteDir in templateDir.GetDirectories()
                    let siteNumber = Path.GetFileName(siteDir.Name)
                    let siteInfo = SiteDataProvider.GetSiteInfo(siteNumber)
                    select new Site
                    {
                        DataPath = siteDir.FullName,
                        ConversionType = Path.GetFileName(templateDir.Name),
                        SiteNumber = siteNumber,
                        SiteId = siteInfo.Id,
                        SiteName = siteInfo.Name,
                        HasPreviousData = SiteDataProvider.SiteHasData(siteInfo.Id),
                        Selected = false
                    }).ToList();
        }

        #endregion

        private static void CreateMissingDataFolders()
        {
            foreach (var conversionType in ImportTemplateFactory.GetImplementedConversionTypes())
            {
                Contract.Assert(conversionType != null);
                var conversionTypeDirectory = Path.Combine(Settings.Default.ConversionDataFileDirectory, conversionType);
                if (!Directory.Exists(conversionTypeDirectory))
                    Directory.CreateDirectory(conversionTypeDirectory);
            }
        }
    }
}

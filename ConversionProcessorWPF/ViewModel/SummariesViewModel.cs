﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ConversionProcessorWPF.DataProviders;
using ConversionProcessorWPF.Utilities;
using ConversionProcessorWPF.View.Commands;
using PropertyChanged;

namespace ConversionProcessorWPF.ViewModel
{
    [ImplementPropertyChanged]
    public class SummariesViewModel
    {

        public string SiteNumber { get; set; }
        public string ErrorMessage { get; set; }
        public bool IsGenerating { get; set; }

        public ObservableCollection<SiteInfo> Sites
        {
            get { return _sites ?? (_sites = new ObservableCollection<SiteInfo>(SummaryOfSummaries.GetSummarySites())); }
            set { _sites = value; }
        }
        private ObservableCollection<SiteInfo> _sites;

        public ICommand RefreshCommand => _refreshCommand ?? (_refreshCommand = new DelegateCommand(Refresh, CanRefresh));
        private DelegateCommand _refreshCommand;

        private void Refresh()
        {
            Sites = null;
        }

        private bool CanRefresh()
        {
            return !IsGenerating;
        }

        public ICommand GenerateCommand => _generateCommand ?? (_generateCommand = new AwaitableDelegateCommand(Generate, CanGenerate));
        private AwaitableDelegateCommand _generateCommand;

        private async Task Generate()
        {
            try
            {
                IsGenerating = true;
                await Task.Run(() => SummaryOfSummaries.Generate(Sites.ToList()));
                MessageBox.Show("Summary has been generated.");
                Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                IsGenerating = false;
            }
        }

        private bool CanGenerate()
        {
            return Sites.Any();
        }

        public ICommand AddCommand => _addCommand ?? (_addCommand = new DelegateCommand(Add, CanAdd));
        private DelegateCommand _addCommand;

        private void Add()
        {
            ErrorMessage = string.Empty;

            if (Sites.Any(s => s.SiteNumber == SiteNumber))
            {
                ErrorMessage = $"Site {SiteNumber} already added";
                return;
            }

            var newSiteInfo = SiteDataProvider.GetSiteInfo(SiteNumber);
            if (newSiteInfo == null || newSiteInfo.Id == 0)
            {
                ErrorMessage = "Invalid Site Number";
                return;
            }

            Sites.Add(newSiteInfo);
            SiteNumber = string.Empty;
        }

        private bool CanAdd()
        {
            return !string.IsNullOrWhiteSpace(SiteNumber);
        }

        public ICommand DeleteCommand => _deleteCommand ?? (_deleteCommand = new DelegateCommand<object>(Delete));
        private DelegateCommand<object> _deleteCommand;

        private void Delete(object selectedItems)
        {
            var selectedSites = selectedItems as IList;
            if (selectedSites == null) return;

            foreach (var site in selectedSites.Cast<SiteInfo>().ToList())
            {
                Sites.Remove(site);
            }
        }
    }
}

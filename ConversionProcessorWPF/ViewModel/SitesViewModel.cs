﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using ConversionProcessorWPF.DataProviders;
using ConversionProcessorWPF.Model;
using ConversionProcessorWPF.Properties;
using ConversionProcessorWPF.View;
using ConversionProcessorWPF.View.Commands;

namespace ConversionProcessorWPF.ViewModel
{
    public class SitesViewModel : INotifyPropertyChanged
    {
        public bool IsValid
        {
            get
            {
                return SelectedSiteViewModels != null && SelectedSiteViewModels.All(s => s.IsValid);
            }
        }

        public ObservableCollection<Site> Sites
        {
            get
            {
                try
                {
                    if (_sites == null)
                    {
                        var dataProvider = new ConversionDataFileProvider();
                        var data = dataProvider.GetSiteConversionData();

                        _sites = new ObservableCollection<Site>(data);

                        foreach (var site in _sites)
                        {
                            site.PropertyChanged += site_PropertyChanged;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorMessage = ex.Message;
                    _sites = new ObservableCollection<Site>();
                }

                return _sites;
            }
        }
        private ObservableCollection<Site> _sites;

        public ListCollectionView SitesCollectionView
        {
            get
            {
                if (_sitesCollectionView == null)
                {
                    _sitesCollectionView = new ListCollectionView(this.Sites);
                    Contract.Assert(_sitesCollectionView.SortDescriptions != null);
                    _sitesCollectionView.SortDescriptions.Add(new SortDescription("ConversionType", ListSortDirection.Ascending));
                    _sitesCollectionView.SortDescriptions.Add(new SortDescription("SiteNumber", ListSortDirection.Ascending));
                    Contract.Assert(_sitesCollectionView.GroupDescriptions != null);
                    _sitesCollectionView.GroupDescriptions.Add(new PropertyGroupDescription("ConversionType"));
                }
                return _sitesCollectionView;
            }
        }
        private ListCollectionView _sitesCollectionView;

        public bool HasSelectedSitesAndSelectedUser
        {
            get { return Sites.Any(s => s.Selected) && SelectedUserId > 0; }
        }

        private ObservableCollection<SiteViewModel> _siteViewModels;
        public ObservableCollection<SiteViewModel> SiteViewModels
        {
            get
            {
                if (_siteViewModels == null)
                {
                    _siteViewModels = new ObservableCollection<SiteViewModel>(Sites.Select(s => new SiteViewModel(s)));
                    foreach (var siteViewModel in _siteViewModels)
                    {
                        siteViewModel.PropertyChanged += siteViewModel_PropertyChanged;
                    }
                }
                return _siteViewModels;
            }
        }

        private ObservableCollection<SiteViewModel> _selectedSiteViewModels = new ObservableCollection<SiteViewModel>();
        public ObservableCollection<SiteViewModel> SelectedSiteViewModels
        {
            get { return _selectedSiteViewModels; }
            set
            {
                _selectedSiteViewModels = value;
                OnPropertyChanged();
            }
        }
        public ListCollectionView SelectedSiteViewModelsCollectionView
        {
            get
            {
                if (_selectedSiteViewModelsCollectionView == null)
                {
                    _selectedSiteViewModelsCollectionView = new ListCollectionView(this.SelectedSiteViewModels);
                    Contract.Assert(_selectedSiteViewModelsCollectionView.SortDescriptions != null);
                    _selectedSiteViewModelsCollectionView.SortDescriptions.Add(new SortDescription("Site.ConversionType", ListSortDirection.Ascending));
                    _selectedSiteViewModelsCollectionView.SortDescriptions.Add(new SortDescription("IsValid", ListSortDirection.Ascending));
                    _selectedSiteViewModelsCollectionView.SortDescriptions.Add(new SortDescription("Site.SiteNumber", ListSortDirection.Ascending));
                    Contract.Assert(_selectedSiteViewModelsCollectionView.GroupDescriptions != null);
                    _selectedSiteViewModelsCollectionView.GroupDescriptions.Add(new PropertyGroupDescription("Site.ConversionType"));
                }
                return _selectedSiteViewModelsCollectionView;
            }
            set
            {
                _selectedSiteViewModelsCollectionView = value;
                OnPropertyChanged();
            }
        }
        private ListCollectionView _selectedSiteViewModelsCollectionView;

        private SiteViewModel _selectedSiteViewModel = null;
        public SiteViewModel SelectedSiteViewModel
        {
            get
            {
                if (_selectedSiteViewModel == null && _selectedSiteViewModels.Count > 0)
                    _selectedSiteViewModel = _selectedSiteViewModels.FirstOrDefault();
                return _selectedSiteViewModel;
            }
            set
            {
                _selectedSiteViewModel = value;
                OnPropertyChanged();
            }
        }

        public List<User> Users => _users ?? (_users = UserDataProvider.GetUsers());
        private List<User> _users;

        public long SelectedUserId
        {
            get { return Settings.Default.PersonId; }
            set
            {
                Settings.Default.PersonId = value;
                Settings.Default.Save();
            }
        }

        public object SelectedConflictsTab { get; set; }
        public object SelectedResultsTab { get; set; }

        public string ErrorMessage
        {
            get { return _errorMessage; }
            set
            {
                _errorMessage = value;
                OnPropertyChanged();
            }
        }
        private string _errorMessage;

        public bool SelectAllChecked
        {
            get { return _selectAllChecked; }
            set
            {
                _selectAllChecked = value;
                OnPropertyChanged();
            }
        }
        private bool _selectAllChecked = false;

        public ICommand CloseCommand => _closeCommand ?? (_closeCommand = new DelegateCommand(CloseApp, CanClose));
        private DelegateCommand _closeCommand;

        private async void CloseApp()
        {
            await Task.WhenAll(SelectedSiteViewModels.Select(site => Task.Run(() => site.RunPostExport())).ToArray());
            Application.Current.Shutdown();
        }

        private bool CanClose()
        {
            return SelectedSiteViewModels.All(s => !s.Exporter.ExportRunning);
        }

        void site_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Selected")
            {
                OnPropertyChanged(nameof(HasSelectedSitesAndSelectedUser));
            }
        }

        public void UpdateSelectedSites()
        {
            Contract.Assume(SelectedSiteViewModels != null);
            SelectedSiteViewModels.Clear();
            foreach (var siteViewModel in SiteViewModels.Where(s => s.Site.Selected))
            {
                SelectedSiteViewModels.Add(siteViewModel);
            }
            SelectedSiteViewModelsCollectionView.Refresh();
        }

        public ICommand SelectAllCommand => _selectAllCommand ?? (_selectAllCommand = new DelegateCommand(SelectAll, () => true));
        private DelegateCommand _selectAllCommand;

        private void SelectAll()
        {
            foreach (var site in Sites)
            {
                if (site.HasSiteId)
                    site.Selected = SelectAllChecked;
            }
        }

        public async void ExportAll()
        {
            await Task.WhenAll(SelectedSiteViewModels.Select(site => Task.Run(() => site.ExportSite())).ToArray());
            _closeCommand.RaiseCanExecuteChanged();
        }

        void siteViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsValid")
                OnPropertyChanged("IsValid");
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event 
        protected void OnPropertyChanged([CallerMemberName]string name = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}

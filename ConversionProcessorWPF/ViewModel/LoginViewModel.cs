﻿using System;
using System.Data;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ConversionProcessorWPF.Properties;
using ConversionProcessorWPF.Utilities;
using ConversionProcessorWPF.View.Commands;
using PropertyChanged;

namespace ConversionProcessorWPF.ViewModel
{
    [ImplementPropertyChanged]
    public class LoginViewModel
    {
        private ICommand _closeCommand;
        private ICommand _loginCommand;
        public string UserName
        {
            get { return Settings.Default.UserName; }
            set
            {
                Settings.Default.UserName = value;
                Settings.Default.Save();
            }
        }
        public string Password { private get; set; }
        public string Database
        {
            get { return Settings.Default.tnsName; }
            set
            {
                Settings.Default.tnsName = value;
                Settings.Default.Save();
            }
        }
        private bool TryingToLogin { get; set; }

        public ICommand LoginCommand => _loginCommand ?? (_loginCommand = new AwaitableDelegateCommand(Login, CanLogin));

        public ICommand CloseCommand => _closeCommand ?? (_closeCommand = new DelegateCommand(Application.Current.Shutdown, CanClose));

        private bool CanClose()
        {
            return !TryingToLogin;
        }

        private async Task Login()
        {
            TryingToLogin = true;

            if (await Task.Run(() => LoginToDatabase()))
                OnLoginSucceeded(EventArgs.Empty);
            else
                MessageBox.Show("Login FAILED!!!!");

            TryingToLogin = false;
        }

        private bool CanLogin()
        {
            return !string.IsNullOrWhiteSpace(UserName) && !string.IsNullOrWhiteSpace(Password) &&
                   !string.IsNullOrWhiteSpace(Database) && !TryingToLogin;
        }

        private bool LoginToDatabase()
        {
            try
            {
                OracleConnectionProvider.Password = Password;
                using (var conn = OracleConnectionProvider.GetConnection())
                {
                    if (ConnectionState.Closed == conn.State)
                        conn.Open();
                    var c = conn.CreateCommand();
                    conn.Close();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public event EventHandler LoginSucceeded;

        public void OnLoginSucceeded(EventArgs e)
        {
            LoginSucceeded?.Invoke(this, e);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using ConversionProcessorWPF.Model;

namespace ConversionProcessorWPF.ViewModel
{
    public class SitePreviouslyConvertedViewModel
    {
        public SitePreviouslyConvertedViewModel(ObservableCollection<Site> sites)
        {
            Contract.Requires(sites != null);
            Contract.Ensures(Sites != null);

            Sites = sites;
        }

        public ObservableCollection<Site> Sites { get; set; }

        public ListCollectionView SitesCollectionView
        {
            get
            {
                if (_sitesCollectionView == null)
                {
                    _sitesCollectionView = new ListCollectionView(this.Sites);
                    Contract.Assert(_sitesCollectionView.SortDescriptions != null);
                    _sitesCollectionView.SortDescriptions.Add(new SortDescription("ConversionType", ListSortDirection.Ascending));
                    _sitesCollectionView.SortDescriptions.Add(new SortDescription("SiteNumber", ListSortDirection.Ascending));
                    Contract.Assert(_sitesCollectionView.GroupDescriptions != null);
                    _sitesCollectionView.GroupDescriptions.Add(new PropertyGroupDescription("ConversionType"));
                    _sitesCollectionView.Filter = Filter;
                }
                return _sitesCollectionView;
            }
        }
        private ListCollectionView _sitesCollectionView;

        public bool Filter(object item)
        {
            var site = item as Site;
            if (site == null)
                return false;

            return site.HasPreviousData && site.Selected;
        }
    }
}

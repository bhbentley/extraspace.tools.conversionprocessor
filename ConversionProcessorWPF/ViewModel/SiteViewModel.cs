﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using ConversionProcessorWPF.Export;
using ConversionProcessorWPF.Model;
using ConversionProcessorWPF.Properties;
using ConversionProcessorWPF.Utilities;
using ConversionProcessorWPF.View.Commands;

namespace ConversionProcessorWPF.ViewModel
{
    public class SiteViewModel : INotifyPropertyChanged
    {

        public SiteViewModel(Site site)
        {
            Contract.Requires(site != null);
            Contract.Ensures(Site != null);

            Site = site;
        }

        public Site Site { get; private set; }

        public bool HasExported { get; private set; }

        public bool SiteKilled { get; private set; } = false;

        public ListCollectionView AutoPays
        {
            get
            {
                if (_autoPays == null)
                {
                    _autoPays = new ListCollectionView(Site.AutoPays);
                    _autoPays.Filter = InvalidFilter;
                    Site.AutoPays.PropertyChanged += site_PropertyChanged;
                }
                return _autoPays;
            }
        }
        private ListCollectionView _autoPays;

        public ListCollectionView Accounts
        {
            get
            {
                if (_accounts == null)
                {
                    _accounts = new ListCollectionView(Site.Accounts);
                    _accounts.Filter = InvalidFilter;
                    Site.Accounts.PropertyChanged += site_PropertyChanged;
                }
                return _accounts;
            }
        }
        private ListCollectionView _accounts;

        public ListCollectionView Addresses
        {
            get
            {
                if (_addresses == null)
                {
                    _addresses = new ListCollectionView(Site.Addresses);
                    _addresses.Filter = InvalidFilter;
                    Site.Addresses.PropertyChanged += site_PropertyChanged;
                }
                return _addresses;
            }
        }
        private ListCollectionView _addresses;

        public ListCollectionView Contacts
        {
            get
            {
                if (_contacts == null)
                {
                    _contacts = new ListCollectionView(Site.Contacts);
                    _contacts.Filter = InvalidFilter;
                    Site.Contacts.PropertyChanged += site_PropertyChanged;
                }
                return _contacts;
            }
        }
        private ListCollectionView _contacts;

        public ListCollectionView Notes
        {
            get
            {
                if (_notes == null)
                {
                    _notes = new ListCollectionView(Site.Notes);
                    _notes.Filter = InvalidFilter;
                    Site.Notes.PropertyChanged += site_PropertyChanged;
                }
                return _notes;
            }
        }
        private ListCollectionView _notes;

        public ListCollectionView Pcds
        {
            get
            {
                if (_pcds == null)
                {
                    _pcds = new ListCollectionView(Site.Pcds);
                    _pcds.Filter = InvalidFilter;
                    Site.Pcds.PropertyChanged += site_PropertyChanged;
                }
                return _pcds;
            }
        }
        private ListCollectionView _pcds;

        public ListCollectionView Phones
        {
            get
            {
                if (_phones == null)
                {
                    _phones = new ListCollectionView(Site.Phones);
                    _phones.Filter = InvalidFilter;
                    Site.Phones.PropertyChanged += site_PropertyChanged;
                }
                return _phones;
            }
        }
        private ListCollectionView _phones;

        public ListCollectionView Rentals
        {
            get
            {
                if (_rentals == null)
                {
                    _rentals = new ListCollectionView(Site.Rentals);
                    _rentals.Filter = InvalidFilter;
                    Site.Rentals.PropertyChanged += site_PropertyChanged;
                }
                return _rentals;
            }
        }
        private ListCollectionView _rentals;

        public ListCollectionView Services
        {
            get
            {
                if (_services == null)
                {
                    _services = new ListCollectionView(Site.Services);
                    _services.Filter = InvalidFilter;
                    Site.Services.PropertyChanged += site_PropertyChanged;
                }
                return _services;
            }
        }
        private ListCollectionView _services;

        public ListCollectionView Units
        {
            get
            {
                if (_units == null)
                {
                    _units = new ListCollectionView(Site.Units);
                    _units.Filter = InvalidFilter;
                    Site.Units.PropertyChanged += site_PropertyChanged;
                }
                return _units;
            }
        }
        private ListCollectionView _units;

        public ListCollectionView History
        {
            get
            {
                if (_history == null)
                {
                    _history = new ListCollectionView(Site.History);
                    _history.Filter = InvalidFilter;
                    Site.History.PropertyChanged += site_PropertyChanged;
                }
                return _history;
            }
        }
        private ListCollectionView _history;

        public string Output { get; set; }

        public int TotalOccupiedRentals => Site.DataImporter.Rentals.Count(r => !r.IsReserved);
        public int TotalReservedRentals => Site.DataImporter.Rentals.Count(r => r.IsReserved);
        public int TotalVacantUnits => Site.DataImporter.Units.Count(r => string.IsNullOrWhiteSpace(r.CommonKey));
        public int TotalBusinessAccounts => Site.DataImporter.Accounts.Count(r => r.AccountClass == 2);
        public int TotalPersonalAccounts => Site.DataImporter.Accounts.Count(r => r.AccountClass == 1);
        public int TotalPrimaryContacts => Site.DataImporter.Contacts.Count(r => r.ContactType == 1);
        public int TotalSecondaryContacts => Site.DataImporter.Contacts.Count(r => r.ContactType == 2);
        public int TotalInsured => Site.DataImporter.Rentals.Count(r => r.Insurance.HasValue);
        public decimal TotalSecurityDepositAmount => Site.DataImporter.Rentals.Sum(r => r.SecurityDeposit ?? 0);
        public decimal TotalSecurityDeposits => Site.DataImporter.Rentals.Count(r => r.SecurityDeposit.HasValue && r.SecurityDeposit != 0);
        public decimal TotalFeeAmount => Site.DataImporter.Rentals.Sum(r => r.FeesBalance ?? 0);
        public decimal TotalFees => Site.DataImporter.Rentals.Count(r => r.FeesBalance.HasValue && r.FeesBalance != 0);
        public decimal TotalCredits => Site.DataImporter.Rentals.Sum(r => r.CreditsBalance ?? 0);
        public int TotalOverlocked => Site.DataImporter.Rentals.Count(r => r.OverlockFlag == true);
        public int TotalZeroDolarRentRateOccupied => Site.DataImporter.Units.Count(r => !string.IsNullOrWhiteSpace(r.CommonKey) && r.RentRate == 0);
        public int TotalZeroDolarRentRateVacant => Site.DataImporter.Units.Count(r => string.IsNullOrWhiteSpace(r.CommonKey) && r.RentRate == 0);
        public decimal TotalVacantRentRates => Site.DataImporter.Units.Where(r => string.IsNullOrWhiteSpace(r.CommonKey)).Sum(r => r.RentRate ?? 0);
        public decimal TotalOccupiedRentRates => Site.DataImporter.Units.Where(r => !string.IsNullOrWhiteSpace(r.CommonKey)).Sum(r => r.RentRate ?? 0);
        public decimal TotalGrossRentRates => Site.DataImporter.Units.Sum(r => r.RentRate ?? 0);

        public IExporter Exporter => _exporter ?? (_exporter = new OracleExporter());
        private IExporter _exporter;

        public ListCollectionView ImportErrorsCollectionView
        {
            get
            {
                if (_importErrorsCollectionView == null)
                {
                    _importErrorsCollectionView = new ListCollectionView(this.Site.DataImporter.ImportErrors);
                    Contract.Assert(_importErrorsCollectionView.GroupDescriptions != null);
                    _importErrorsCollectionView.GroupDescriptions.Add(new PropertyGroupDescription("ErrorType"));
                    _importErrorsCollectionView.SortDescriptions.Add(new SortDescription("ErrorType", ListSortDirection.Ascending));
                }
                return _importErrorsCollectionView;
            }
        }
        private ListCollectionView _importErrorsCollectionView;

        public bool HasImportErrors => this.Site.DataImporter.ImportErrors.Any();

        public int SelectedTabIndex { get; set; }

        public bool IsValid
        {
            get
            {
                return (Site.AutoPays.IsValid &&
                        Site.Accounts.IsValid &&
                        Site.Addresses.IsValid &&
                        Site.Contacts.IsValid &&
                        Site.Notes.IsValid &&
                        Site.Pcds.IsValid &&
                        Site.Phones.IsValid &&
                        Site.Rentals.IsValid &&
                        Site.Services.IsValid &&
                        Site.Units.IsValid &&
                        Site.History.IsValid);
            }
        }

        private bool _filteringInvalid = true;
        public bool FilteringInvalid
        {
            get { return _filteringInvalid; }
            set
            {
                _filteringInvalid = value;
                if (_filteringInvalid)
                {
                    ApplyInvalidFilter();
                }
                else
                {
                    RemoveInvalidFilter();
                }
            }
        }

        public bool InvalidFilter(object item)
        {
            var clg_Object = item as CLG_Object;
            if (clg_Object == null)
                return false;

            return !clg_Object.Filtered;
        }

        public void ApplyInvalidFilter()
        {
            Site.AutoPays.SetFiltered();
            Site.Accounts.SetFiltered();
            Site.Addresses.SetFiltered();
            Site.Contacts.SetFiltered();
            Site.Notes.SetFiltered();
            Site.Pcds.SetFiltered();
            Site.Phones.SetFiltered();
            Site.Rentals.SetFiltered();
            Site.Services.SetFiltered();
            Site.Units.SetFiltered();
            Site.History.SetFiltered();

            RefreshListCollectionViews();
        }

        public void RemoveInvalidFilter()
        {
            Site.AutoPays.ClearFiltered();
            Site.Accounts.ClearFiltered();
            Site.Addresses.ClearFiltered();
            Site.Contacts.ClearFiltered();
            Site.Notes.ClearFiltered();
            Site.Pcds.ClearFiltered();
            Site.Phones.ClearFiltered();
            Site.Rentals.ClearFiltered();
            Site.Services.ClearFiltered();
            Site.Units.ClearFiltered();
            Site.History.ClearFiltered();

            RefreshListCollectionViews();
        }

        private void RefreshListCollectionViews()
        {
            AutoPays.Refresh();
            Accounts.Refresh();
            Addresses.Refresh();
            Contacts.Refresh();
            Notes.Refresh();
            Pcds.Refresh();
            Phones.Refresh();
            Rentals.Refresh();
            Services.Refresh();
            Units.Refresh();
            History.Refresh();
        }

        public void ExportSite(bool cstageOnly = false)
        {
            Exporter.Export(Site.SiteId,
                 new List<ICLG_Collection>()
                 {
                    Site.DataImporter.AutoPays,
                    Site.DataImporter.Accounts,
                    Site.DataImporter.Addresses,
                    Site.DataImporter.Contacts,
                    Site.DataImporter.Phones,
                    Site.DataImporter.Pcds,
                    Site.DataImporter.Rentals,
                    Site.DataImporter.Notes,
                    Site.DataImporter.History,
                    Site.DataImporter.Services,
                    Site.DataImporter.Units
                 }, cstageOnly);

            WriteToOutputFile();

            if (!Exporter.ExportFailed)
                SummaryOfSummaries.AddToSummaries(Site.SiteNumber);

            _killSiteCommand?.RaiseCanExecuteChanged();
            HasExported = true;
        }

        private void WriteToOutputFile()
        {
            if (!Directory.Exists(Settings.Default.OutputDirectory))
                Directory.CreateDirectory(Settings.Default.OutputDirectory);

            File.WriteAllText(Path.Combine(Settings.Default.OutputDirectory, $"{DateTime.Now.ToString("yyyyMMdd")}_{Site.SiteNumber}_Output.txt"), Exporter.Output);
        }

        void site_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsValid")
                OnPropertyChanged("IsValid");
        }

        private ICommand _exportToCstageCommand;

        public ICommand ExportToCstageCommand => _exportToCstageCommand ?? (_exportToCstageCommand = new AwaitableDelegateCommand(ExportToCstage, () => !Exporter.ExportRunning));

        private async Task ExportToCstage()
        {
            await Task.Run(() => ExportSite(true));
        }

        public ICommand KillSiteCommand => _killSiteCommand ?? (_killSiteCommand = new AwaitableDelegateCommand(KillSite, CanKillSite));
        private AwaitableDelegateCommand _killSiteCommand;

        public SiteKiller SiteKiller => _siteKiller ?? (_siteKiller = new SiteKiller());
        private SiteKiller _siteKiller;

        private async Task KillSite()
        {
            var dialogResult = MessageBox.Show($"You are about to kill site {Site.SiteNumber}.  This cannot be undone.  Are you sure?", "Kill Site", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
            if (dialogResult == MessageBoxResult.Cancel) return;

            SiteKilled = true;
            await Task.Run(() => SiteKiller.KillSite(Site.SiteId));
        }

        private bool CanKillSite()
        {
            return HasExported;
        }

        public void RunPostExport()
        {
            if (!HasExported || SiteKilled || Exporter.ExportRunning || Exporter.ExportFailed) return;
            ArchiveData();
        }

        public void ArchiveData()
        {
            try
            {
                Archiver.ArchiveData(Site.SiteNumber, Site.DataPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        [ContractInvariantMethod]
        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "Required for code contracts.")]
        private void ObjectInvariant()
        {
            Contract.Invariant(Site != null);
        }


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event 
        protected void OnPropertyChanged([CallerMemberName]string name = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }


        #endregion
    }
}

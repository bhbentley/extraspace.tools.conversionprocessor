﻿using System.Threading.Tasks;
using System.Windows.Input;
using ConversionProcessorWPF.DataProviders;
using ConversionProcessorWPF.Utilities;
using ConversionProcessorWPF.View.Commands;
using PropertyChanged;

namespace ConversionProcessorWPF.ViewModel
{
    [ImplementPropertyChanged]
    public class SiteKillerViewModel
    {
        public long? SiteId { get; set; }
        public string SiteNumber { get; set; }
        public string SiteName { get; set; }
        public string SiteStatus { get; set; }
        public string ErrorMessage { get; set; }
        public bool? FinancialClose { get; set; }

        private bool _killSiteRunning;

        public SiteKiller SiteKiller => _siteKiller ?? (_siteKiller = new SiteKiller());
        private SiteKiller _siteKiller;

        public ICommand CheckSiteCommand => _checkSiteCommand ?? (_checkSiteCommand = new DelegateCommand(CheckSite, this.CanCheckSite));
        private DelegateCommand _checkSiteCommand;

        private void CheckSite()
        {
            ErrorMessage = string.Empty;

            var siteInfo = SiteDataProvider.GetSiteInfo(SiteNumber);
            SiteId = siteInfo.Id;
            SiteName = siteInfo.Name;
            SiteStatus = siteInfo.Status;
            FinancialClose = siteInfo.FinancialClose;

            if (SiteStatus == "Active")
                ErrorMessage = "Cannot kill site because site is active.\r\n";

            if (FinancialClose == true)
                ErrorMessage += "Cannot kill site because financial close is true.";
        }

        private bool CanCheckSite()
        {
            return !string.IsNullOrWhiteSpace(SiteNumber);
        }

        public ICommand KillSiteCommand => _killSiteCommand ?? (_killSiteCommand = new AwaitableDelegateCommand(KillSite, CanKillSite));
        private AwaitableDelegateCommand _killSiteCommand;

        private async Task KillSite()
        {
            if (!SiteId.HasValue) return;
            _killSiteRunning = true;
            await Task.Run(() => SiteKiller.KillSite(SiteId.Value));
            _killSiteRunning = false;
        }

        private bool CanKillSite()
        {
            return !_killSiteRunning &&
                   SiteId.HasValue &&
                   SiteStatus != "Active" &&
                   FinancialClose == false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ConversionProcessorWPF.ImportTemplates
{
    internal static class ImportTemplateFactory
    {
        private static IEnumerable<Type> GetImplementations()
        {
            return Assembly.GetExecutingAssembly().GetTypes().Where(t => t.IsClass && !t.IsAbstract&& t.IsSubclassOf(typeof(ImportTemplate)));
        }

        public static bool IsImplemented(string conversionType)
        {
            return GetImplementations().Any(t => ((ConversionTypeAttribute) t.GetCustomAttributes(typeof (ConversionTypeAttribute), false).FirstOrDefault())?.Value == conversionType);
        }

        public static string[] GetImplementedConversionTypes()
        {
            return GetImplementations().Select(t => ((ConversionTypeAttribute)t.GetCustomAttributes(typeof(ConversionTypeAttribute), false).FirstOrDefault())?.Value).ToArray();
        }

        public static ImportTemplate Create(string conversionType, params object[] constructorArgs)
        {
            var type = GetImplementations().FirstOrDefault(t => ((ConversionTypeAttribute)t.GetCustomAttributes(typeof(ConversionTypeAttribute), false).FirstOrDefault())?.Value == conversionType);

            if (type == null) throw new Exception("Cannot create Import Template for " + conversionType);

            return Activator.CreateInstance(type, constructorArgs) as ImportTemplate;
        }
    }
}

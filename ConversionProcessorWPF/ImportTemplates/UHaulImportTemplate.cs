﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ClosedXML.Excel;
using ConversionProcessorWPF.Model;
using ConversionProcessorWPF.Utilities;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace ConversionProcessorWPF.ImportTemplates
{
    [ConversionType("UHaul")]
    public class UHaulImportTemplate : ImportTemplate
    {

        public UHaulImportTemplate(string dataPath, long siteId, string siteNumber) : base(dataPath, siteId, siteNumber)
        {
            Contract.Requires(!string.IsNullOrEmpty(dataPath));
        }

        protected override void LoadTemplateData()
        {
            try
            {
                var allAccountInfo = LoadAccountInfo();
                LoadAccounts(allAccountInfo);
                LoadUnits(allAccountInfo);
                LoadRentals(allAccountInfo);
                LoadEmail();
            }
            catch (Exception ex)
            {
                ImportErrors.AddImportError("Unexpected Error", "Error loading data", ex.Message);
            }
        }

        private List<AccountInfo> LoadAccountInfo()
        {
            Contract.Ensures(Contract.Result<List<AccountInfo>>() != null);

            var allAccountInfo = new List<AccountInfo>();
            var rows = GetCsvData(Path.Combine(_dataPath, "CustomerRentRoll.xls"), "\t");
            var columnIndexMap = BuildColumnIndexMap(rows);


            foreach (var row in rows)
            {
                // make sure the row has the same number of fields as the header.
                if (row.Count() != columnIndexMap.Count())
                {
                    if (!row.All(string.IsNullOrWhiteSpace))
                    {
                        ImportErrors.AddIncorrectColumnCountImportError("Account", columnIndexMap.Count(), row.Count(), string.Join(" ", row));
                    }
                    continue;
                }

                var account = new AccountInfo
                {
                    AccountName = row.GetValueAt(columnIndexMap["Last Name"]) + ", " + row.GetValueAt(columnIndexMap["First Name"]),
                    UnitNumber = row.GetValueAt(columnIndexMap["Room"]).RemoveApostrophes(),
                    FirstName = row.GetValueAt(columnIndexMap["First Name"]),
                    LastName = row.GetValueAt(columnIndexMap["Last Name"]),
                    RentalStartDate = row.GetValueAt(columnIndexMap["Move-In"]).ToNullableDate(),
                    Rate = row.GetValueAt(columnIndexMap["Rate"]).ToNullableDecimal(),
                    PaidThruDate = row.GetValueAt(columnIndexMap["Paid Through"]).ToNullableDate(),
                    BalanceDue = row.GetValueAt(columnIndexMap["Balance Due"]).ToNullableDecimal(),
                    Address = row.GetValueAt(columnIndexMap["Address"]),
                    City = row.GetValueAt(columnIndexMap["City"]),
                    State = row.GetValueAt(columnIndexMap["State"]),
                    Zip = row.GetValueAt(columnIndexMap["Zip"]),
                    Phone = row.GetValueAt(columnIndexMap["Phone"])
                };

                allAccountInfo.Add(account);
            }

            LoadGatePins(allAccountInfo);

            return allAccountInfo;
        }

        private void LoadAccounts(List<AccountInfo> allAccountInfo)
        {
            Contract.Requires(allAccountInfo != null);

            // Find all of the distinct accounts and get the unit Id's that go with them.
            var distinctAccounts = allAccountInfo.GroupBy(r => new
            {
                r.AccountName,
                r.FirstName,
                r.LastName,
                r.Address,
            }).Select((g, index) => new
            {
                CommonKey = (index + 100).ToString(),  // Start at 100
                g.Key.AccountName,
                ContactKey = (index + 1000).ToString(),  // Start at 1000 so as to not confuse this number with CommonKey
                g.Key.FirstName,
                g.Key.LastName,
                g.Key.Address,
                City = g.Select(gr => gr.City).FirstOrDefault(),
                State = g.Select(gr => gr.State).FirstOrDefault(),
                Zip = g.Select(gr => gr.Zip).FirstOrDefault(),
                Phone = g.Select(gr => gr.Phone).FirstOrDefault(),
                GatePin = g.Select(gr => gr.GatePin).FirstOrDefault(),
                UnitNumbers = g.Select(gr => gr.UnitNumber).Distinct().ToList()
            }).ToList();

            foreach (var a in distinctAccounts)
            {
                foreach (var unitNumber in a.UnitNumbers)
                {
                    if (!_accountCommonKeyMap.ContainsKey(unitNumber))
                        _accountCommonKeyMap.Add(unitNumber, a.CommonKey);
                }

                Accounts.Add(new Account
                {
                    CommonKey = a.CommonKey,
                    AccountName = a.AccountName,
                    AccountClass = 1,
                    GatePin = a.GatePin,
                    SiteId = _siteId
                });

                Contacts.Add(new Contact
                {
                    FirstName = a.FirstName,
                    LastName = a.LastName,
                    ContactType = 1,    // 1 indicates to Siphon that this is a primary contact.
                    CommonKey = a.CommonKey,
                    ContactKey = a.ContactKey,
                });

                AddAddress(a.Address, null, null, a.City, a.State, null, a.Zip, a.ContactKey, 1);


                AddPhone(a.Phone, a.ContactKey, PhoneType.Home);
            }

        }

        private void LoadGatePins(List<AccountInfo> allAccountInfo)
        {
            Contract.Requires(allAccountInfo != null);

            var rows = GetCsvData(Path.Combine(_dataPath, "SecurityAccess.xls"), "\t");
            var columnIndexMap = BuildColumnIndexMap(rows);

            foreach (var row in rows)
            {
                // make sure the row has the same number of fields as the header.
                if (row.Count() != columnIndexMap.Count())
                {
                    if (!row.All(string.IsNullOrWhiteSpace))
                    {
                        ImportErrors.AddIncorrectColumnCountImportError("Gate Pin", columnIndexMap.Count(), row.Count(), string.Join(" ", row));
                    }
                    continue;
                }


                var unitNumber = row.GetValueAt(columnIndexMap["Room"]).RemoveApostrophes();
                var account = allAccountInfo.FirstOrDefault(u => u.UnitNumber == unitNumber);

                var gatePin = row.GetValueAt(columnIndexMap["Access Code"]);

                if (account == null)
                {
                    ImportErrors.AddImportError("Gate Pin Skipped", "Account not found.", $"Unit Number: {unitNumber}  Gate Pin: {gatePin}");
                    continue;
                }

                account.GatePin = gatePin.RemoveApostrophes();
            }
        }

        private void LoadUnits(List<AccountInfo> allAccountInfo)
        {
            Contract.Requires(allAccountInfo != null);

            var rows = GetCsvData(Path.Combine(_dataPath, "WalkAround.xls"), "\t");
            var columnIndexMap = BuildColumnIndexMap(rows);

            var tempUnits = new List<Unit>();

            foreach (var row in rows)
            {
                // make sure the row has the same number of fields as the header.
                if (row.Count() != columnIndexMap.Count())
                {
                    if (!row.All(string.IsNullOrWhiteSpace))
                    {
                        ImportErrors.AddIncorrectColumnCountImportError("Unit", columnIndexMap.Count(), row.Count(), string.Join(" ", row));
                    }
                    continue;
                }

                var unit = new Unit()
                {
                    UnitNumber = row.GetValueAt(columnIndexMap["Room"]).RemoveApostrophes(),
                    RateEffDate = DateTime.Today
                };

                var dimensions = row.GetValueAt(columnIndexMap["Size"]);
                unit.Width = GetWidth(dimensions);
                unit.Depth = GetDepth(dimensions);

                unit.CommonKey = GetCommonKey(unit.UnitNumber);
                unit.RentRate = allAccountInfo.FirstOrDefault(a => a.UnitNumber == unit.UnitNumber)?.Rate ?? 0;
                unit.StreetRate = unit.RentRate;
                unit.RentalUnitKey = unit.UnitNumber;

                tempUnits.Add(unit);
            }

            UpdateDamagedUnitStatus(tempUnits);
            UpdateVacantUnitRates(tempUnits);

            foreach (var tempUnit in tempUnits)
            {
                Units.Add(tempUnit);
            }
        }

        private void UpdateDamagedUnitStatus(List<Unit> units)
        {
            Contract.Requires(units != null);

            var rows = GetCsvData(Path.Combine(_dataPath, "DamagedRooms.xls"), "\t");
            var columnIndexMap = BuildColumnIndexMap(rows);

            foreach (var row in rows)
            {
                // make sure the row has the same number of fields as the header.
                if (row.Count() != columnIndexMap.Count())
                {
                    if (!row.All(string.IsNullOrWhiteSpace))
                    {
                        ImportErrors.AddIncorrectColumnCountImportError("Damaged Unit", columnIndexMap.Count(), row.Count(), string.Join(" ", row));
                    }
                    continue;
                }

                var unitNumber = row.GetValueAt(columnIndexMap["Unit"]);
                var unit = units.FirstOrDefault(u => u.UnitNumber == unitNumber);

                if (unit == null)
                {
                    ImportErrors.AddImportError("Damaged Unit Skipped", "Unit not found.", $"Unit Number: {unitNumber}  Unit Number: {unitNumber}");
                    continue;
                }

                unit.Status = 5;
                unit.RentRate = row.GetValueAt(columnIndexMap["Rate"]).ToNullableDecimal();
                unit.StreetRate = unit.RentRate;
            }
        }

        private void UpdateVacantUnitRates(List<Unit> units)
        {
            Contract.Requires(units != null);

            var rows = GetCsvData(Path.Combine(_dataPath, "RoomsAvailable.xls"), "\t");
            var columnIndexMap = BuildColumnIndexMap(rows);

            foreach (var row in rows)
            {
                // make sure the row has the same number of fields as the header.
                if (row.Count() != columnIndexMap.Count())
                {
                    if (!row.All(string.IsNullOrWhiteSpace))
                    {
                        ImportErrors.AddIncorrectColumnCountImportError("Vacant Unit", columnIndexMap.Count(), row.Count(), string.Join(" ", row));
                    }
                    continue;
                }

                var unitNumber = row.GetValueAt(columnIndexMap["Rooms Available"])?.Replace("_", string.Empty);
                var unit = units.FirstOrDefault(u => u.UnitNumber == unitNumber);

                if (unit == null)
                {
                    ImportErrors.AddImportError("Vacant Unit Skipped", "Unit not found.", $"Unit Number: {unitNumber}  Unit Number: {unitNumber}");
                    continue;
                }

                unit.RentRate = row.GetValueAt(columnIndexMap["Rate"]).ToNullableDecimal();
                unit.StreetRate = unit.RentRate;
            }
        }

        private void LoadRentals(List<AccountInfo> allAccountInfo)
        {
            var tempRentals = new List<Rental>();

            foreach (var accountInfo in allAccountInfo)
            {
                var rental = new Rental
                {
                    CommonKey = GetCommonKey(accountInfo.UnitNumber),
                    RentalStartDate = accountInfo.RentalStartDate,
                    PaidThruDate = accountInfo.PaidThruDate,
                    RentalUnitKey = Units.FirstOrDefault(u => u.UnitNumber == accountInfo.UnitNumber)?.RentalUnitKey
                };

                tempRentals.Add(rental);
            }

            LoadInsurance(tempRentals);
            LoadSecurityDeposits(tempRentals);
            LoadCreditsAndFees(tempRentals);

            foreach (var tempRental in tempRentals)
            {
                Rentals.Add(tempRental);
            }
        }

        private void LoadInsurance(List<Rental> rentals)
        {
            Contract.Requires(rentals != null);

            var rows = GetCsvData(Path.Combine(_dataPath, "CustomersWithInsuranceCoverage.xls"), "\t");
            var columnIndexMap = BuildColumnIndexMap(rows);

            foreach (var row in rows)
            {
                // make sure the row has the same number of fields as the header.
                if (row.Count() != columnIndexMap.Count())
                {
                    if (!row.All(string.IsNullOrWhiteSpace))
                    {
                        ImportErrors.AddIncorrectColumnCountImportError("Insurance", columnIndexMap.Count(), row.Count(), string.Join(" ", row));
                    }
                    continue;
                }


                var unitNumber = row.GetValueAt(columnIndexMap["Room"]).RemoveApostrophes();
                var rentalUnitKey = Units.FirstOrDefault(u => u.UnitNumber == unitNumber)?.RentalUnitKey;
                var rental = rentals.FirstOrDefault(r => r.RentalUnitKey == rentalUnitKey);

                var description = row.GetValueAt(columnIndexMap["Description"]);

                decimal? coverageAmount = null;
                if (!string.IsNullOrEmpty(description))
                    coverageAmount = Regex.Match(description, @"\$[0-9\,]+").Value.ToNullableDecimal();

                if (rental == null)
                {
                    ImportErrors.AddImportError("Insurance Skipped", "Rental not found.", $"Unit Number: {unitNumber}  Insurance Coverage Amount: {coverageAmount}");
                    continue;
                }

                rental.Insurance = GetInsuranceId(coverageAmount, unitNumber);
            }
        }

        private void LoadSecurityDeposits(List<Rental> rentals)
        {
            Contract.Requires(rentals != null);

            var rows = GetCsvData(Path.Combine(_dataPath, "SecurityDeposits.xls"), "\t");
            var columnIndexMap = BuildColumnIndexMap(rows);

            foreach (var row in rows)
            {
                // make sure the row has the same number of fields as the header.
                if (row.Count() != columnIndexMap.Count())
                {
                    if (!row.All(string.IsNullOrWhiteSpace))
                    {
                        ImportErrors.AddIncorrectColumnCountImportError("Security Deposit", columnIndexMap.Count(), row.Count(), string.Join(" ", row));
                    }
                    continue;
                }

                var unitNumber = row.GetValueAt(columnIndexMap["Room"]).RemoveApostrophes();
                var rentalUnitKey = Units.FirstOrDefault(u => u.UnitNumber == unitNumber)?.RentalUnitKey;
                var rental = rentals.FirstOrDefault(r => r.RentalUnitKey == rentalUnitKey);
                var amount = row.GetValueAt(columnIndexMap["Amount"]).ToNullableDecimal();

                if (rental == null)
                {
                    ImportErrors.AddImportError("Security Deposit Skipped", "Rental not found.", $"Unit Number: {unitNumber}  Security Deposit Amount: {amount}");
                    continue;
                }

                rental.SecurityDeposit = amount;
            }
        }

        private void LoadCreditsAndFees(List<Rental> rentals)
        {
            Contract.Requires(rentals != null);

            LoadCreditsAndFees("CollectionWorksheet1.xls", rentals);
            LoadCreditsAndFees("CollectionWorksheet2.xls", rentals);
        }

        private void LoadCreditsAndFees(string fileName, List<Rental> rentals)
        {
            Contract.Requires(rentals != null);

            var rows = GetCsvData(Path.Combine(_dataPath, fileName), "\t");
            var columnIndexMap = BuildColumnIndexMap(rows);

            foreach (var row in rows)
            {
                // make sure the row has the same number of fields as the header.
                if (row.Count() != columnIndexMap.Count())
                {
                    if (!row.All(string.IsNullOrWhiteSpace))
                    {
                        ImportErrors.AddIncorrectColumnCountImportError("Collection Worksheet Item", columnIndexMap.Count(), row.Count(), string.Join(" ", row));
                    }
                    continue;
                }

                var unitNumber = row.GetValueAt(columnIndexMap["Room"]).RemoveApostrophes();
                var rentalUnitKey = Units.FirstOrDefault(u => u.UnitNumber == unitNumber)?.RentalUnitKey;
                var rental = rentals.FirstOrDefault(r => r.RentalUnitKey == rentalUnitKey);

                if (rental == null)
                {
                    ImportErrors.AddImportError("Collection Worksheet Item", "Rental not found.", $"Unit Number: {unitNumber}");
                    continue;
                }

                rental.CreditsBalance = row.GetValueAt(columnIndexMap["Credit"]).ToNullableDecimal();
                rental.FeesBalance = row.GetValueAt(columnIndexMap["Fees"]).ToNullableDecimal();
            }
        }

        private void LoadEmail()
        {
            var rows = GetCsvData(Path.Combine(_dataPath, "EmailCustomerList.xls"), "\t");
            var columnIndexMap = BuildColumnIndexMap(rows);

            foreach (var row in rows)
            {
                // make sure the row has the same number of fields as the header.
                if (row.Count() != columnIndexMap.Count())
                {
                    if (!row.All(string.IsNullOrWhiteSpace))
                    {
                        ImportErrors.AddIncorrectColumnCountImportError("Email", columnIndexMap.Count(), row.Count(), string.Join(" ", row));
                    }
                    continue;
                }

                var unitNumber = row.GetValueAt(columnIndexMap["Unit"]).RemoveApostrophes();
                var rentalUnitKey = Units.FirstOrDefault(u => u.UnitNumber == unitNumber)?.RentalUnitKey;
                var rental = Rentals.FirstOrDefault(r => r.RentalUnitKey == rentalUnitKey);
                var email = row.GetValueAt(columnIndexMap["Customer e-mail"]);
                var contact = Contacts.FirstOrDefault(c => c.CommonKey == rental?.CommonKey);

                if (contact == null)
                {
                    ImportErrors.AddImportError("Email Skipped", "Contact not found.", $"Unit Number: {unitNumber}  Email: {email}");
                    continue;
                }

                contact.EmailAddress = email;
            }
        }

        #region Helper Classes

        private class AccountInfo
        {
            public string AccountName { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public DateTime? RentalStartDate { get; set; }
            public string UnitNumber { get; set; }
            public decimal? Rate { get; set; }
            public DateTime? PaidThruDate { get; set; }
            public decimal? BalanceDue { get; set; }
            public string Address { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Zip { get; set; }
            public string Phone { get; set; }
            public string GatePin { get; set; }
        }

        #endregion
    }
}

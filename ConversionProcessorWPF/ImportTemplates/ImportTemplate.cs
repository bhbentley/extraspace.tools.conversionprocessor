﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using ConversionProcessorWPF.Model;
using ConversionProcessorWPF.Properties;
using ConversionProcessorWPF.Utilities;
using CsvHelper;
using CsvHelper.Configuration;
using PropertyChanged;

namespace ConversionProcessorWPF.ImportTemplates
{
    [ImplementPropertyChanged]
    public abstract class ImportTemplate
    {
        protected string _dataPath;
        protected long _siteId;
        protected string _siteNumber;
        protected const string DefaultServiceId = "999001";
        protected readonly Dictionary<string, string> _accountCommonKeyMap = new Dictionary<string, string>();
        private readonly Regex _dimensionsRegex = new Regex(@"([\d\.]+)(x|X)?");

        protected ImportTemplate(string dataPath, long siteId, string siteNumber)
        {
            Contract.Requires(!string.IsNullOrEmpty(dataPath));

            _dataPath = dataPath;
            _siteId = siteId;
            _siteNumber = siteNumber;
        }

        private AutoPayCollection _autoPays;
        public AutoPayCollection AutoPays
        {
            get
            {
                Contract.Ensures(Contract.Result<AutoPayCollection>() != null);
                return (_autoPays ?? (_autoPays = new AutoPayCollection()));
            }
        }

        private AccountCollection _accounts;
        public AccountCollection Accounts
        {
            get
            {
                Contract.Ensures(Contract.Result<AccountCollection>() != null);
                return (_accounts ?? (_accounts = new AccountCollection()));
            }
        }

        private AddressCollection _addresses;
        public AddressCollection Addresses
        {
            get
            {
                Contract.Ensures(Contract.Result<AddressCollection>() != null);
                return (_addresses ?? (_addresses = new AddressCollection()));
            }
        }

        private ContactCollection _contacts;
        public ContactCollection Contacts
        {
            get
            {
                Contract.Ensures(Contract.Result<ContactCollection>() != null);
                return (_contacts ?? (_contacts = new ContactCollection()));
            }
        }

        private NoteCollection _notes;
        public NoteCollection Notes
        {
            get
            {
                Contract.Ensures(Contract.Result<NoteCollection>() != null);
                return (_notes ?? (_notes = new NoteCollection()));
            }
        }

        private PcdCollection _pcds;
        public PcdCollection Pcds
        {
            get
            {
                Contract.Ensures(Contract.Result<PcdCollection>() != null);
                return (_pcds ?? (_pcds = new PcdCollection()));
            }
        }

        private PhoneCollection _phones;
        public PhoneCollection Phones
        {
            get
            {
                Contract.Ensures(Contract.Result<PhoneCollection>() != null);
                return (_phones ?? (_phones = new PhoneCollection()));
            }
        }

        private RentalCollection _rentals;
        public RentalCollection Rentals
        {
            get
            {
                Contract.Ensures(Contract.Result<RentalCollection>() != null);
                return (_rentals ?? (_rentals = new RentalCollection()));
            }
        }

        private ServiceCollection _services;
        public ServiceCollection Services
        {
            get
            {
                Contract.Ensures(Contract.Result<ServiceCollection>() != null);
                return (_services ?? (_services = GetServices()));
            }
        }

        private UnitCollection _units;
        public UnitCollection Units
        {
            get
            {
                Contract.Ensures(Contract.Result<UnitCollection>() != null);
                return (_units ?? (_units = new UnitCollection()));
            }
        }

        private HistoryCollection _history;
        public HistoryCollection History
        {
            get
            {
                Contract.Ensures(Contract.Result<HistoryCollection>() != null);
                return (_history ?? (_history = new HistoryCollection()));
            }
        }

        public ImportErrorCollection ImportErrors => _importErrors ?? (_importErrors = new ImportErrorCollection());
        private ImportErrorCollection _importErrors;

        public void LoadData()
        {
            LoadTemplateData();
            ValidateTemplateData();
            WriteLogToFile();
        }

        protected abstract void LoadTemplateData();

        private void WriteLogToFile()
        {
            if (!Directory.Exists(Settings.Default.OutputDirectory))
                Directory.CreateDirectory(Settings.Default.OutputDirectory);

            var fileName = Path.Combine(Settings.Default.OutputDirectory, $"{DateTime.Now.ToString("yyyyMMdd")}_{_siteNumber}_Log.txt");
            if (File.Exists(fileName))
                File.Delete(fileName);

            using (var writer = File.CreateText(fileName))
            {

                foreach (var errorGroup in ImportErrors.GroupBy(x => x.ErrorType).OrderBy(x => x.Key))
                {
                    writer.WriteLine($"{errorGroup.Key} {errorGroup.Count()} Item(s)");

                    foreach (var error in errorGroup)
                    {
                        writer.WriteLine($"\t{error.Error}: {error.Description}");
                    }

                    writer.WriteLine();
                }
            }
        }

        #region Validation Methods

        protected virtual void ValidateTemplateData()
        {
            // Continue to loop until all the validate methods return true.
            while (!(ValidateUniqueRentalUnitKeyPerRental() &&
                     ValidateNoDuplicateUnitNumbers() &&
                     ValidatePhonesWithNoContacts() &&
                     ValidatePrimaryContactsWithNoPhones() &&
                     ValidateAccountsWithNoRentals() &&
                     ValidateRentalsWithNoAccount() &&
                     ValidateAddressesWithNoContacts() &&
                     ValidatePrimaryContactsWithNoAddresses() &&
                     ValidateContactsWithNoAccount() &&
                     ValidateSinglePrimaryContact() &&
                     ValidateDuplicateContacts() &&
                     ValidateNoDuplicateAccounts() &&
                     ValidateAutoPaysWithNoAccount()
                     ))
            {
            }
        }

        internal bool ValidateUniqueRentalUnitKeyPerRental()
        {
            var duplicateRentals = Rentals.GroupBy(r => r.RentalUnitKey)
                                           .Where(g => g.Count() > 1)
                                           .Select(g => new { g.Key, Rentals = g.ToList() }).ToList();

            foreach (var duplicateRental in duplicateRentals)
            {
                for (int i = 0; i < duplicateRental.Rentals.Count(); i++)
                {
                    // Leave the first one.  Remove all the rentals after that.
                    if (i == 0) continue;
                    Rentals.Remove(duplicateRental.Rentals[i]);
                    ImportErrors.AddImportError("Duplicate Unit ID", "Unit tied to more than one rental", "Rental Unit Key: " + duplicateRental.Rentals[i].RentalUnitKey);
                }
            }

            return !duplicateRentals.Any();
        }

        internal bool ValidateNoDuplicateUnitNumbers()
        {
            var duplicateUnitNumbers = Units.GroupBy(r => r.UnitNumber)
                                           .Where(g => g.Count() > 1)
                                           .Select(g => new { g.Key, Units = g.ToList() }).ToList();

            foreach (var duplicateUnit in duplicateUnitNumbers)
            {
                for (int i = 0; i < duplicateUnit.Units.Count(); i++)
                {
                    // Leave the first one.  Remove all the units after that.
                    if (i == 0) continue;
                    Units.Remove(duplicateUnit.Units[i]);
                    ImportErrors.AddImportError("Duplicate Unit Number", "Duplicate Unit Number", duplicateUnit.Units[i].UnitNumber);
                }
            }

            return !duplicateUnitNumbers.Any();
        }

        internal bool ValidatePhonesWithNoContacts()
        {
            var phonesWithoutContacts = Phones.Where(p => string.IsNullOrWhiteSpace(p.ContactKey) || Contacts.All(c => c.ContactKey != p.ContactKey)).ToList();

            foreach (var phone in phonesWithoutContacts)
            {
                Phones.Remove(phone);
                ImportErrors.AddImportError("Phone Missing Contact Key", "Phone found with no contact.  Phone has been removed.", phone.PhoneNumber);
            }

            return !phonesWithoutContacts.Any();
        }

        internal bool ValidatePrimaryContactsWithNoPhones()
        {
            var contactsMissingPhones = Contacts.Where(c => c.ContactType == 1 && Phones.All(p => p.ContactKey != c.ContactKey)).ToList();

            foreach (var contact in contactsMissingPhones)
            {
                AddPhone("NEED PHONE", contact.ContactKey, PhoneType.Home);
                ImportErrors.AddImportError("Primary Contact Missing Phone", "Empty phone record added for contact", "Contact Key: " + contact.ContactKey);
            }

            return !contactsMissingPhones.Any();
        }

        internal bool ValidateAccountsWithNoRentals()
        {
            var accountsWithNoRentals = Accounts.Where(a => Rentals.All(r => r.CommonKey != a.CommonKey)).ToList();

            foreach (var account in accountsWithNoRentals)
            {
                Accounts.Remove(account);
                ImportErrors.AddImportError("Account Missing Rental", "Account found with no Rentals", "Common Key: " + account.CommonKey);
            }

            return !accountsWithNoRentals.Any();
        }

        internal bool ValidateRentalsWithNoAccount()
        {
            var rentalsWithNoAccount = Rentals.Where(a => Accounts.All(r => r.CommonKey != a.CommonKey)).ToList();

            foreach (var rental in rentalsWithNoAccount)
            {
                Rentals.Remove(rental);
                ImportErrors.AddImportError("Rental Missing Account", "Rental removed due to no account", "Common Key: " + rental.CommonKey);
            }

            return !rentalsWithNoAccount.Any();
        }

        internal bool ValidateAddressesWithNoContacts()
        {
            var addressesWithoutContacts = Addresses.Where(a => string.IsNullOrWhiteSpace(a.ContactKey) || Contacts.All(c => c.ContactKey != a.ContactKey)).ToList();

            foreach (var address in addressesWithoutContacts)
            {
                Addresses.Remove(address);
                ImportErrors.AddImportError("Address Missing Contact Key", "Address removed due to no contact", address.Line1);
            }

            return !addressesWithoutContacts.Any();
        }

        internal bool ValidatePrimaryContactsWithNoAddresses()
        {
            var contactsMissingAddresses = Contacts.Where(c => c.ContactType == 1 && Addresses.All(a => a.ContactKey != c.ContactKey)).ToList();

            foreach (var contact in contactsMissingAddresses)
            {
                AddAddress("NEED ADDRESS", null, null, "NEED CITY", "NEED STATE", null, "NEED POSTAL CODE", contact.ContactKey, 1);
                ImportErrors.AddImportError("Primary Contact Missing Address", "Empty address record added for contact", "Contact Key: " + contact.ContactKey);
            }

            return !contactsMissingAddresses.Any();
        }

        internal bool ValidateContactsWithNoAccount()
        {
            var contactsMissingAccounts = Contacts.Where(c => Accounts.All(a => a.CommonKey != c.CommonKey)).ToList();

            foreach (var contact in contactsMissingAccounts)
            {
                Contacts.Remove(contact);
                ImportErrors.AddImportError("Contact Missing Account", "Contact removed due to no account", "Contact Key: " + contact.ContactKey);
            }

            return !contactsMissingAccounts.Any();
        }

        internal bool ValidateSinglePrimaryContact()
        {
            var duplicatePrimaryContacts = Contacts.Where(c => c.ContactType == 1).GroupBy(c => c.CommonKey)
                                              .Where(g => g.Count() > 1)
                                              .Select(g => new { g.Key, Contacts = g.ToList() }).ToList();

            foreach (var duplicateContact in duplicatePrimaryContacts)
            {
                for (int i = 0; i < duplicateContact.Contacts.Count(); i++)
                {
                    // Leave the first one.  Update all the contacts after that to be secondary.
                    if (i == 0) continue;
                    duplicateContact.Contacts[i].ContactType = 2;
                    ImportErrors.AddImportError("Mulitple Primary Contacts", "Contact updated from primary to secondary", duplicateContact.Contacts[i].ContactKey);
                }
            }

            return !duplicatePrimaryContacts.Any();
        }

        internal bool ValidateDuplicateContacts()
        {
            var duplicateContacts = Contacts.GroupBy(c => new { c.CommonKey, FirstName = c.FirstName.ToLower(), LastName = c.LastName.ToLower() })
                                                       .Where(g => g.Count() > 1)
                                                       .Select(g => new { g.Key, Contacts = g.ToList() }).ToList();

            foreach (var duplicateContact in duplicateContacts)
            {
                // If one of the contacts is the primary, we want to make sure we keep that one.
                var firstContact = duplicateContact.Contacts.FirstOrDefault(c => c.ContactType == 1) ??
                                   duplicateContact.Contacts.FirstOrDefault();

                if (firstContact == null) continue;

                foreach (var contact in duplicateContact.Contacts.Where(c => !ReferenceEquals(c, firstContact)))
                {
                    MergeContacts(firstContact, contact);
                }
            }

            RemoveDuplicateAddresses();
            RemoveDuplicatePhones();

            return !duplicateContacts.Any();
        }

        /// <summary>
        /// Merges contact2 into contact1.  Then removes contact2 from Contacts.
        /// </summary>
        /// <param name="contact1"></param>
        /// <param name="contact2"></param>
        private void MergeContacts(Contact contact1, Contact contact2)
        {
            Contract.Requires(contact1 != null);
            Contract.Requires(contact2 != null);

            MergePhones(contact1, contact2);
            MergeAddresses(contact1, contact2);

            Contacts.Remove(contact2);
            ImportErrors.AddImportError("Duplicate Contact Found", "Contacts Merged", $"Contact {contact2.ContactKey} has been merged with {contact1.ContactKey}.");

        }

        /// <summary>
        /// Merges the phones from contact2 into contact1.  
        /// </summary>
        /// <param name="contact1"></param>
        /// <param name="contact2"></param>
        private void MergePhones(Contact contact1, Contact contact2)
        {
            Contract.Requires(contact1 != null);
            Contract.Requires(contact2 != null);

            var contact1Phones = Phones.Where(p => p.ContactKey == contact1.ContactKey).ToList();

            foreach (var phone in Phones.Where(p => p.ContactKey == contact2.ContactKey).ToList())
            {
                if (contact1Phones.Any(p => p.PhoneNumber == phone.PhoneNumber))
                    Phones.Remove(phone);
                else
                    phone.ContactKey = contact1.ContactKey;
            }
        }

        /// <summary>
        /// Merges the addresses from contact2 into contact1.  
        /// </summary>
        /// <param name="contact1"></param>
        /// <param name="contact2"></param>
        private void MergeAddresses(Contact contact1, Contact contact2)
        {
            Contract.Requires(contact1 != null);
            Contract.Requires(contact2 != null);

            var contact1Addresses = Addresses.Where(p => p.ContactKey == contact1.ContactKey).ToList();

            foreach (var address in Addresses.Where(p => p.ContactKey == contact2.ContactKey).ToList())
            {
                if (contact1Addresses.Any(a => string.Equals(a.Line1, address.Line1, StringComparison.CurrentCultureIgnoreCase)))
                    Addresses.Remove(address);
                else
                    address.ContactKey = contact1.ContactKey;
            }
        }

        private void RemoveDuplicatePhones()
        {
            var duplicatePhones = Phones.GroupBy(p => new { p.ContactKey, p.PhoneNumber })
                    .Where(g => g.Count() > 1)
                    .Select(g => new { g.Key, Phones = g.ToList() })
                    .ToList();

            foreach (var duplicatePhone in duplicatePhones)
            {
                for (int i = 0; i < duplicatePhone.Phones.Count(); i++)
                {
                    // Leave the first one.  Remove all the phones after that.
                    if (i == 0) continue;
                    Phones.Remove(duplicatePhone.Phones[i]);
                    ImportErrors.AddImportError("Duplicate Phone Found", "Duplicate phone removed", duplicatePhone.Phones[i].ContactKey);
                }
            }
        }

        private void RemoveDuplicateAddresses()
        {
            var duplicateAddresses = Addresses.GroupBy(p => new { p.ContactKey, Line1 = p.Line1.ToLower() })
                     .Where(g => g.Count() > 1)
                     .Select(g => new { g.Key, Addresses = g.ToList() })
                     .ToList();

            foreach (var duplicateAddress in duplicateAddresses)
            {
                for (int i = 0; i < duplicateAddress.Addresses.Count(); i++)
                {
                    // Leave the first one.  Remove all the addresses after that.
                    if (i == 0) continue;
                    Addresses.Remove(duplicateAddress.Addresses[i]);
                    ImportErrors.AddImportError("Duplicate Address Found", "Duplicate address removed", duplicateAddress.Addresses[i].ContactKey);
                }
            }
        }

        internal bool ValidateNoDuplicateAccounts()
        {
            var duplicateAccounts = Accounts.GroupBy(r => r.CommonKey)
                                           .Where(g => g.Count() > 1)
                                           .Select(g => new { g.Key, Accounts = g.ToList() }).ToList();

            foreach (var duplicateUnit in duplicateAccounts)
            {
                for (int i = 0; i < duplicateUnit.Accounts.Count(); i++)
                {
                    // Leave the first one.  Remove all the accounts after that.
                    if (i == 0) continue;
                    Accounts.Remove(duplicateUnit.Accounts[i]);
                    ImportErrors.AddImportError("Duplicate Account", "Duplicate account removed", duplicateUnit.Accounts[i].CommonKey);
                }
            }

            return !duplicateAccounts.Any();
        }

        internal bool ValidateAutoPaysWithNoAccount()
        {
            var autoPaysMissingAccounts = AutoPays.Where(c => Accounts.All(a => a.CommonKey != c.CommonKey)).ToList();

            foreach (var autoPay in autoPaysMissingAccounts)
            {
                AutoPays.Remove(autoPay);
                ImportErrors.AddImportError("AutoPay Missing Account", "AutoPay removed due to no account", "Common Key: " + autoPay.CommonKey);
            }

            return !autoPaysMissingAccounts.Any();
        }

        #endregion

        #region Helper Methods

        protected virtual ServiceCollection GetServices()
        {
            return new ServiceCollection
            {
                new Service()
                {
                    ServiceObjectId = DefaultServiceId,
                    ServiceName = "Default Invoicing, Individual Class",
                    Price = 1,
                    ServiceDesc = "CS-Conversion Default Invoicing Service (CSINV)",
                    Active = true,
                    PartRefund = true,
                    ServiceType = 1
                },
                new Service()
                {
                    ServiceObjectId = "999002",
                    ServiceName = "Default Invoicing, Business Class",
                    Price = 0,
                    ServiceDesc = "CS-Conversion Default Invoicing Service (CSBIN)",
                    Active = true,
                    PartRefund = true,
                    ServiceType = 1
                },
                new Service()
                {
                    ServiceObjectId = "999003",
                    ServiceName = "Default 24 Hour Access",
                    Price = 10,
                    ServiceDesc = "CS-Conversion Default 24 Hour Access Service (CS24H)",
                    Active = true,
                    PartRefund = true,
                    ServiceType = 2
                }
            };
        }

        protected decimal? GetInsuranceId(decimal? coverageAmount, string unitNumber)
        {
            if (coverageAmount == null) return null;

            if (coverageAmount <= 0)
                return null;

            if (coverageAmount <= 2000)
                return 1000000221;

            if (coverageAmount <= 3000)
                return 1000000132;

            if (coverageAmount <= 5000)
                return 1000000133;

            if (coverageAmount <= 10000)
                return 1000000134;

            ImportErrors.AddImportError("Invalid Insurance Coverage", "Insurance coverage amount is too High.", $"Coverage Amount: {coverageAmount}  Unit Number: {unitNumber}  $10,000 coverage was used.");

            return 1000000134;
        }

        protected void AddPhone(string phoneNumber, string contactKey, PhoneType phoneType)
        {
            if (string.IsNullOrEmpty(phoneNumber)) return;

            AddPhone(phoneNumber, contactKey, (decimal)phoneType);
        }

        protected void AddPhone(string phoneNumber, string contactKey, decimal? phoneType)
        {
            if (string.IsNullOrEmpty(phoneNumber)) return;

            Phones.Add(new Phone
            {
                PhoneNumber = phoneNumber,
                ContactKey = contactKey,
                PhoneType = phoneType ?? (decimal)PhoneType.Home,
                SiteId = _siteId
            });
        }

        protected void AddAddress(string line1, string line2, string line3, string city, string state, string country,
            string postalCode, string contactKey, decimal? addressType)
        {
            if (!string.IsNullOrWhiteSpace(line1) || !string.IsNullOrWhiteSpace(line2) ||
                !string.IsNullOrWhiteSpace(line3) || !string.IsNullOrWhiteSpace(city) ||
                !string.IsNullOrWhiteSpace(state) || !string.IsNullOrWhiteSpace(country) ||
                !string.IsNullOrWhiteSpace(postalCode))
            {

                Addresses.Add(new Address()
                {
                    ContactKey = contactKey,
                    Line1 = line1,
                    Line2 = line2,
                    Line3 = line3,
                    City = city,
                    State = state,
                    Country = country,
                    PostalCode = postalCode,
                    AddressType = addressType,
                    SiteId = _siteId
                });

            }
        }

        protected string GetCommonKey(string unitNumber)
        {
            return (unitNumber != null && _accountCommonKeyMap.ContainsKey(unitNumber)) ? _accountCommonKeyMap[unitNumber] : null;
        }

        protected IReadOnlyDictionary<string, int> BuildColumnIndexMap(ICollection<string[]> rows)
        {
            Contract.Ensures(Contract.Result<IReadOnlyDictionary<string, int>>() != null);

            var result = new Dictionary<string, int>();

            if (rows == null) return result;

            var headerRow = rows.FirstOrDefault();
            if (headerRow == null) return result;

            for (var i = 0; i < headerRow.Length; i++)
            {
                var value = headerRow[i] ?? string.Empty;
                if (!result.ContainsKey(value))
                    result.Add(value.Trim(), i);
                else
                    result.Add(value.Trim() + i, i); //If it already exists, just append the index to make it unique.  This is so the count is not off.
            }

            // Remove the header row as it won't be needed from here on out.
            rows.Remove(headerRow);

            return result;
        }

        protected List<string[]> GetCsvData(string fileName, string delimiter = ",", bool ignoreQuotes = false)
        {
            var result = new List<string[]>();
            try
            {
                using (var sReader = new StreamReader(fileName))
                using (var parser = new CsvParser(sReader, new CsvConfiguration() { Delimiter = delimiter, IgnoreQuotes = ignoreQuotes }))
                {
                    while (true)
                    {
                        var row = parser.Read();
                        if (row == null) break;
                        result.Add(row);
                    }
                }
            }
            catch (FileNotFoundException)
            {
                this.ImportErrors.AddFileNotFoundImportError(fileName);
            }

            return result;
        }

        protected void SaveCsvData(string fileName, List<string[]> data, string delimiter = ",")
        {
            Contract.Requires(data != null);

            if (File.Exists(fileName))
                File.Delete(fileName);

            using (var writer = File.CreateText(fileName))
            {
                foreach (var record in data)
                {
                    writer.WriteLine(string.Join(delimiter, record));
                }
            }
        }

        protected decimal GetWidth(string dimensions)
        {
            var matches = _dimensionsRegex.Matches(dimensions);
            if (matches.Count < 1)
            {
                ImportErrors.AddImportError("Invalid Dimensions", "Unable to get width.", dimensions);
            }

            var width = matches[0].Groups[1].Value.ToDecimal();
            return width;
        }

        protected decimal GetDepth(string dimensions)
        {
            var matches = _dimensionsRegex.Matches(dimensions);
            if (matches.Count < 2)
            {
                ImportErrors.AddImportError("Invalid Dimensions", "Unable to get depth.", dimensions);
            }

            var depth = matches[1].Groups[1].Value.ToDecimal();
            return depth;
        }

        #endregion

        [ContractInvariantMethod]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "Required for code contracts.")]
        private void ObjectInvariant()
        {
            Contract.Invariant(!string.IsNullOrEmpty(_dataPath));
        }


    }
}

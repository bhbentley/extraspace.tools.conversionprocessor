﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using ConversionProcessorWPF.Model;
using ConversionProcessorWPF.Utilities;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace ConversionProcessorWPF.ImportTemplates
{
    [ConversionType("CS")]
    public class CentershiftImportTemplate : ImportTemplate
    {
        public CentershiftImportTemplate(string dataPath, long siteId, string siteNumber)
            : base(dataPath, siteId, siteNumber)
        {
            Contract.Requires(!string.IsNullOrEmpty(dataPath));
        }

        protected virtual string InsuranceMapPath => Path.Combine(_dataPath, "Insurance.xlsx");

        protected override void LoadTemplateData()
        {
            LoadAccounts();
            LoadUnits();
            LoadContacts();
            LoadAddresses();
            LoadPhones();
            LoadRentals();
            LoadPcds();
            LoadAutoPays();
            LoadNotes();
            //LoadHistory();
        }

        protected virtual decimal? GetEssAttribute01(decimal? csAttributes)
        {
            return 1;
        }

        protected virtual decimal? GetEssRate(decimal? csAttributes, decimal width, decimal depth, decimal? csRate)
        {
            return csRate;
        }

        protected virtual int GetDelStep(DateTime? paidThruDate)
        {
            return 0;
        }

        protected void LoadAccounts()
        {
            var rows = GetCsvData(Path.Combine(_dataPath, "stage_op_accounts.txt"));
            var columnIndexMap = BuildColumnIndexMap(rows);

            foreach (var row in rows)
            {
                // make sure the row has the same number of fields as the header.
                if (row.Count() != columnIndexMap.Count())
                {
                    if (!row.All(string.IsNullOrWhiteSpace))
                    {
                        ImportErrors.AddIncorrectColumnCountImportError("Account", columnIndexMap.Count(), row.Count(), string.Join(" ", row));
                    }
                    continue;
                }

                var account = new Account()
                {
                    CommonKey = row.GetValueAt(columnIndexMap["COMMONKEY"]),
                    AccountName = row.GetValueAt(columnIndexMap["ACCOUNT_NAME"]),
                    AccountClass = row.GetValueAt(columnIndexMap["ACCOUNT_CLASS"]).ToNullableDecimal(),
                    GatePin = row.GetValueAt(columnIndexMap["GATE_PIN"]),
                    Gate24hr = row.GetValueAt(columnIndexMap["GATE24HR"]).ToNullableBoolean(),
                    GateTimeZone = row.GetValueAt(columnIndexMap["GATE_TIME_ZONE"]),
                    GateKeypadZone = row.GetValueAt(columnIndexMap["GATE_KEYPAD_ZONE"]),
                    SiteId = _siteId
                };

                Accounts.Add(account);
            }

        }

        protected void LoadUnits()
        {
            var rows = GetCsvData(Path.Combine(_dataPath, "stage_op_site_units.txt"));
            var columnIndexMap = BuildColumnIndexMap(rows);

            foreach (var row in rows)
            {
                // make sure the row has the same number of fields as the header.
                if (row.Count() != columnIndexMap.Count())
                {
                    if (!row.All(string.IsNullOrWhiteSpace))
                    {
                        ImportErrors.AddIncorrectColumnCountImportError("Unit", columnIndexMap.Count(), row.Count(), string.Join(" ", row));
                    }
                    continue;
                }

                var unit = new Unit()
                {
                    CommonKey = row.GetValueAt(columnIndexMap["COMMONKEY"]),
                    UnitNumber = row.GetValueAt(columnIndexMap["UNIT_NUMBER"]),
                    RentalUnitKey = row.GetValueAt(columnIndexMap["RENTAL_UNIT_KEY"]),
                    RateEffDate = row.GetValueAt(columnIndexMap["RATE_EFF_DATE"]).ToNullableDate(),
                    Door = row.GetValueAt(columnIndexMap["DOOR"]).ToNullableLong(),
                    Access = row.GetValueAt(columnIndexMap["AKSESS"]).ToNullableLong(),
                    Climate = row.GetValueAt(columnIndexMap["CLIMATE"]).ToNullableLong(),
                    Status = row.GetValueAt(columnIndexMap["STATUS"]).ToNullableShort(),
                    Attributes = GetEssAttribute01(row.GetValueAt(columnIndexMap["ATTRIBUTES"]).ToNullableDecimal())
                };

                var dimensions = row.GetValueAt(columnIndexMap["DIMENSIONS"]);
                var width = GetWidth(dimensions);
                var depth = GetDepth(dimensions);

                // Set the dimensions.  Floor width and depth.
                unit.Width = width;
                unit.Depth = depth;

                // If the unit is rented or reserved, use the rate given in the data file.
                if (unit.Status == 3 || unit.Status == 2)
                    unit.RentRate = row.GetValueAt(columnIndexMap["RENT_RATE"]).ToNullableDecimal();
                else
                    unit.RentRate = GetEssRate(unit.Attributes, width, depth, row.GetValueAt(columnIndexMap["RENT_RATE"]).ToNullableDecimal());

                unit.StreetRate = GetEssRate(unit.Attributes, width, depth, row.GetValueAt(columnIndexMap["STREET_RATE"]).ToNullableDecimal());

                Units.Add(unit);
            }

        }

        protected void LoadContacts()
        {
            var rows = GetCsvData(Path.Combine(_dataPath, "stage_op_contacts.txt"));
            var columnIndexMap = BuildColumnIndexMap(rows);

            foreach (var row in rows)
            {
                // make sure the row has the same number of fields as the header.
                if (row.Count() != columnIndexMap.Count())
                {
                    if (!row.All(string.IsNullOrWhiteSpace))
                    {
                        ImportErrors.AddIncorrectColumnCountImportError("Contact", columnIndexMap.Count(), row.Count(), string.Join(" ", row));
                    }
                    continue;
                }

                var contact = new Contact()
                {
                    CommonKey = row.GetValueAt(columnIndexMap["COMMONKEY"]),
                    FirstName = row.GetValueAt(columnIndexMap["FIRST_NAME"]),
                    MiddleName = row.GetValueAt(columnIndexMap["MIDDLE_NAME"]),
                    LastName = row.GetValueAt(columnIndexMap["LAST_NAME"]),
                    KnownAs = row.GetValueAt(columnIndexMap["KNOWN_AS"]),
                    ContactType = row.GetValueAt(columnIndexMap["CONTACT_TYPE"]).ToNullableDecimal(),
                    Employer = row.GetValueAt(columnIndexMap["EMPLOYER"]),
                    EmailAddress = row.GetValueAt(columnIndexMap["EMAIL_ADDRESS"]),
                    DriversLicenseNo = row.GetValueAt(columnIndexMap["DRIVERS_LICENSE_NO"]),
                    DriversLicenseState = row.GetValueAt(columnIndexMap["DRIVERS_LICENSE_STATE"]),
                    DriversLicenseExpires = row.GetValueAt(columnIndexMap["DRIVERS_LICENSE_EXPIRES"]),
                    ContactKey = row.GetValueAt(columnIndexMap["CONTACT_KEY"]),
                    Ssn = row.GetValueAt(columnIndexMap["SSN"]),
                    DateOfBirth = row.GetValueAt(columnIndexMap["DATE_OF_BIRTH"])
                };

                Contacts.Add(contact);
            }
        }

        protected void LoadAddresses()
        {
            var rows = GetCsvData(Path.Combine(_dataPath, "stage_op_addresses.txt"));
            var columnIndexMap = BuildColumnIndexMap(rows);

            foreach (var row in rows)
            {
                // make sure the row has the same number of fields as the header.
                if (row.Count() != columnIndexMap.Count())
                {
                    if (!row.All(string.IsNullOrWhiteSpace))
                    {
                        ImportErrors.AddIncorrectColumnCountImportError("Address", columnIndexMap.Count(), row.Count(), string.Join(" ", row));
                    }
                    continue;
                }

                AddAddress(row.GetValueAt(columnIndexMap["LINE1"]),
                           row.GetValueAt(columnIndexMap["LINE2"]),
                           row.GetValueAt(columnIndexMap["LINE3"]),
                           row.GetValueAt(columnIndexMap["CITY"]),
                           row.GetValueAt(columnIndexMap["STATE"]),
                           row.GetValueAt(columnIndexMap["COUNTRY"]),
                           row.GetValueAt(columnIndexMap["POSTAL_CODE"]),
                           row.GetValueAt(columnIndexMap["CONTACT_KEY"]),
                           row.GetValueAt(columnIndexMap["ADDRESS_TYPE"]).ToNullableDecimal());
            }
        }

        protected void LoadPhones()
        {
            var rows = GetCsvData(Path.Combine(_dataPath, "stage_op_phones.txt"));
            var columnIndexMap = BuildColumnIndexMap(rows);

            foreach (var row in rows)
            {
                // make sure the row has the same number of fields as the header.
                if (row.Count() != columnIndexMap.Count())
                {
                    if (!row.All(string.IsNullOrWhiteSpace))
                    {
                        ImportErrors.AddIncorrectColumnCountImportError("Phone", columnIndexMap.Count(), row.Count(), string.Join(" ", row));
                    }
                    continue;
                }

                AddPhone(row.GetValueAt(columnIndexMap["PHONE_NUMBER"]),
                         row.GetValueAt(columnIndexMap["CONTACT_KEY"]),
                         row.GetValueAt(columnIndexMap["PHONE_TYPE"]).ToNullableDecimal());
            }
        }

        protected void LoadRentals()
        {
            var rows = GetCsvData(Path.Combine(_dataPath, "stage_op_rentals.txt"));
            var columnIndexMap = BuildColumnIndexMap(rows);
            var insuranceMap = GetInsuranceMap();

            foreach (var row in rows)
            {
                // make sure the row has the same number of fields as the header.
                if (row.Count() != columnIndexMap.Count())
                {
                    if (!row.All(string.IsNullOrWhiteSpace))
                    {
                        ImportErrors.AddIncorrectColumnCountImportError("Rental", columnIndexMap.Count(), row.Count(), string.Join(" ", row));
                    }
                    continue;
                }

                var insOptionId = row.GetValueAt(columnIndexMap["INSURANCE"]).ToNullableDecimal();
                decimal? coverageAmount = null;

                if (insOptionId.HasValue)
                {
                    if (insuranceMap.ContainsKey(insOptionId.Value))
                    {
                        coverageAmount = insuranceMap[insOptionId.Value];
                    }
                    else
                    {
                        ImportErrors.AddImportError("Insurance not found.", "No insurance found", $"OptionID={insOptionId.Value}");
                    }
                }

                var rentalUnitKey = row.GetValueAt(columnIndexMap["RENTAL_UNIT_KEY"]);


                var rental = new Rental()
                {
                    CommonKey = row.GetValueAt(columnIndexMap["COMMONKEY"]),
                    RentalStartDate = row.GetValueAt(columnIndexMap["RENTAL_START_DATE"]).ToNullableDate(),
                    RentalFinishDate = row.GetValueAt(columnIndexMap["RENTAL_FINISH_DATE"]).ToNullableDate(),
                    ReservationDate = row.GetValueAt(columnIndexMap["RESERVATION_DATE"]).ToNullableDate(),
                    IsReserved = (string.Equals(row.GetValueAt(columnIndexMap["STATUS"]), "R", StringComparison.CurrentCultureIgnoreCase)),
                    Insurance = GetInsuranceId(coverageAmount, Units.FirstOrDefault(u => u.RentalUnitKey == rentalUnitKey)?.UnitNumber),
                    PaidThruDate = row.GetValueAt(columnIndexMap["PAID_THRU_DATE"]).ToNullableDate(),
                    SecurityDeposit = row.GetValueAt(columnIndexMap["SECURITY_DEPOSIT"]).ToNullableDecimal(),
                    FeesBalance = row.GetValueAt(columnIndexMap["FEES_BALANCE"]).ToNullableDecimal(),
                    CreditsBalance = row.GetValueAt(columnIndexMap["CREDITS_BALANCE"]).ToNullableDecimal(),
                    UnitId = row.GetValueAt(columnIndexMap["UNIT_ID"]).ToNullableLong(),
                    AccountId = row.GetValueAt(columnIndexMap["ACCOUNT_ID"]).ToNullableLong(),
                    InsuranceStartDate = row.GetValueAt(columnIndexMap["INSURANCE_START_DATE"]).ToNullableDate(),
                    RentalUnitKey = rentalUnitKey
                };

                rental.DelinquencyStep = GetDelStep(rental.PaidThruDate);

                Rentals.Add(rental);
            }
        }

        protected void LoadPcds()
        {
            var rows = GetCsvData(Path.Combine(_dataPath, "stage_op_pcd.txt"));
            var columnIndexMap = BuildColumnIndexMap(rows);

            foreach (var row in rows)
            {
                // make sure the row has the same number of fields as the header.
                if (row.Count() != columnIndexMap.Count())
                {
                    if (!row.All(string.IsNullOrWhiteSpace))
                    {
                        ImportErrors.AddIncorrectColumnCountImportError("PCD", columnIndexMap.Count(), row.Count(), string.Join(" ", row));
                    }
                    continue;
                }

                var pcd = new Pcd()
                {
                    CommonKey = row.GetValueAt(columnIndexMap["COMMONKEY"]),
                    RentalId = row.GetValueAt(columnIndexMap["RENTAL_ID"]).ToNullableLong(),
                    GivenDate = row.GetValueAt(columnIndexMap["GIVEN_DATE"]).ToNullableDate(),
                    EffDate = row.GetValueAt(columnIndexMap["EFF_DATE"]).ToNullableDate(),
                    EndDate = row.GetValueAt(columnIndexMap["END_DATE"]).ToNullableDate(),
                    Status = row.GetValueAt(columnIndexMap["STATUS"]).ToNullableShort(),
                    DiscAmt = row.GetValueAt(columnIndexMap["DISC_AMT"]).ToNullableDecimal(),
                    Balance = row.GetValueAt(columnIndexMap["BALANCE"]).ToNullableDecimal(),
                    ReasonCode = row.GetValueAt(columnIndexMap["REASON_CODE"]).ToNullableShort(),
                    ReasonDesc = row.GetValueAt(columnIndexMap["REASON_DESC"]),
                    RevCat = row.GetValueAt(columnIndexMap["REV_CAT"]).ToNullableShort(),
                    Refills = row.GetValueAt(columnIndexMap["REFILLS"]).ToNullableInt(),
                    DiscPerc = row.GetValueAt(columnIndexMap["DISC_PERC"]).ToNullableDecimal(),
                    DiscAmtType = row.GetValueAt(columnIndexMap["DISC_AMT_TYPE"]).ToNullableShort(),
                    Rollover = row.GetValueAt(columnIndexMap["ROLLOVER"]).ToNullableBoolean(),
                    RentalUnitKey = row.GetValueAt(columnIndexMap["RENTAL_UNIT_KEY"])
                };

                Pcds.Add(pcd);
            }
        }

        protected void LoadAutoPays()
        {
            var rows = GetCsvData(Path.Combine(_dataPath, "stage_ap.txt"));
            var columnIndexMap = BuildColumnIndexMap(rows);

            foreach (var row in rows)
            {
                // make sure the row has the same number of fields as the header.
                if (row.Count() != columnIndexMap.Count())
                {
                    if (!row.All(string.IsNullOrWhiteSpace))
                    {
                        ImportErrors.AddIncorrectColumnCountImportError("PCD", columnIndexMap.Count(), row.Count(), string.Join(" ", row));
                    }
                    continue;
                }

                var autoPay = new AutoPay()
                {
                    CommonKey = row.GetValueAt(columnIndexMap["COMMONKEY"]),
                    AccountName = row.GetValueAt(columnIndexMap["ACCOUNT_NAME"]),
                    NameOnCard = row.GetValueAt(columnIndexMap["NAME_ON_CARD"]),
                    StreetAddr = row.GetValueAt(columnIndexMap["STREET_ADDR"]),
                    CityAddr = row.GetValueAt(columnIndexMap["CITY_ADDR"]),
                    StateAddr = row.GetValueAt(columnIndexMap["STATE_ADDR"]),
                    PostalAddr = row.GetValueAt(columnIndexMap["POSTAL_ADDR"]),
                    CardNo = row.GetValueAt(columnIndexMap["CARD_NO"]),
                    CardExp = row.GetValueAt(columnIndexMap["CARD_EXP"]),
                    SiteId = _siteId
                };

                AutoPays.Add(autoPay);
            }
        }

        protected void LoadNotes()
        {
            var rows = GetCsvData(Path.Combine(_dataPath, "stage_op_notes.txt"));
            var columnIndexMap = BuildColumnIndexMap(rows);

            foreach (var row in rows)
            {
                // make sure the row has the same number of fields as the header.
                if (row.Count() != columnIndexMap.Count())
                {
                    if (!row.All(string.IsNullOrWhiteSpace))
                    {
                        ImportErrors.AddIncorrectColumnCountImportError("Note", columnIndexMap.Count(), row.Count(), string.Join(" ", row));
                    }
                    continue;
                }

                var note = new Note()
                {
                    CommonKey = row.GetValueAt(columnIndexMap["COMMONKEY"]),
                    Text = row.GetValueAt(columnIndexMap["NOTE"]),
                    Flagged = row.GetValueAt(columnIndexMap["FLAGGED"]).ToNullableBoolean(),
                    FlagExp = row.GetValueAt(columnIndexMap["FLAG_EXP"]).ToNullableDate(),
                    SiteId = _siteId
                };

                if (!string.IsNullOrWhiteSpace(note.Text))
                {
                    Notes.Add(note);
                }
            }
        }

        protected void LoadHistory()
        {
            var rows = GetCsvData(Path.Combine(_dataPath, "stage_op_tran_legacy.txt"));
            var columnIndexMap = BuildColumnIndexMap(rows);

            foreach (var row in rows)
            {
                // make sure the row has the same number of fields as the header.
                if (row.Count() != columnIndexMap.Count())
                {
                    if (!row.All(string.IsNullOrWhiteSpace))
                    {
                        ImportErrors.AddIncorrectColumnCountImportError("History", columnIndexMap.Count(), row.Count(), string.Join(" ", row));
                    }
                    continue;
                }

                var history = new History();
                history.CommonKey = row.GetValueAt(columnIndexMap["COMMON_KEY"]);
                history.RentalId = row.GetValueAt(columnIndexMap["RENTAL_ID"]).ToNullableLong();
                history.Tdate = row.GetValueAt(columnIndexMap["TDATE"]).ToNullableDate();
                history.Tran = row.GetValueAt(columnIndexMap["TRAN"]);
                history.Ttype = row.GetValueAt(columnIndexMap["TTYPE"]);
                history.Account = row.GetValueAt(columnIndexMap["ACCOUNT"]);
                history.Description = row.GetValueAt(columnIndexMap["DESCRIPTION"]);
                history.ItemPrice = row.GetValueAt(columnIndexMap["ITEM_PRICE"]).ToNullableDecimal();
                history.Charge = row.GetValueAt(columnIndexMap["CHARGE"]).ToNullableDecimal();
                history.Payment = row.GetValueAt(columnIndexMap["PAYMENT"]).ToNullableDecimal();
                history.Rentals = row.GetValueAt(columnIndexMap["RENTALS"]);
                history.Balance = row.GetValueAt(columnIndexMap["BALANCE"]).ToNullableDecimal();
                history.Credbal = row.GetValueAt(columnIndexMap["CREDBAL"]).ToNullableDecimal();
                history.Pmt = row.GetValueAt(columnIndexMap["PMT"]).ToNullableDecimal();
                history.Ptd = row.GetValueAt(columnIndexMap["PTD"]).ToNullableDate();
                history.RowNumber = row.GetValueAt(columnIndexMap["ROW_NUMBER"]).ToNullableLong();
                history.RentalUnitKey = row.GetValueAt(columnIndexMap["RENTAL_UNIT_KEY"]);
                history.SiteId = _siteId;

                History.Add(history);
            }
        }

        #region Helper Methods

        private Dictionary<decimal, decimal?> GetInsuranceMap()
        {
            var result = new Dictionary<decimal, decimal?>();

            try
            {
                using (var fs = new FileStream(InsuranceMapPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                using (var doc = SpreadsheetDocument.Open(fs, false))
                {
                    var workbookPart = doc.WorkbookPart;

                    var sharedStrings = workbookPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault().GetSharedStrings();

                    var worksheetPart = workbookPart.WorksheetParts.FirstOrDefault();
                    if (worksheetPart == null) return result;

                    using (var reader = OpenXmlReader.Create(worksheetPart))
                    {
                        while (reader.Read())
                        {
                            if (reader.ElementType != typeof(Row)) continue;

                            do
                            {
                                var r = reader.LoadCurrentElement() as Row;
                                if (r == null || r.RowIndex == 1 || r.IsEmptyRow()) continue;

                                var coverageAmount = r.GetDecimalCellValue("E", sharedStrings);
                                var insOptionId = r.GetDecimalCellValue("G", sharedStrings);

                                if (coverageAmount.HasValue && insOptionId.HasValue)
                                    result.Add(insOptionId.Value, coverageAmount.Value);

                            } while (reader.ReadNextSibling());
                        }
                    }
                }
            }
            catch (FileNotFoundException)
            {
                ImportErrors.AddFileNotFoundImportError("Insurance.xlsx");
            }
            return result;
        }


        #endregion
    }
}
﻿using PropertyChanged;

namespace ConversionProcessorWPF.ImportTemplates
{
    [ImplementPropertyChanged]
    public class ImportError
    {
        public string ErrorType { get; set; }
        public string Error { get; set; }
        public string Description { get; set; }
    }
}

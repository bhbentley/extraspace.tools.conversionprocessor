﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using ConversionProcessorWPF.Model;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using static ConversionProcessorWPF.Utilities.OpenXmlExtensions;


namespace ConversionProcessorWPF.ImportTemplates
{
    [ConversionType("Manual")]
    public class ManualImportTemplate : ImportTemplate
    {
        public ManualImportTemplate(string dataPath, long siteId, string siteNumber)
            : base(dataPath, siteId, siteNumber)
        {
            Contract.Requires(!string.IsNullOrEmpty(dataPath));
        }

        protected override void LoadTemplateData()
        {
            using (var fs = new FileStream(GetFileName(), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (var doc = SpreadsheetDocument.Open(fs, false))
            {
                var workbookPart = doc.WorkbookPart;

                var sharedStrings = workbookPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault().GetSharedStrings();
                LoadAccounts(workbookPart.GetWorksheetPart("Accounts"), sharedStrings);
                LoadUnits(workbookPart.GetWorksheetPart("Units"), sharedStrings);
                LoadNotes(workbookPart.GetWorksheetPart("Notes"), sharedStrings);
                LoadRentals(workbookPart.GetWorksheetPart("Rentals"), sharedStrings);
                LoadAlternateContacts(workbookPart.GetWorksheetPart("AltContacts"), sharedStrings);
            }
        }

        private void LoadAccounts(WorksheetPart worksheetPart, Dictionary<int, string> sharedStrings)
        {
            if (worksheetPart == null) return;

            var allAccountInfo = new List<AccountInfo>();

            using (var reader = OpenXmlReader.Create(worksheetPart))
            {
                while (reader.Read())
                {
                    if (reader.ElementType != typeof(Row)) continue;

                    do
                    {
                        var r = reader.LoadCurrentElement() as Row;
                        if (r == null || r.RowIndex == 1 || r.IsEmptyRow()) continue;

                        var account = new AccountInfo
                        {
                            UnitNumber = r.GetCellValue("A", sharedStrings),
                            AccountName = r.GetCellValue("B", sharedStrings),
                            AccountClass = GetAccountClassValue(r.GetCellValue("F", sharedStrings)),
                            GatePin = r.GetCellValue("G", sharedStrings),
                            GateTimeZone = r.GetCellValue("H", sharedStrings),
                            GateKeypadZone = r.GetCellValue("I", sharedStrings),
                            CustomerFirstName = r.GetCellValue("C", sharedStrings),
                            CustomerMiddleName = r.GetCellValue("D", sharedStrings),
                            CustomerLastName = r.GetCellValue("E", sharedStrings),
                            CustomerAddress1 = r.GetCellValue("J", sharedStrings),
                            CustomerAddress2 = r.GetCellValue("K", sharedStrings),
                            CustomerCity = r.GetCellValue("L", sharedStrings),
                            CustomerState = r.GetCellValue("M", sharedStrings),
                            CustomerZip = r.GetCellValue("N", sharedStrings),
                            HomePhone = r.GetCellValue("O", sharedStrings),
                            WorkPhone = r.GetCellValue("P", sharedStrings),
                            CellPhone = r.GetCellValue("Q", sharedStrings),
                            OtherPhone = r.GetCellValue("R", sharedStrings),
                            EmailAddress = r.GetCellValue("S", sharedStrings),
                            DLNumber = r.GetCellValue("T", sharedStrings),
                            DLState = r.GetCellValue("U", sharedStrings),
                            DOB = r.GetCellValue("V", sharedStrings),
                            SSN = r.GetCellValue("W", sharedStrings),
                            Employer = r.GetCellValue("X", sharedStrings)
                        };

                        allAccountInfo.Add(account);
                    } while (reader.ReadNextSibling());
                }
            }

            // Find all of the distinct accounts and get the unit Id's that go with them.
            var distinctAccounts = allAccountInfo.GroupBy(r => new
            {
                r.AccountName,
                r.CustomerFirstName,
                r.CustomerLastName,
                r.CustomerAddress1,
            }).Select((g, index) => new
            {
                CommonKey = (index + 100).ToString(),  // Start at 100
                g.Key.AccountName,
                AccountClass = g.Select(gr => gr.AccountClass).FirstOrDefault(),
                GatePin = g.Select(gr => gr.GatePin).FirstOrDefault(),
                GateTimeZone = g.Select(gr => gr.GateTimeZone).FirstOrDefault(),
                GateKeypadZone = g.Select(gr => gr.GateKeypadZone).FirstOrDefault(),
                ContactKey = (index + 1000).ToString(),  // Start at 1000 so as to not confuse this number with CommonKey
                g.Key.CustomerFirstName,
                CustomerMiddleName = g.Select(gr => gr.CustomerMiddleName).FirstOrDefault(),
                g.Key.CustomerLastName,
                g.Key.CustomerAddress1,
                CustomerAddress2 = g.Select(gr => gr.CustomerAddress2).FirstOrDefault(),
                CustomerCity = g.Select(gr => gr.CustomerCity).FirstOrDefault(),
                CustomerState = g.Select(gr => gr.CustomerState).FirstOrDefault(),
                CustomerZip = g.Select(gr => gr.CustomerZip).FirstOrDefault(),
                HomePhone = g.Select(gr => gr.HomePhone).FirstOrDefault(),
                WorkPhone = g.Select(gr => gr.WorkPhone).FirstOrDefault(),
                CellPhone = g.Select(gr => gr.CellPhone).FirstOrDefault(),
                OtherPhone = g.Select(gr => gr.OtherPhone).FirstOrDefault(),
                EmailAddress = g.Select(gr => gr.EmailAddress).FirstOrDefault(),
                DLNumber = g.Select(gr => gr.DLNumber).FirstOrDefault(),
                DLState = g.Select(gr => gr.DLState).FirstOrDefault(),
                DOB = g.Select(gr => gr.DOB).FirstOrDefault(),
                SSN = g.Select(gr => gr.SSN).FirstOrDefault(),
                Employer = g.Select(gr => gr.Employer).FirstOrDefault(),
                UnitNumbers = g.Select(gr => gr.UnitNumber).Distinct().ToList()
            }).ToList();

            foreach (var a in distinctAccounts)
            {
                foreach (var unitNumber in a.UnitNumbers)
                {
                    if (!_accountCommonKeyMap.ContainsKey(unitNumber))
                        _accountCommonKeyMap.Add(unitNumber, a.CommonKey);
                }

                Accounts.Add(new Account
                {
                    CommonKey = a.CommonKey,
                    AccountName = a.AccountName,
                    AccountClass = a.AccountClass,
                    GatePin = a.GatePin,
                    GateTimeZone = a.GateTimeZone,
                    GateKeypadZone = a.GateKeypadZone,
                    SiteId = _siteId
                });

                Contacts.Add(new Contact
                {
                    FirstName = a.CustomerFirstName,
                    MiddleName = a.CustomerMiddleName,
                    LastName = a.CustomerLastName,
                    ContactType = 1,    // 1 indicates to Siphon that this is a primary contact.
                    EmailAddress = a.EmailAddress,
                    DateOfBirth = a.DOB,
                    Ssn = a.SSN,
                    CommonKey = a.CommonKey,
                    ContactKey = a.ContactKey,
                    DriversLicenseNo = a.DLNumber,
                    DriversLicenseState = a.DLState,
                    Employer = a.Employer
                });

                AddAddress(a.CustomerAddress1, a.CustomerAddress2, null, a.CustomerCity, a.CustomerState, null, a.CustomerZip, a.ContactKey, 1);


                AddPhone(a.HomePhone, a.ContactKey, PhoneType.Home);
                AddPhone(a.WorkPhone, a.ContactKey, PhoneType.Office);
                AddPhone(a.CellPhone, a.ContactKey, PhoneType.Mobile);
                AddPhone(a.OtherPhone, a.ContactKey, PhoneType.Other);
            }

        }

        private void LoadUnits(WorksheetPart worksheetPart, Dictionary<int, string> sharedStrings)
        {
            if (worksheetPart == null) return;

            using (var reader = OpenXmlReader.Create(worksheetPart))
            {
                while (reader.Read())
                {
                    if (reader.ElementType != typeof(Row)) continue;

                    do
                    {
                        var r = reader.LoadCurrentElement() as Row;
                        if (r == null || r.RowIndex == 1 || r.IsEmptyRow()) continue;

                        var unit = new Unit
                        {
                            RentalUnitKey = (r.RowIndex - 1).ToString(),
                            UnitNumber = r.GetCellValue("A", sharedStrings),
                            Status = GetStatusValue(r.GetCellValue("D", sharedStrings)),
                            RentRate = r.GetDecimalCellValue("E", sharedStrings),
                            StreetRate = r.GetDecimalCellValue("F", sharedStrings),
                            RateEffDate = r.GetDateCellValue("G", sharedStrings)
                        };

                        var dimensions = r.GetCellValue("B", sharedStrings)?.ToUpper();
                        unit.Width = GetWidth(dimensions);
                        unit.Depth = GetDepth(dimensions);

                        unit.CommonKey = GetCommonKey(unit.UnitNumber);

                        Units.Add(unit);
                    } while (reader.ReadNextSibling());
                }
            }
        }

        private void LoadNotes(WorksheetPart worksheetPart, Dictionary<int, string> sharedStrings)
        {
            if (worksheetPart == null) return;

            using (var reader = OpenXmlReader.Create(worksheetPart))
            {
                while (reader.Read())
                {
                    if (reader.ElementType != typeof(Row)) continue;

                    do
                    {
                        var r = reader.LoadCurrentElement() as Row;
                        if (r == null || r.RowIndex == 1 || r.IsEmptyRow()) continue;

                        var note = new Note()
                        {
                            CommonKey = GetCommonKey(r.GetCellValue("A", sharedStrings)),
                            Text = r.GetCellValue("B", sharedStrings)?.Replace("\r\n", " ").Replace("\n", " "),
                            SiteId = _siteId
                        };

                        if (!string.IsNullOrWhiteSpace(note.Text))
                        {
                            Notes.Add(note);
                        }
                    } while (reader.ReadNextSibling());
                }
            }
        }

        private void LoadRentals(WorksheetPart worksheetPart, Dictionary<int, string> sharedStrings)
        {

            if (worksheetPart == null) return;

            using (var reader = OpenXmlReader.Create(worksheetPart))
            {
                while (reader.Read())
                {
                    if (reader.ElementType != typeof(Row)) continue;

                    do
                    {
                        var r = reader.LoadCurrentElement() as Row;
                        if (r == null || r.RowIndex == 1 || r.IsEmptyRow()) continue;

                        var unitNumber = r.GetCellValue("A", sharedStrings);
                        var rental = new Rental
                        {
                            CommonKey = GetCommonKey(unitNumber),
                            RentalStartDate = r.GetDateCellValue("B", sharedStrings),
                            PaidThruDate = r.GetDateCellValue("C", sharedStrings),
                            Insurance = GetInsuranceId(r.GetDecimalCellValue("D", sharedStrings), unitNumber),
                            SecurityDeposit = r.GetDecimalCellValue("F", sharedStrings),
                            FeesBalance = ((r.GetDecimalCellValue("G", sharedStrings) ?? 0) +
                                           (r.GetDecimalCellValue("H", sharedStrings) ?? 0) +
                                           (r.GetDecimalCellValue("I", sharedStrings) ?? 0) +
                                           (r.GetDecimalCellValue("J", sharedStrings) ?? 0) +
                                           (r.GetDecimalCellValue("K", sharedStrings) ?? 0)),
                            CreditsBalance = r.GetDecimalCellValue("L", sharedStrings),
                            ServiceId =
                                r.GetBooleanCellValue("O", sharedStrings) == true ? DefaultServiceId : null,
                            RentalUnitKey = Units.FirstOrDefault(u => u.UnitNumber == unitNumber)?.RentalUnitKey
                        };

                        Rentals.Add(rental);
                    } while (reader.ReadNextSibling());
                }
            }
        }

        private void LoadAlternateContacts(WorksheetPart worksheetPart, Dictionary<int, string> sharedStrings)
        {
            if (worksheetPart == null) return;

            using (var reader = OpenXmlReader.Create(worksheetPart))
            {
                var nextContactKey = (Contacts.Max(c => int.Parse(c.ContactKey)) + 1);

                while (reader.Read())
                {
                    if (reader.ElementType != typeof(Row)) continue;

                    do
                    {
                        var r = reader.LoadCurrentElement() as Row;
                        if (r == null || r.RowIndex == 1 || r.IsEmptyRow()) continue;

                        var nextContactKeyStr = nextContactKey.ToString();
                        var contact = new Contact()
                        {
                            CommonKey = GetCommonKey(r.GetCellValue("A", sharedStrings)),
                            FirstName = r.GetCellValue("B", sharedStrings),
                            MiddleName = r.GetCellValue("C", sharedStrings),
                            LastName = r.GetCellValue("D", sharedStrings),
                            ContactType = 2,    // 2 indicates to Siphon that this is a secondary contact.
                            EmailAddress = r.GetCellValue("Q", sharedStrings),
                            DateOfBirth = r.GetCellValue("P", sharedStrings),
                            Ssn = r.GetCellValue("O", sharedStrings),
                            ContactKey = nextContactKeyStr,
                            DriversLicenseNo = r.GetCellValue("N", sharedStrings)
                        };

                        Contacts.Add(contact);

                        AddAddress(r.GetCellValue("E", sharedStrings),
                            r.GetCellValue("F", sharedStrings),
                            null,
                            r.GetCellValue("G", sharedStrings),
                            r.GetCellValue("H", sharedStrings),
                            null,
                            r.GetCellValue("I", sharedStrings),
                            nextContactKeyStr,
                            1);

                        AddPhone(r.GetCellValue("J", sharedStrings), nextContactKeyStr, PhoneType.Home);
                        AddPhone(r.GetCellValue("K", sharedStrings), nextContactKeyStr, PhoneType.Office);
                        AddPhone(r.GetCellValue("L", sharedStrings), nextContactKeyStr, PhoneType.Mobile);
                        AddPhone(r.GetCellValue("M", sharedStrings), nextContactKeyStr, PhoneType.Other);

                        nextContactKey++;
                    } while (reader.ReadNextSibling());
                }
            }
        }

        #region Helper Methods

        private static short? GetStatusValue(string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            switch (value.ToLower().Trim())
            {
                case "vacant":
                    return 1;
                case "reserved":
                    return 2;
                case "rented":
                case "occupied":
                    return 3;
                case "company use":
                    return 4;
                case "damaged":
                    return 5;
                case "other":
                    return 8;
                case "unavailable":
                    return 9;
                default:
                    return null;
            }
        }

        private static decimal? GetAccountClassValue(string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            switch (value.ToLower().Trim())
            {
                case "personal":
                case "1":
                    return 1;
                case "business":
                case "2":
                    return 2;
                default:
                    return null;
            }
        }

        private string GetFileName()
        {
            return Directory.GetFiles(_dataPath, @"40 Manual Conversion Spreadsheet*.xlsx").FirstOrDefault();
        }

        #endregion

        #region Helper Classes

        private class AccountInfo
        {
            public string AccountName { get; set; }
            public decimal? AccountClass { get; set; }
            public string GatePin { get; set; }
            public string GateTimeZone { get; set; }
            public string GateKeypadZone { get; set; }
            public string CustomerFirstName { get; set; }
            public string CustomerMiddleName { get; set; }
            public string CustomerLastName { get; set; }
            public string CustomerAddress1 { get; set; }
            public string CustomerAddress2 { get; set; }
            public string CustomerCity { get; set; }
            public string CustomerState { get; set; }
            public string CustomerZip { get; set; }
            public string HomePhone { get; set; }
            public string WorkPhone { get; set; }
            public string CellPhone { get; set; }
            public string OtherPhone { get; set; }
            public string EmailAddress { get; set; }
            public string DLNumber { get; set; }
            public string DLState { get; set; }
            public string DOB { get; set; }
            public string SSN { get; set; }
            public string Employer { get; set; }
            public string UnitNumber { get; set; }
        }

        #endregion
    }
}

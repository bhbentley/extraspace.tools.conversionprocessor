﻿namespace ConversionProcessorWPF.ImportTemplates
{
    public enum PhoneType 
    {
        Home = 1,
        Office = 2,
        Mobile = 3,
        Fax = 4,
        Other = 5
    }
}
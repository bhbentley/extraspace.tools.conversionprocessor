﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConversionProcessorWPF.ImportTemplates
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ConversionTypeAttribute : Attribute
    {
        public ConversionTypeAttribute(string folderName)
        {
            Value = folderName;
        }

        public string Value { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using ConversionProcessorWPF.Model;
using ConversionProcessorWPF.Utilities;

namespace ConversionProcessorWPF.ImportTemplates
{
    [ConversionType("SiteLink")]
    public class SiteLinkImportTemplate : ImportTemplate
    {
        public SiteLinkImportTemplate(string dataPath, long siteId, string siteNumber)
            : base(dataPath, siteId, siteNumber)
        {
            Contract.Requires(!string.IsNullOrEmpty(dataPath));
        }

        protected override void LoadTemplateData()
        {
            LoadRentRoll();
            LoadNotes();
            LoadAutoPays();
        }

        private void LoadRentRoll()
        {
            try
            {

                var rows = GetCsvData(GetFilePath("RentRoll"), "\t");
                var columnIndexMap = BuildColumnIndexMap(rows);

                var allAccountinfo = LoadAllAccountInfo(rows, columnIndexMap);
                LoadAccounts(allAccountinfo);
                LoadContacts(allAccountinfo);
                LoadUnits(rows, columnIndexMap);
                LoadRentals(rows, columnIndexMap);

            }
            catch (ArgumentNullException)
            {
                ImportErrors.AddFileNotFoundImportError("RentRoll file not found");
            }
        }

        private List<AccountInfo> LoadAllAccountInfo(IEnumerable<string[]> rows, IReadOnlyDictionary<string, int> columnIndexMap)
        {
            var allAccountInfo = new List<AccountInfo>();

            if (rows == null) return allAccountInfo;

            foreach (var fields in rows)
            {
                if (fields.All(string.IsNullOrWhiteSpace))
                    continue;

                var isPrimary = fields.GetValueAt(columnIndexMap["bPrimary"]);
                if (!string.Equals(isPrimary, "true", StringComparison.CurrentCultureIgnoreCase))
                    continue;

                var leaseNum = fields.GetValueAt(columnIndexMap["iLeaseNum"]);
                if (string.IsNullOrWhiteSpace(leaseNum))
                    continue;

                var unitNumber = fields.GetValueAt(columnIndexMap["sUnitName"]);
                if (unitNumber.Contains("POS$"))
                    continue;

                var account = new AccountInfo
                {
                    UnitNumber = unitNumber,
                    GatePin = fields.GetValueAt(columnIndexMap["sAccessCode"]),
                    GateTimeZone = fields.GetValueAt(columnIndexMap["iTimeZ"]),
                    GateKeypadZone = fields.GetValueAt(columnIndexMap["iKeypadZ"]),
                    LeaseNum = leaseNum
                };

                account.PrimaryContact.FirstName = fields.GetValueAt(columnIndexMap["sFName"]);
                account.PrimaryContact.MiddleName = fields.GetValueAt(columnIndexMap["sMI"]);
                account.PrimaryContact.LastName = fields.GetValueAt(columnIndexMap["sLName"]);
                account.PrimaryContact.Address1 = fields.GetValueAt(columnIndexMap["sAddr1"]);
                account.PrimaryContact.Address2 = fields.GetValueAt(columnIndexMap["sAddr2"]);
                account.PrimaryContact.City = fields.GetValueAt(columnIndexMap["sCity"]);
                account.PrimaryContact.State = fields.GetValueAt(columnIndexMap["sRegion"]);
                account.PrimaryContact.Zip = fields.GetValueAt(columnIndexMap["sPostalCode"]);
                account.PrimaryContact.Country = fields.GetValueAt(columnIndexMap["sCountry"]);
                account.PrimaryContact.HomePhone = fields.GetValueAt(columnIndexMap["sPhone"]);
                account.PrimaryContact.EmailAddress = fields.GetValueAt(columnIndexMap["sEmail"]);
                account.PrimaryContact.CellPhone = fields.GetValueAt(columnIndexMap["sMobile"]);
                account.PrimaryContact.DLNumber = fields.GetValueAt(columnIndexMap["sLicense"]);
                account.PrimaryContact.DLState = fields.GetValueAt(columnIndexMap["sLicRegion"]);
                account.PrimaryContact.DOB = fields.GetValueAt(columnIndexMap["dDOB"]);
                account.PrimaryContact.SSN = fields.GetValueAt(columnIndexMap["sSSN"]);
                account.PrimaryContact.Employer = fields.GetValueAt(columnIndexMap["sEmployer"]);

                account.AltContact.FirstName = fields.GetValueAt(columnIndexMap["sFNameAlt"]);
                account.AltContact.MiddleName = fields.GetValueAt(columnIndexMap["sMIAlt"]);
                account.AltContact.LastName = fields.GetValueAt(columnIndexMap["sLNameAlt"]);
                account.AltContact.Address1 = fields.GetValueAt(columnIndexMap["sAddr1Alt"]);
                account.AltContact.Address2 = fields.GetValueAt(columnIndexMap["sAddr2Alt"]);
                account.AltContact.City = fields.GetValueAt(columnIndexMap["sCityAlt"]);
                account.AltContact.State = fields.GetValueAt(columnIndexMap["sRegionAlt"]);
                account.AltContact.Zip = fields.GetValueAt(columnIndexMap["sPostalCodeAlt"]);
                account.AltContact.Country = fields.GetValueAt(columnIndexMap["sCountryAlt"]);
                account.AltContact.HomePhone = fields.GetValueAt(columnIndexMap["sPhoneAlt"]);
                account.AltContact.EmailAddress = fields.GetValueAt(columnIndexMap["sEmailAlt"]);

                allAccountInfo.Add(account);
            }
            return allAccountInfo;
        }

        private void LoadAccounts(IEnumerable<AccountInfo> allAccountInfo)
        {
            // Find all of the distinct accounts and get the unit Id's that go with them.
            var distinctAccounts = allAccountInfo.GroupBy(r => new
            {
                r.LeaseNum,
                r.PrimaryContact.FirstName,
                r.PrimaryContact.LastName,
                r.PrimaryContact.Address1
            }).Select((g, index) => new
            {
                CommonKey = g.Key.LeaseNum, // using the LeaseNum as the CommonKey
                GatePin = g.Select(gr => gr.GatePin).FirstOrDefault(),
                GateTimeZone = g.Select(gr => gr.GateTimeZone).FirstOrDefault(),
                GateKeypadZone = g.Select(gr => gr.GateKeypadZone).FirstOrDefault(),
                g.Key.FirstName,
                g.Key.LastName,
                UnitNumbers = g.Select(gr => gr.UnitNumber).Distinct(),
            });

            foreach (var a in distinctAccounts)
            {
                foreach (var unitNumber in a.UnitNumbers)
                {
                    if (!_accountCommonKeyMap.ContainsKey(unitNumber))
                        _accountCommonKeyMap.Add(unitNumber, a.CommonKey);
                }

                Accounts.Add(new Account
                {
                    CommonKey = a.CommonKey,
                    AccountName = a.FirstName + " " + a.LastName,
                    AccountClass = 1,  // Set to 1 to indicate a personal account
                    GatePin = a.GatePin,
                    GateTimeZone = a.GateTimeZone,
                    GateKeypadZone = a.GateKeypadZone,
                    SiteId = _siteId
                });
            }
        }

        private void LoadContacts(IEnumerable<AccountInfo> allAccountInfo)
        {
            if (allAccountInfo == null) return;
            var nextContactKey = 1000;

            // Load all of the Primary Contacts
            foreach (var r in allAccountInfo.Where(a => !string.IsNullOrWhiteSpace(a.PrimaryContact.FirstName) && !string.IsNullOrWhiteSpace(a.PrimaryContact.LastName)).Select(a => new
            {
                a.PrimaryContact,
                a.UnitNumber
            }).Distinct())
            {
                AddContact(r.PrimaryContact, nextContactKey.ToString(), GetCommonKey(r.UnitNumber), 1);
                nextContactKey++;
            }

            // Load all of the Secondary Contacts
            foreach (var r in allAccountInfo.Where(a => !string.IsNullOrWhiteSpace(a.AltContact.FirstName) && !string.IsNullOrWhiteSpace(a.AltContact.LastName)).Select(a => new
            {
                a.AltContact,
                a.UnitNumber
            }).Distinct())
            {
                AddContact(r.AltContact, nextContactKey.ToString(), GetCommonKey(r.UnitNumber), 2);
                nextContactKey++;
            }
        }

        private void AddContact(ContactInfo contact, string contactKey, string commonKey, int contactType)
        {
            Contacts.Add(new Contact
            {
                FirstName = contact.FirstName,
                MiddleName = contact.MiddleName,
                LastName = contact.LastName,
                ContactType = contactType,
                EmailAddress = contact.EmailAddress,
                DateOfBirth = contact.DOB,
                Ssn = contact.DOB,
                CommonKey = commonKey,
                ContactKey = contactKey,
                DriversLicenseNo = contact.DLNumber,
                DriversLicenseState = contact.DLState,
                Employer = contact.Employer,
            });

            AddAddress(contact.Address1, contact.Address2, null, contact.City, contact.State, contact.Country, contact.Zip, contactKey, 1);

            AddPhone(contact.HomePhone, contactKey, PhoneType.Home);
            AddPhone(contact.WorkPhone, contactKey, PhoneType.Office);
            AddPhone(contact.CellPhone, contactKey, PhoneType.Mobile);
            AddPhone(contact.OtherPhone, contactKey, PhoneType.Other);
        }

        private void LoadUnits(IEnumerable<string[]> rows, IReadOnlyDictionary<string, int> columnIndexMap)
        {
            if (rows == null) return;

            var i = 1;
            foreach (var fields in rows)
            {
                if (fields.All(string.IsNullOrWhiteSpace))
                    continue;

                var unitNumber = fields.GetValueAt(columnIndexMap["sUnitName"]);
                if (unitNumber.Contains("POS$"))
                    continue;

                var unit = new Unit
                {
                    RentalUnitKey = (i).ToString(),
                    UnitNumber = unitNumber,
                    Width = fields.GetValueAt(columnIndexMap["dcWidth"]).ToDecimal(),
                    Depth = fields.GetValueAt(columnIndexMap["dcLength"]).ToDecimal(),
                    RentRate = fields.GetValueAt(columnIndexMap["dcRent"]).ToNullableDecimal(),
                    StreetRate = fields.GetValueAt(columnIndexMap["dcStdRate"]).ToNullableDecimal(),
                    RateEffDate = DateTime.Today,
                    CommonKey = GetCommonKey(unitNumber)
                };


                // if the rent rate is null, set it to the street rate.
                if (!unit.RentRate.HasValue)
                    unit.RentRate = unit.StreetRate;

                Units.Add(unit);
                i++;
            }
        }

        private void LoadRentals(IEnumerable<string[]> rows, IReadOnlyDictionary<string, int> columnIndexMap)
        {

            if (rows == null) return;

            foreach (var fields in rows)
            {
                if (fields.All(string.IsNullOrWhiteSpace))
                    continue;


                var rentalStartDate = fields.GetValueAt(columnIndexMap["dMovedIn"]).ToNullableDate();
                if (!rentalStartDate.HasValue)
                    continue;

                var unitNumber = fields.GetValueAt(columnIndexMap["sUnitName"]);
                if (unitNumber.Contains("POS$"))
                    continue;

                var rental = new Rental
                {
                    CommonKey = GetCommonKey(unitNumber),
                    RentalStartDate = rentalStartDate,
                    PaidThruDate = fields.GetValueAt(columnIndexMap["dPaidThru"]).ToNullableDate(),
                    Insurance = GetInsuranceId(fields.GetValueAt(columnIndexMap["dcCoverage"]).ToNullableDecimal(), unitNumber),
                    SecurityDeposit = fields.GetValueAt(columnIndexMap["dcSecDepPaid"]).ToNullableDecimal(),
                    FeesBalance = (fields.GetValueAt(columnIndexMap["dcLateFee1Bal"]).ToDecimal() +
                                   fields.GetValueAt(columnIndexMap["dcLateFee2Bal"]).ToDecimal() +
                                   fields.GetValueAt(columnIndexMap["dcLateFee3Bal"]).ToDecimal() +
                                   fields.GetValueAt(columnIndexMap["dcLateFee4Bal"]).ToDecimal() +
                                   fields.GetValueAt(columnIndexMap["dcLateFee5Bal"]).ToDecimal() +
                                   fields.GetValueAt(columnIndexMap["dcNSFBal"]).ToDecimal() +
                                   fields.GetValueAt(columnIndexMap["dcAdminFeeBal"]).ToDecimal() +
                                   fields.GetValueAt(columnIndexMap["dcCutLockFeeBal"]).ToDecimal() +
                                   fields.GetValueAt(columnIndexMap["dcAuctionFeeBal"]).ToDecimal() +
                                   fields.GetValueAt(columnIndexMap["dcOtherBal"]).ToDecimal()),
                    CreditsBalance = fields.GetValueAt(columnIndexMap["dcCreditBal"]).ToNullableDecimal(),
                    ServiceId = fields.GetValueAt(columnIndexMap["bInvoice"]).ToNullableBoolean() == true ? DefaultServiceId : null,
                    RentalUnitKey = Units.FirstOrDefault(u => u.UnitNumber == unitNumber)?.RentalUnitKey
                };

                Rentals.Add(rental);
            }
        }

        private void LoadNotes()
        {
            try
            {
                var rows = File.ReadAllLines(GetFilePath("Notes")).Select(ln => RunNotesRegex(ln).Split('\t')).ToList();
                var columnIndexMap = BuildColumnIndexMap(rows);

                foreach (var row in rows)
                {
                    // make sure the row has the same number of fields as the header.
                    if (row.Count() != columnIndexMap.Count())
                    {
                        if (!row.All(string.IsNullOrWhiteSpace))
                        {
                            ImportErrors.AddIncorrectColumnCountImportError("Note", columnIndexMap.Count(), row.Count(), string.Join(" ", row));
                        }
                        continue;
                    }

                    var note = new Note()
                    {
                        CommonKey = GetCommonKey(row.GetValueAt(columnIndexMap["sUnitName"])),
                        Text = row.GetValueAt(columnIndexMap["sNote"]),
                        SiteId = _siteId
                    };

                    if (!string.IsNullOrEmpty(note.Text))
                    {
                        Notes.Add(note);
                    }
                }
            }
            catch (ArgumentNullException)
            {
                ImportErrors.AddFileNotFoundImportError("Notes file not found");
            }
        }

        private void LoadAutoPays()
        {
            try
            {
                var rows = GetCsvData(GetFilePath("CreditCard"), "\t");
                var columnIndexMap = BuildColumnIndexMap(rows);


                foreach (var row in rows)
                {
                    // make sure the row has the same number of fields as the header.
                    if (row.Count() != columnIndexMap.Count())
                    {
                        if (!row.All(string.IsNullOrWhiteSpace))
                        {
                            ImportErrors.AddIncorrectColumnCountImportError("AutoPay", columnIndexMap.Count(), row.Count(), string.Join(" ", row));
                        }
                        continue;
                    }

                    var autoPay = new AutoPay()
                    {
                        CommonKey = GetCommonKey(row.GetValueAt(columnIndexMap["Unit"])),
                        AccountName = row.GetValueAt(columnIndexMap["NameOnCard"]),
                        NameOnCard = row.GetValueAt(columnIndexMap["NameOnCard"]),
                        StreetAddr = row.GetValueAt(columnIndexMap["Address"]),
                        PostalAddr = row.GetValueAt(columnIndexMap["ZipCode"]),
                        CardNo = row.GetValueAt(columnIndexMap["CardNumber"]),
                        CardExp = row.GetValueAt(columnIndexMap["ExpirationDate"]).ToNullableDate()?.ToString("MM/yy"),
                        SiteId = _siteId
                    };

                    if (!string.IsNullOrEmpty(autoPay.CardNo))
                    {
                        AutoPays.Add(autoPay);
                    }
                }
            }
            catch (ArgumentNullException)
            {
                ImportErrors.AddFileNotFoundImportError("CreditCard file not found");
            }
        }

        #region Helper Methods

        private string GetFilePath(string fileName)
        {
            return Directory.GetFiles(_dataPath, $@"*{fileName}*.txt").FirstOrDefault(f => !f.Contains("wquotedfields"));
        }

        private static string RunNotesRegex(string value)
        {
            var regex = new Regex(@"(Name.*)\t(.*)\t(.*)\t(.*)\t(.*)\t(.*Reason Code)");
            return regex.Replace(value, @"$1 $2 $3 $4 $5 $6");
        }

        #endregion

        #region Helper Classes

        protected class AccountInfo
        {
            public string AccountName { get; set; }
            public decimal? AccountClass { get; set; }
            public string GatePin { get; set; }
            public string GateTimeZone { get; set; }
            public string GateKeypadZone { get; set; }
            public string UnitNumber { get; set; }
            public ContactInfo PrimaryContact => _primaryContact ?? (_primaryContact = new ContactInfo());
            private ContactInfo _primaryContact;
            public ContactInfo AltContact => _altContact ?? (_altContact = new ContactInfo());
            private ContactInfo _altContact;
            public string LeaseNum { get; set; }
        }

        protected class ContactInfo
        {
            public string FirstName { get; set; }
            public string MiddleName { get; set; }
            public string LastName { get; set; }
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Zip { get; set; }
            public string Country { get; set; }
            public string HomePhone { get; set; }
            public string WorkPhone { get; set; }
            public string CellPhone { get; set; }
            public string OtherPhone { get; set; }
            public string EmailAddress { get; set; }
            public string DLNumber { get; set; }
            public string DLState { get; set; }
            public string DOB { get; set; }
            public string SSN { get; set; }
            public string Employer { get; set; }
        }
        #endregion
    }
}

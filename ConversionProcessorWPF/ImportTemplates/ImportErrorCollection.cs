﻿using System.Collections.Generic;

namespace ConversionProcessorWPF.ImportTemplates
{
    public class ImportErrorCollection : List<ImportError>
    {

        public void AddImportError(string errorType, string error, string description)
        {
            Add(new ImportError()
            {
                ErrorType = errorType,
                Error = error,
                Description = description
            });
        }

        public void AddFileNotFoundImportError(string description)
        {
            AddImportError("Data File Not Found", "File Not Found", description);
        }

        public void AddIncorrectColumnCountImportError(string recordtype, int expected, int actual, string description)
        {
            AddImportError($"{recordtype} Skipped", $"Incorrect Column Count (Expected: {expected} Actual: {actual})", description);
        }

    }
}

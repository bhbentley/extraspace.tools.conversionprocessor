﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using ClosedXML.Excel;
using ConversionProcessorWPF.Utilities;
using System.Data.OleDb;
using ConversionProcessorWPF.DataProviders;

namespace ConversionProcessorWPF.ImportTemplates
{
    [ConversionType("SmartStop")]
    public class SmartStopImportTemplate : CentershiftImportTemplate
    {
        private readonly List<DelinquencyMapping> _delinquencyMap = new List<DelinquencyMapping>();
        private readonly List<AttributeMapping> _attributeMap = new List<AttributeMapping>();
        private readonly List<RateMapping> _ratesMap = new List<RateMapping>();
        public SmartStopImportTemplate(string dataPath, long siteId, string siteNumber)
            : base(dataPath, siteId, siteNumber)
        {
            Contract.Requires(!string.IsNullOrEmpty(dataPath));
        }

        protected override string InsuranceMapPath => Path.Combine(Properties.Settings.Default.ConversionDataFileDirectory, "SmartStop", "Insurance.xlsx");

        protected override void LoadTemplateData()
        {
            LoadAttributesMap();
            LoadRatesMap();
            LoadDelinquencyMap();
            base.LoadTemplateData();
        }

        private void LoadAttributesMap()
        {
            try
            {
                var workbook = new XLWorkbook(Path.Combine(Properties.Settings.Default.ConversionDataFileDirectory, "SmartStop", "SST Attribute Map.xlsx"));
                var ws = workbook.Worksheets.FirstOrDefault();

                foreach (var row in ws.RowsUsed().Where(r => r.RowNumber() > 1))
                {
                    _attributeMap.Add(new AttributeMapping()
                    {
                        CsAttribute = row.Cell("A").Value?.ToString().ToNullableDecimal(),
                        EssAttribute = row.Cell("C").Value?.ToString().ToNullableDecimal()
                    });
                }

            }
            catch (FileNotFoundException)
            {
                ImportErrors.AddFileNotFoundImportError("SST Attribute Map.xlsx");
            }
            catch (Exception ex)
            {
                ImportErrors.AddImportError("Unable to Load File", "Unable to load Attribute Map.xlsx.", ex.Message);
            }
        }

        private void LoadRatesMap()
        {
            try
            {
                var connectionString = new OleDbConnectionStringBuilder()
                {
                    DataSource = Path.Combine(Properties.Settings.Default.ConversionDataFileDirectory, "SmartStop", "SST_Rates.accdb"),
                    Provider = "Microsoft.ACE.OLEDB.12.0"
                }.ConnectionString;

                using (OleDbConnection conn = new OleDbConnection(connectionString))
                {
                    if (ConnectionState.Closed == conn.State)
                    {
                        conn.Open();
                    }

                    var cmd = new OleDbCommand("Select * from Rates", conn); ;
                    var reader = cmd.ExecuteReader();

                    if (reader == null) return;

                    while (reader.Read())
                    {
                        var values = new object[reader.FieldCount];
                        reader.GetValues(values);

                        _ratesMap.Add(new RateMapping()
                        {
                            SiteNumber = values[0]?.ToString(),
                            AttributeID = values[1]?.ToString().ToNullableDecimal(),
                            Width = values[2]?.ToString().ToNullableDecimal(),
                            Depth = values[3]?.ToString().ToNullableDecimal(),
                            Rate = values[4]?.ToString().ToNullableDecimal(),
                        });
                    }

                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                ImportErrors.AddImportError("Unable to load Rate Map", "Unable to load Rate Map", ex.Message);
            }
        }

        private void LoadDelinquencyMap()
        {
            var state = SiteDataProvider.GetSiteState(_siteId);

            if (string.IsNullOrWhiteSpace(state))
            {
                ImportErrors.AddImportError("Site State Not Found", "Unable to load state for this site.", "");
                return;
            }

            try
            {
                var workbook = new XLWorkbook(Path.Combine(Properties.Settings.Default.ConversionDataFileDirectory, "SmartStop", "DelinquencyMapping.xlsx"));
                var ws = workbook.Worksheets.FirstOrDefault();

                foreach (var row in ws.RowsUsed().Where(r => r.RowNumber() > 1 && r.Cell("A").Value?.ToString() == state))
                {
                    _delinquencyMap.Add(new DelinquencyMapping()
                    {
                        Day = (row.Cell("B").Value?.ToString()).ToInt(),
                        Step = (row.Cell("C").Value?.ToString()).ToInt()
                    });
                }

            }
            catch (FileNotFoundException)
            {
                ImportErrors.AddFileNotFoundImportError("DelinquencyMapping.xlsx");
            }
            catch (Exception ex)
            {
                ImportErrors.AddImportError("Unable to Load File", "Unable to load DelinquencyMapping.xlsx.", ex.Message);
            }
        }

        protected override decimal? GetEssAttribute01(decimal? csAttributes)
        {
            var result = _attributeMap.FirstOrDefault(a => a.CsAttribute == csAttributes)?.EssAttribute;
            return result.HasValue ? result : 1;
        }

        protected override decimal? GetEssRate(decimal? csAttributes, decimal width, decimal depth, decimal? csRate)
        {
            var result = _ratesMap.FirstOrDefault(r => r.SiteNumber == _siteNumber && r.AttributeID == csAttributes && r.Width == width && r.Depth == depth)?.Rate;

            if (!result.HasValue)
            {
                ImportErrors.AddImportError("Invalid Dimensions", "Unable to match unit to the Rate map", $"CSAttribute01={csAttributes} Dimensions={width}x{depth}");
            }

            return result ?? csRate;
        }

        protected override int GetDelStep(DateTime? paidThruDate)
        {
            var daysLate = (DateTime.Today - paidThruDate)?.Days;

            var step = _delinquencyMap.Where(d => daysLate >= d.Day).OrderByDescending(d => d.Day).FirstOrDefault()?.Step;

            return step ?? 0;
        }

        #region Helper Classes

        private class AttributeMapping
        {
            public decimal? CsAttribute { get; set; }
            public decimal? EssAttribute { get; set; }
        }

        private class DelinquencyMapping
        {
            public int Day { get; set; }
            public int Step { get; set; }
        }

        private class RateMapping
        {
            public string SiteNumber { get; set; }
            public decimal? AttributeID { get; set; }
            public decimal? Width { get; set; }
            public decimal? Depth { get; set; }
            public decimal? Rate { get; set; }
        }

        #endregion
    }
}
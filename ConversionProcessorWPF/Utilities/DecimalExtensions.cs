﻿namespace ConversionProcessorWPF.Utilities
{
    public static class DecimalExtensions
    {
        /// <summary>
        /// Removes the trailing zeros from a decimal. (i.e. converts 1.00000 to 1, 2.20000 to 2.2, etc) 
        /// </summary>
        /// <param name="value"></param>
        /// <returns>Returns the same value without the trailing 0's.</returns>
        public static decimal Normalize(this decimal value)
        {
            return value / 1.00000000000000000000000000000m;
        }
    }
}

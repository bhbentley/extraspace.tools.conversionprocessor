﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConversionProcessorWPF.Properties;
using EXR.Email;

namespace ConversionProcessorWPF.Utilities
{
    internal static class Email
    {
        public static void Send(string body, string attachmentName)
        {
#if DEBUG 
            var mailToAddresses = Settings.Default.SummariesEmailToAddressTest.Cast<string>().Where(s => !string.IsNullOrWhiteSpace(s)).ToList();
#else
            var mailToAddresses = Settings.Default.SummariesEmailToAddress.Cast<string>().Where(s => !string.IsNullOrWhiteSpace(s)).ToList();
#endif

            if (!mailToAddresses.Any())
                throw new Exception("Unable to send email:  SummariesEmailToAddress has not been configured.");

            if (string.IsNullOrWhiteSpace(attachmentName) && !File.Exists(attachmentName))
                throw new Exception("Unable to send email:  Summaries file not found.");

            foreach (var mailToAddress in mailToAddresses)
            {
                Send(mailToAddress, body, attachmentName);
            }
        }

        public static void Send(string mailToAddress, string body, string attachmentName)
        {
            var emailServer = new EmailServer();

            emailServer.Send("noreply@extraspace.com", mailToAddress,
                             "Summary of Summaries", body, attachmentName, string.Empty);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace ConversionProcessorWPF.Utilities
{
    public static class OpenXmlExtensions
    {
        public static SheetData GetSheetData(this WorkbookPart workbookPart, string sheetName)
        {
            var sheet = workbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().FirstOrDefault(s => s.Name == sheetName);

            if (sheet != null)
            {
                string relationshipId = sheet.Id.Value;
                var worksheetPart = (WorksheetPart)workbookPart.GetPartById(relationshipId);
                var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

                return sheetData;
            }

            return null;
        }
        public static WorksheetPart GetWorksheetPart(this WorkbookPart workbookPart, string sheetName)
        {
            var sheet = workbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().FirstOrDefault(s => s.Name == sheetName);

            if (sheet != null)
            {
                string relationshipId = sheet.Id.Value;
                return (WorksheetPart)workbookPart.GetPartById(relationshipId);
            }

            return null;
        }

        public static Cell GetCell(this Row row, string column)
        {
            return row.Descendants<Cell>().FirstOrDefault(c => c.CellReference.InnerText.StartsWith(column, StringComparison.InvariantCultureIgnoreCase));
        }

        public static string GetCellValue(this Row row, string column, Dictionary<int, string> stringTable)
        {
            var cell = GetCell(row, column);

            if (cell == null) return null;

            string value = string.Empty;
            if (cell.DataType != null)
            {
                switch (cell.DataType.Value)
                {
                    case CellValues.String:
                        value = cell.CellValue?.Text;
                        break;

                    case CellValues.SharedString:
                        if (stringTable != null)
                        {
                            value = stringTable[int.Parse(cell.InnerText)];
                        }
                        break;

                    case CellValues.Boolean:
                        switch (value)
                        {
                            case "0":
                                value = "FALSE";
                                break;
                            default:
                                value = "TRUE";
                                break;
                        }
                        break;
                }
            }
            else
            {
                value = cell.CellValue?.Text;
            }
            return value;
        }

        public static decimal? GetDecimalCellValue(this Row row, string column, Dictionary<int, string> stringTable)
        {
            var value = GetCellValue(row, column, stringTable);
            return value.ToNullableDecimal();
        }

        public static bool? GetBooleanCellValue(this Row row, string column, Dictionary<int, string> stringTable)
        {
            var value = GetCellValue(row, column, stringTable);
            return value.ToNullableBoolean();
        }

        public static DateTime? GetDateCellValue(this Row row, string column, Dictionary<int, string> stringTable)
        {
            var value = GetCellValue(row, column, stringTable);

            double dateValueAsDouble;

            if (double.TryParse(value, out dateValueAsDouble))
            {
                return DateTime.FromOADate(dateValueAsDouble);
            }
            return null;
        }

        public static bool IsEmptyRow(this Row row)
        {
            if (row == null) return false;
            if (!row.ChildElements.Any()) return false;

            return !row.Elements<Cell>().Any(c => c.ChildElements.Any());  
        }

        public static Dictionary<int, string> GetSharedStrings(this SharedStringTablePart stringTable)
        {
            var result = new Dictionary<int, string>();
            if (stringTable == null) return null;

            using (var reader = OpenXmlReader.Create(stringTable))
            {
                var i = 0;
                while (reader.Read())
                {
                    if (reader.ElementType == typeof(SharedStringItem))
                    {
                        var sharedStringItem = reader.LoadCurrentElement() as SharedStringItem;
                        if (sharedStringItem != null)
                        {
                            result.Add(i, sharedStringItem.Text != null ? sharedStringItem.Text.Text : string.Empty);
                        }
                        i++;
                    }
                }
            }
            return result;
        }
    }
}

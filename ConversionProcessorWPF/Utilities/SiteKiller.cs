﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Oracle.DataAccess.Client;
using PropertyChanged;

namespace ConversionProcessorWPF.Utilities
{
    [ImplementPropertyChanged]
    public class SiteKiller
    {
        public string Output { get; set; }

        public void KillSite(long siteId)
        {
            try
            {
                Output = string.Empty;

                var killSiteTimer = new Stopwatch();
                var logId = CreateKillLog(siteId);

                SetFinancialCloseToFalse(siteId);

                Output += $"Starting SiteKill on SiteId: {siteId} - ConvId: {logId}\r\n\r\n";
                killSiteTimer.Start();
                foreach (var proc in GetKillProcedureList())
                {
                    var procTimer = new Stopwatch();
                    using (var conn = OracleConnectionProvider.GetConnection())
                    {
                        var cmd = conn.CreateCommand();
                        if (ConnectionState.Closed == conn.State)
                        {
                            conn.Open();
                        }
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = proc;
                        cmd.BindByName = true;
                        cmd.Parameters.Add(new OracleParameter("pn_site_id", OracleDbType.Int64, siteId,
                            ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pn_log_id", OracleDbType.Int64, logId,
                            ParameterDirection.Input));
                        Output += $"{proc}:\r\n";
                        procTimer.Start();
                        cmd.ExecuteNonQuery();
                        procTimer.Stop();
                        Output += $"\tFinished in {(procTimer.ElapsedMilliseconds / 1000.0D):0.000} seconds.\r\n";
                        conn.Close();
                    }
                }
                killSiteTimer.Stop();
                Output += $"\r\nFinished SiteKill on SiteId: {siteId} in {killSiteTimer.Elapsed.ToString(@"mm\:ss\.fff")}.";
            }
            catch (Exception ex)
            {
                Output += $"\r\nSiteKill Failed ex: {ex.GetFullExceptionMessage()}";
            }
        }

        private static IEnumerable<string> GetKillProcedureList()
        {
            var killList = new List<string>
            {
                "appl.site_killer.kill_yp",
                "appl.site_killer.kill_comp_rates",
                "appl.site_killer.kill_auctions",
                "appl.site_killer.kill_letters_and_leases",
                "appl.site_killer.kill_refunds",
                "appl.site_killer.kill_move_outs",
                "appl.site_killer.kill_autopay",
                "appl.site_killer.kill_tran_data",
                "appl.site_killer.kill_acct_notes",
                "appl.site_killer.kill_site_tasks",
                "appl.site_killer.kill_pcd_sites",
                "appl.site_killer.kill_discounts",
                "appl.site_killer.kill_tran_quotes",
                "appl.site_killer.kill_rentals",
                "appl.site_killer.kill_rates",
                "appl.site_killer.kill_units",
                "appl.site_killer.kill_act_phones",
                "appl.site_killer.kill_act_addr",
                "appl.site_killer.kill_contacts",
                "appl.site_killer.kill_ecomm",
                "appl.site_killer.kill_accounts"
            };

            return killList;
        }

        private static long CreateKillLog(long siteId)
        {
            using (var conn = OracleConnectionProvider.GetConnection())
            {
                var cmd = conn.CreateCommand();
                if (ConnectionState.Closed == conn.State)
                {
                    conn.Open();
                }
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "appl.site_killer.create_kill_log";
                cmd.BindByName = true;
                cmd.Parameters.Add(new OracleParameter("pn_site_id", OracleDbType.Int64, siteId,
                    ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("retval", OracleDbType.Int64, null,
                    ParameterDirection.ReturnValue));
                cmd.ExecuteNonQuery();

                conn.Close();
                return long.Parse(cmd.Parameters["retval"].Value.ToString());
            }
        }

        private void SetFinancialCloseToFalse(long siteId)
        {
            using (var conn = OracleConnectionProvider.GetConnection())
            {
                var cmd = conn.CreateCommand();
                if (ConnectionState.Closed == conn.State)
                {
                    conn.Open();
                }
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "update org.org_site_proc set financial_close = 'N', updated_by = 11 where site_id = :pn_site_id";
                cmd.BindByName = true;
                cmd.Parameters.Add(new OracleParameter("pn_site_id", OracleDbType.Int64, siteId, ParameterDirection.Input));
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}
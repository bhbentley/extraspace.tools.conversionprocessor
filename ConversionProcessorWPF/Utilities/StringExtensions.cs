﻿using System;
using System.Globalization;

namespace ConversionProcessorWPF.Utilities
{
    public static class StringExtensions
    {
        public static short? ToNullableShort(this string value)
        {
            short result;
            if (short.TryParse(value, out result))
                return result;

            return null;
        }

        public static int ToInt(this string value)
        {
            int result;
            if (int.TryParse(value, out result))
                return result;

            return 0;
        }

        public static int? ToNullableInt(this string value)
        {
            int result;
            if (int.TryParse(value, out result))
                return result;

            return null;
        }

        public static long? ToNullableLong(this string value)
        {
            long result;
            if (long.TryParse(value, out result))
                return result;

            return null;
        }

        public static decimal? ToNullableDecimal(this string value)
        {
            decimal result;
            if (decimal.TryParse(value, NumberStyles.Currency, CultureInfo.CurrentCulture, out result))
                return result.Normalize();

            return null;
        }

        public static decimal ToDecimal(this string value)
        {
            decimal result;
            if (decimal.TryParse(value, NumberStyles.Currency, CultureInfo.CurrentCulture, out result))
                return result.Normalize();

            return 0;
        }

        public static bool? ToNullableBoolean(this string value)
        {
            bool result;
            if (bool.TryParse(value, out result))
                return result;

            return ("Y".Equals(value, StringComparison.OrdinalIgnoreCase) || "1".Equals(value, StringComparison.OrdinalIgnoreCase));
        }

        public static DateTime? ToNullableDate(this string value)
        {
            DateTime result;
            if (DateTime.TryParse(value, out result))
                return result;

            return null;
        }

        public static string RemoveApostrophes(this string value)
        {
            return value?.Replace("'", string.Empty);
        }
    }
}

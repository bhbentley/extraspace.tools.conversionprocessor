﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using ClosedXML.Excel;
using ConversionProcessorWPF.DataProviders;
using ConversionProcessorWPF.Properties;
using Oracle.DataAccess.Client;

namespace ConversionProcessorWPF.Utilities
{
    internal static class SummaryOfSummaries
    {
        private static readonly object ObjLock = new object();
        private static readonly string SummaryListFileName = Path.Combine(Settings.Default.SummariesDirectory, "Summaries.txt");
        private static readonly string SummariesOutputDirectory = Path.Combine(Settings.Default.SummariesDirectory, $"{DateTime.Now.ToString("yyyyMMdd")}");
        private static readonly string SummariesOutputFileName = Path.Combine(SummariesOutputDirectory, "SummaryOfSummaries.xlsx");

        private static readonly LimitedConcurrencyLevelTaskScheduler Lcts = new LimitedConcurrencyLevelTaskScheduler(Settings.Default.MaxThreads);
        private static readonly TaskFactory Factory = new TaskFactory(Lcts);

        internal static void AddToSummaries(string siteNumber)
        {
            lock (ObjLock)
            {
                if (!Directory.Exists(Settings.Default.SummariesDirectory))
                    Directory.CreateDirectory(Settings.Default.SummariesDirectory);

                File.AppendAllText(SummaryListFileName, siteNumber.Trim() + Environment.NewLine);
            }
        }

        internal static void ClearSummaries()
        {
            if (File.Exists(SummaryListFileName))
                File.Delete(SummaryListFileName);
        }

        internal static List<SiteInfo> GetSummarySites()
        {
            if (!File.Exists(SummaryListFileName))
                return new List<SiteInfo>();

            var siteNumbers = File.ReadAllLines(SummaryListFileName).Distinct();

            return siteNumbers.Select(SiteDataProvider.GetSiteInfo).ToList();
        }

        internal static void Generate(List<SiteInfo> sites)
        {
            if (!Directory.Exists(SummariesOutputDirectory))
                Directory.CreateDirectory(SummariesOutputDirectory);

            var summaryLists = new List<SummaryItemList>();

            var tasks = sites.Select(s => Factory.StartNew(() => Generate(s)).ContinueWith(t =>
            {
                lock (ObjLock)
                {
                    summaryLists.Add(t.Result);
                }
            }));

            Task.WaitAll(tasks.ToArray());

            SyncSummaryLists(summaryLists);
            OrganizeSummaryLists(summaryLists);
            ExportToFile(summaryLists);
            ClearSummaries();
            EmailSummaryFile(sites);
        }

        private static SummaryItemList Generate(SiteInfo site)
        {
            var result = new SummaryItemList() { SiteNumber = site.SiteNumber, SiteName = site.Name };
            using (var conn = OracleConnectionProvider.GetConnection())
            {
                var cmd = conn.CreateCommand();
                if (ConnectionState.Closed == conn.State)
                    conn.Open();

                cmd.CommandText = @"select * from table(MORPHEUS.POST_CONVERSION_BIN.GET_EXR_CONVERSION_INFO(PN_SITE_ID31 => 0, PN_SITE_ID40 => :PN_SITE_ID40))";
                cmd.BindByName = true;

                cmd.Parameters.Add(new OracleParameter("PN_SITE_ID40", OracleDbType.Int64, site.Id, ParameterDirection.Input));

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var values = new object[reader.FieldCount];
                    reader.GetValues(values);

                    result.Add(new SummaryItem()
                    {
                        Name = values[0].ToString(),
                        Value = values[1].ToString()
                    });
                }
                conn.Close();
            }

            return result;
        }

        internal static void SyncSummaryLists(List<SummaryItemList> summaryLists)
        {
            if (summaryLists == null || summaryLists.Count == 0) return;

            var nameList = summaryLists.SelectMany(l => l.GroupBy(i => i.Name))
                                                         .Select(g => new { Name = g.Key, NameCount = g.Count(i => i.Name == g.Key) })
                                                            .GroupBy(i => i.Name)
                                                            .Select(g => new { Name = g.Key, MaxCount = g.Max(i => i.NameCount) }).ToList();

            foreach (var summaryList in summaryLists)
            {
                var index = 0;
                foreach (var name in nameList)
                {
                    for (var i = 0; i < name.MaxCount; i++)
                    {
                        if (summaryList.Count <= index)
                            summaryList.Add(new SummaryItem() { Name = name.Name });
                        else if (summaryList[index].Name != name.Name)
                            summaryList.Insert(index, new SummaryItem() { Name = name.Name });
                        index++;
                    }
                }
            }
        }

        /// <summary>
        /// Moves the Insurance Provider and Insurance Integration items to after the Fee Total item and adds blank lines before each grouping.
        /// </summary>
        /// <param name="summaryLists"></param>
        internal static void OrganizeSummaryLists(List<SummaryItemList> summaryLists)
        {
            foreach (var summaryList in summaryLists)
            {
                // Move the insurance Items
                var feeTotalItem = summaryList.FirstOrDefault(i => i.Name == "Fee Total");
                if (feeTotalItem == null) return;
                summaryList.MoveInsuranceItem(summaryList.FirstOrDefault(i => i.Name == "Insurance Integration #"), feeTotalItem);
                summaryList.MoveInsuranceItem(summaryList.FirstOrDefault(i => i.Name == "Insurance Provider"), feeTotalItem);

                // Now add a blank item in between them
                summaryList.Insert(summaryList.IndexOf(feeTotalItem) + 1, new SummaryItem());

                summaryList.InsertBlankItemBefore("Unit Type");
                summaryList.InsertBlankItemBefore("Active Revenue Class");
                summaryList.InsertBlankItemBefore("Services");
                summaryList.InsertBlankItemBefore("GL Mapping");
            }
        }

        private static void MoveInsuranceItem(this SummaryItemList summaryList, SummaryItem insuranceItem, SummaryItem feeTotalItem)
        {
            if (insuranceItem == null) return;

            summaryList.Remove(insuranceItem);

            if (feeTotalItem != null)
                summaryList.Insert(summaryList.IndexOf(feeTotalItem) + 1, insuranceItem);
            else
                summaryList.Add(insuranceItem);
        }

        private static void InsertBlankItemBefore(this SummaryItemList summaryList, string itemName)
        {
            var unitTypeItem = summaryList.FirstOrDefault(i => i.Name == itemName);
            if (unitTypeItem != null)
                summaryList.Insert(summaryList.IndexOf(unitTypeItem), new SummaryItem());
        }

        private static void ExportToFile(List<SummaryItemList> summaryLists)
        {
            if (File.Exists(SummariesOutputFileName))
                File.Delete(SummariesOutputFileName);

            using (var wb = new XLWorkbook())
            using (var ws = wb.Worksheets.Add("Summary"))
            {

                var columns = new List<string>();
                columns.Add(string.Empty);
                columns.AddRange(summaryLists.Select(l => l.ColumnHeader).ToList());

                for (var i = 0; i < columns.Count; i++)
                {
                    var cell = ws.Cell(1, i + 1);
                    cell.Value = columns[i];
                    cell.Style.Font.Bold = true;
                    cell.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    cell.Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                    cell.DataType = XLCellValues.Text;
                }

                for (var i = 0; i < summaryLists.First().Count; i++)
                {
                    for (var j = 0; j < columns.Count; j++)
                    {
                        var cell = ws.Cell(i + 2, j + 1);
                        cell.Value = string.IsNullOrEmpty(columns[j]) ? summaryLists.First()[i].Name : summaryLists.First(l => l.ColumnHeader == columns[j])[i].Value;
                        cell.DataType = XLCellValues.Text;
                    }
                }

                ws.ColumnsUsed().AdjustToContents();
                foreach (var column in ws.ColumnsUsed())
                {
                    column.Width += 5;
                }
                wb.SaveAs(SummariesOutputFileName);
            }
        }

        private static void EmailSummaryFile(List<SiteInfo> sites)
        {
            try
            {
                var body = "Summary generated for the following sites:<br><br>" + string.Join("<br>", sites.Select(s => $"{s.SiteNumber} - {s.Name}"));
                Email.Send(body, SummariesOutputFileName);
            }
            catch (Exception ex)
            {

                throw new Exception("Summaries generated successfully.  However, the email did not send: " + ex.Message, ex);
            }
        }

        internal class SummaryItem
        {
            public string Name { get; set; }
            public string Value { get; set; }
        }

        internal class SummaryItemList : List<SummaryItem>
        {
            public string SiteNumber { get; set; }
            public string SiteName { get; set; }
            public string ColumnHeader => $"{SiteNumber} - {SiteName}";
        }
    }
}

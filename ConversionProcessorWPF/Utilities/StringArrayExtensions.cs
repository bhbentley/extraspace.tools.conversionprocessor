﻿using System;
using System.Diagnostics.Contracts;
using System.Globalization;

namespace ConversionProcessorWPF.Utilities
{
    public static class StringArrayExtensions
    {
        public static string GetValueAt(this string[] array, int index)
        {
            Contract.Requires(array != null);
            return array.Length > index ? array[index] : string.Empty;
        }
    }
}

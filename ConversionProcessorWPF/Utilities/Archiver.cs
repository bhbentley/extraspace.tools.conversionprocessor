﻿using System;
using System.Diagnostics;
using System.IO;
using ConversionProcessorWPF.Properties;

namespace ConversionProcessorWPF.Utilities
{
    internal static class Archiver
    {
        internal static void ArchiveData(string siteNumber, string dataPath)
        {
            var zipFileName = Path.Combine(Settings.Default.ArchiveDirectory, $"{DateTime.Now.ToString("yyyyMMdd")}/Archive{siteNumber}.7z");

            if (File.Exists(zipFileName))
                File.Delete(zipFileName);

            var p = new Process
            {
                StartInfo =
                {
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    FileName = "7za.exe",
                    Arguments = $"a \"{zipFileName}\" -p{GetPassword()} -mhe \"{dataPath}\"",
                    CreateNoWindow=true
                }
            };

            p.Start();
            // var output = p.StandardOutput.ReadToEnd();
            p.WaitForExit();

            try
            {
                // Delete files after they have been archived.
                if (Directory.Exists(dataPath))
                    Directory.Delete(dataPath, true);
            }
            catch (Exception ex)
            {
                throw new Exception($"Unable to delete data folder: {dataPath};  {ex.Message}");
            }
        }

        private static string GetPassword()
        {
            return "Extra!space"; // TODO: Replace this with logic to get the password that is controlled by Karl's team.
        }
    }
}

﻿using System.Reflection;

namespace ConversionProcessorWPF.Utilities
{
    public static class PropertyInfoExtensions
    {
        public static object GetCustomValue(this PropertyInfo prop, object obj)
        {
            var value = prop.GetValue(obj);
            if (value == null) return null;

            if (prop.PropertyType.IsBoolean())
            {
                return (bool)value ? "Y" : "N";
            }
            return value;
        }
    }
}

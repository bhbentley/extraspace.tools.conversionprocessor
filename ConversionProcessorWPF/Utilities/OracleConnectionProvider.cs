﻿using ConversionProcessorWPF.Properties;
using Oracle.DataAccess.Client;

namespace ConversionProcessorWPF.Utilities
{
    internal static class OracleConnectionProvider
    {
        public static string Password { private get; set; }
                         
        internal static OracleConnection GetConnection()
        {
            var csb = new OracleConnectionStringBuilder
            {
                UserID = Settings.Default.UserName,
                Password = Password,
                DataSource = Settings.Default.tnsName,
                MinPoolSize = Settings.Default.MaxThreads/2,
                MaxPoolSize = Settings.Default.MaxThreads,
                ConnectionLifeTime = 1,
                ConnectionTimeout = 3600,
                StatementCacheSize = 15
            };

            return new OracleConnection(csb.ConnectionString);
        }
    }
}

﻿using System;
using System.Text;

namespace ConversionProcessorWPF.Utilities
{
    public static class ExceptionExtensions
    {
        public static string GetFullExceptionMessage(this Exception ex)
        {
            var sb = new StringBuilder();
            sb.AppendLine("Exception: " + ex.Message);
            sb.AppendLine("Exception StackTrace: " + ex.StackTrace);

            if (ex.InnerException == null) return sb.ToString();

            var innerEx = ex.InnerException;
            while (innerEx.InnerException != null)
            {
                sb.AppendLine("InnerException: " + innerEx.Message);
                innerEx = innerEx.InnerException;
            }
            sb.AppendLine("InnerException StackTrace: " + innerEx.StackTrace);

            return sb.ToString();
        }
    }
}

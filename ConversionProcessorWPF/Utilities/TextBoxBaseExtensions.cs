﻿using System.Windows.Controls.Primitives;

namespace ConversionProcessorWPF.Utilities
{
    public static class TextBoxBaseExtensions
    {
        public static bool IsScrolledToBottom(this TextBoxBase textBox)
        {
            // get the vertical scroll position
            var dVer = textBox.VerticalOffset;

            //get the vertical size of the scrollable content area
            var dViewport = textBox.ViewportHeight;

            //get the vertical size of the visible content area
            var dExtent = textBox.ExtentHeight;

            return dVer + dViewport >= dExtent;
        }
    }
}

﻿namespace ConversionProcessorWPF.View.Commands
{
    public interface IAsyncCommand : IAsyncCommand<object>
    {
    }
}
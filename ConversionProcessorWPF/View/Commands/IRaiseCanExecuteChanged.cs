﻿namespace ConversionProcessorWPF.View.Commands
{
    public interface IRaiseCanExecuteChanged
    {
        void RaiseCanExecuteChanged();
    }
}
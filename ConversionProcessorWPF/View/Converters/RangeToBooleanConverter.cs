﻿using System;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.Windows.Data;

namespace ConversionProcessorWPF.View.Converters
{
    public class RangeToBooleanConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Contract.Assume(value != null);
            return (int)value == 1;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

    }
}

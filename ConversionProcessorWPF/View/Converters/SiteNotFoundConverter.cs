﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ConversionProcessorWPF.View.Converters
{
    public class SiteNotFoundConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((long)value > 0)
                return value;

            return "No Site ID Found";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

    }
}

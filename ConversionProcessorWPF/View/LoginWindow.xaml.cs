﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ConversionProcessorWPF.ViewModel;

namespace ConversionProcessorWPF.View
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        public LoginWindow(LoginViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }

        private void Password_PasswordChanged(object sender, RoutedEventArgs e)
        {
            var viewModel = DataContext as LoginViewModel;
            var passwordBox = sender as PasswordBox;
            if (viewModel != null && passwordBox != null)
            {
                viewModel.Password = passwordBox.Password;
            }
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}

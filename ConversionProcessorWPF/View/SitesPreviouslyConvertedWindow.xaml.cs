﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ConversionProcessorWPF.ViewModel;

namespace ConversionProcessorWPF.View
{
    /// <summary>
    /// Interaction logic for SitesPreviouslyConvertedWindow.xaml
    /// </summary>
    public partial class SitesPreviouslyConvertedWindow : Window
    {
        public SitesPreviouslyConvertedWindow(SitePreviouslyConvertedViewModel model)
        {
            DataContext = model;
            InitializeComponent();

            System.Drawing.Icon icon = System.Drawing.SystemIcons.Warning;
            BitmapSource bs = System.Windows.Interop.Imaging.CreateBitmapSourceFromHIcon(icon.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            WarningImage.Source = bs;
        }

        public bool Accepted { get; private set; } 

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            Accepted = true;
            this.Close();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

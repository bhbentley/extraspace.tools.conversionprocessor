﻿using System.Windows;
using System.Windows.Input;

namespace ConversionProcessorWPF.View
{
    /// <summary>
    /// Interaction logic for SummariesWindow.xaml
    /// </summary>
    public partial class SummariesWindow : Window
    {
        public SummariesWindow()
        {
            InitializeComponent();
        }

        private void Window_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }
    }
}

﻿using System.Diagnostics.Contracts;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Navigation;
using ConversionProcessorWPF.ViewModel;

namespace ConversionProcessorWPF.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly SelectSitesControl _selectSitesControl;
        private ResolveConflictsControl _resolveConflictsControl;
        private ResultsControl _resultsControl;

        public MainWindow()
        {
            this.DataContext = Model;
            InitializeComponent();
            _selectSitesControl = new SelectSitesControl(Model);
            NavigationFrame.Navigate(_selectSitesControl);
        }

        public SitesViewModel Model => _model ?? (_model = new SitesViewModel());
        private SitesViewModel _model;

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            Contract.Assume(NavigationFrame != null);
            if (ReferenceEquals(NavigationFrame.Content, _selectSitesControl))
            {
                if (!ValidateSitesWithPreviousData())
                    return;

                WaitCursor.Show();
                Model.UpdateSelectedSites();
                _resolveConflictsControl = new ResolveConflictsControl(Model);
                NavigationFrame.Navigate(_resolveConflictsControl);
            }
            else if (ReferenceEquals(NavigationFrame.Content, _resolveConflictsControl))
            {
                _resultsControl = new ResultsControl(Model);
                NavigationFrame.Navigate(_resultsControl);
            }
            else if (ReferenceEquals(NavigationFrame.Content, _resultsControl))
            {
                BackButton.IsEnabled = false;
                NextButton.IsEnabled = false;
                Model.ExportAll();
            }
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            Contract.Assume(NavigationFrame != null);
            if (NavigationFrame.CanGoBack)
                NavigationFrame.GoBack();
        }

        private void NavigationFrame_Navigated(object sender, NavigationEventArgs e)
        {
            Contract.Assume(NavigationFrame != null);
            Contract.Assume(NextButton != null);
            if (ReferenceEquals(NavigationFrame.Content, _selectSitesControl))
            {
                UserComboBox.IsEnabled = true;
                BackButton.IsEnabled = false;
                NextButton.Content = "Next";
                NextButton.SetBinding(IsEnabledProperty, "HasSelectedSitesAndSelectedUser");
                SiteKillerButton.Visibility = Visibility.Visible;
            }
            else if (ReferenceEquals(NavigationFrame.Content, _resolveConflictsControl))
            {
                UserComboBox.IsEnabled = false;
                BackButton.IsEnabled = true;
                NextButton.Content = "Next";
                NextButton.SetBinding(IsEnabledProperty, "IsValid");
                SiteKillerButton.Visibility = Visibility.Visible;
            }
            else
            {
                BindingOperations.ClearBinding(NextButton, IsEnabledProperty);
                NextButton.Content = "Convert";
                UserComboBox.IsEnabled = false;
                SiteKillerButton.Visibility = Visibility.Hidden;
            }
        }

        private void SiteKillerButton_Click(object sender, RoutedEventArgs e)
        {
            var siteKillerWindow = new SiteKillerWindow { Owner = this };
            siteKillerWindow.ShowDialog();
        }

        private void mainWindow_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }

        private void SOSButton_Click(object sender, RoutedEventArgs e)
        {
            var summariesWindow = new SummariesWindow { Owner = this };
            summariesWindow.ShowDialog();
        }

        private bool ValidateSitesWithPreviousData()
        {
            if (!Model.Sites.Any(s => s.HasPreviousData && s.Selected)) return true;

            var previouslyConvertedWindow = new SitesPreviouslyConvertedWindow(new SitePreviouslyConvertedViewModel(Model.Sites));
            previouslyConvertedWindow.ShowDialog();
            return previouslyConvertedWindow.Accepted;
        }
    }
}

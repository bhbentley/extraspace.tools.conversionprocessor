﻿using System.Diagnostics.Contracts;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using ConversionProcessorWPF.Utilities;
using ConversionProcessorWPF.ViewModel;

namespace ConversionProcessorWPF.View
{
    /// <summary>
    /// Interaction logic for ResultsControl.xaml
    /// </summary>
    public partial class ResultsControl : UserControl
    {
        public ResultsControl(SitesViewModel model)
        {
            Contract.Requires(model != null);

            this.DataContext = model;
            InitializeComponent();
        }

        private void OutputTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox == null) return;

            if (textBox.IsScrolledToBottom())
                textBox.ScrollToEnd();
        }
    }
}

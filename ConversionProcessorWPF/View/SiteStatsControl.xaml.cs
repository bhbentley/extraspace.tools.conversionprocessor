﻿using System.Windows;
using System.Windows.Controls;
using ConversionProcessorWPF.Utilities;

namespace ConversionProcessorWPF.View
{
    /// <summary>
    /// Interaction logic for StatisticsControl.xaml
    /// </summary>
    public partial class SiteStatsControl : UserControl
    {
        public SiteStatsControl()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox == null) return;

            if (textBox.IsScrolledToBottom())
                textBox.ScrollToEnd();
        }
      
    }
}

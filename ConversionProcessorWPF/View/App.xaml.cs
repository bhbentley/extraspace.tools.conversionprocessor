﻿using System;
using System.IO;
using System.Windows;
using ConversionProcessorWPF.Properties;
using ConversionProcessorWPF.ViewModel;

namespace ConversionProcessorWPF.View
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private readonly LoginWindow _loginWindow;

        public App()
            : base()
        {
            UpgradeUserSettings();

            var loginViewModel = new LoginViewModel();
            loginViewModel.LoginSucceeded += LoginViewModel_LoginSucceeded;
            _loginWindow = new LoginWindow(loginViewModel);
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            try
            {
                Resources.MergedDictionaries.Add(Application.LoadComponent(new Uri("PresentationFramework.Aero;V4.0.0.0;31bf3856ad364e35;component\\themes/aero.NormalColor.xaml", UriKind.Relative)) as ResourceDictionary);
            }
            catch (Exception)
            {
                // Do nothing
            }

            _loginWindow.Show();
        }

        private void LoginViewModel_LoginSucceeded(object sender, EventArgs e)
        {
            _loginWindow?.Hide();
            var mainWindow = new MainWindow();
            mainWindow.Show();
            _loginWindow?.Close();
        }

        /// <summary>
        /// If the version has changed, then this will upgrade the settings from the previous version.
        /// </summary>
        private static void UpgradeUserSettings()
        {
            if (!Settings.Default.UpdateSettings) return;

            Settings.Default.Upgrade();
            Settings.Default.UpdateSettings = false;
            Settings.Default.Save();
        }

        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            try
            {
                if (File.Exists("ErrorLog.txt"))
                    File.Delete("ErrorLog.txt");

                File.WriteAllText("ErrorLog.txt", e.Exception.ToString());
            }
            finally
            {
                MessageBox.Show("An unhandled exception just occurred: " + e.Exception.Message + "\r\n\r\nSee ErrorLog.txt for more details.", "Unhandled Exception", MessageBoxButton.OK, MessageBoxImage.Error);
                e.Handled = true;
            }
        }
    }
}

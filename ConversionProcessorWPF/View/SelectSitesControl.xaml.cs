﻿using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;
using System.Windows;
using System.Windows.Controls;
using ConversionProcessorWPF.ViewModel;

namespace ConversionProcessorWPF.View
{
    /// <summary>
    /// Interaction logic for SelectSitesControl.xaml
    /// </summary>
    public partial class SelectSitesControl : UserControl
    {
        private SitesViewModel Model { get; }

        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SelectSitesControl(SitesViewModel model)
        {
            Contract.Requires(model != null);

            Model = model;
            this.DataContext = model;
            InitializeComponent();
        }

        [ContractInvariantMethod]
        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "Required for code contracts.")]
        private void ObjectInvariant()
        {
            Contract.Invariant(Model != null);
        }

    }
}

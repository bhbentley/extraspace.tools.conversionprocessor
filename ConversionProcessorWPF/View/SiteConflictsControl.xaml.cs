﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ConversionProcessorWPF.Model;
using ConversionProcessorWPF.Utilities;
using ConversionProcessorWPF.View.Behaviors;
using ConversionProcessorWPF.View.Converters;
using ConversionProcessorWPF.ViewModel;

namespace ConversionProcessorWPF.View
{
    /// <summary>
    /// Interaction logic for SiteConflictsControl.xaml
    /// </summary>
    public partial class SiteConflictsControl : UserControl
    {
        public SiteConflictsControl()
        {
            InitializeComponent();
        }

        DataTemplate CreateTemplate(SiteViewModel dataContext)
        {
            var xaml =
                $@"<DataTemplate>
                    <DataTemplate.Resources>
                        <c:DateConverter x:Key=""dateConverter""/>
                        <c:RangeToBooleanConverter x:Key=""Conv"" />
                        <Style x:Key=""controlStyle"" TargetType=""{{x:Type FrameworkElement}}"">
                            <Style.Triggers>
                                <Trigger Property=""Validation.HasError"" Value=""True"">
                                    <Setter Property=""ToolTip"" Value=""{{Binding RelativeSource={{x:Static RelativeSource.Self}}, 
                                                                               Path=(Validation.Errors)[0].ErrorContent}}""/>
                                </Trigger>
                            </Style.Triggers>
                        </Style>
                        <Style x:Key=""checkBoxStyle"" TargetType=""{{x:Type CheckBox}}"" BasedOn=""{{StaticResource controlStyle}}"">
                            <Setter Property=""VerticalAlignment"" Value=""Stretch"" />
                            <Setter Property=""HorizontalAlignment"" Value=""Stretch"" />
                            <Setter Property=""Template"">
                                <Setter.Value>
                                    <ControlTemplate TargetType=""{{x:Type CheckBox}}"">
                                        <Grid Background=""Transparent"">
                                            <CheckBox IsChecked=""{{Binding RelativeSource={{RelativeSource TemplatedParent}}, Path=IsChecked, Mode=TwoWay}}"" 
                                                      IsThreeState=""{{TemplateBinding Property=IsThreeState}}"" 
                                                      HorizontalAlignment=""Center"" VerticalAlignment=""Center"" />
                                        </Grid>
                                    </ControlTemplate>
                                </Setter.Value>
                            </Setter>
                        </Style>
                        <Style x:Key=""textBoxStyle"" TargetType=""{{x:Type TextBox}}"" BasedOn=""{{StaticResource controlStyle}}"">
                            <Setter Property=""Background"" Value=""#FFE8ECF0"" /> 
                        </Style>
                    </DataTemplate.Resources>
                    <Grid>
                        <!--<Grid.RowDefinitions>
                            <RowDefinition Height=""30""/>
                            <RowDefinition Height=""*""/>
                        </Grid.RowDefinitions>-->
                        <!--<CheckBox Content=""Show Invalid Only"" Grid.Row=""0"" IsChecked=""{{Binding Path=DataContext.FilteringInvalid, RelativeSource={{RelativeSource AncestorType=UserControl}}}}"" Margin=""5,5,0,0"" HorizontalAlignment=""Left"" VerticalAlignment=""Top""/>-->
                        <TabControl SelectedIndex=""{{Binding Path=DataContext.SelectedTabIndex, RelativeSource={{RelativeSource AncestorType=UserControl}}}}"">
                            <TabItem Header=""Log"">
                                <TabItem.Style>
                                    <Style TargetType=""TabItem"">
                                        <Setter Property=""FontWeight"" Value=""SemiBold""/>
                                    </Style>
                                </TabItem.Style>
                                <local:ErrorLogControl  DataContext=""{{Binding Path=DataContext, RelativeSource={{RelativeSource AncestorType=UserControl}}}}""/>
                            </TabItem>
                            {(dataContext.Site.Units.IsValid ? string.Empty : GetTabItemXaml<Unit>("Units", "Units"))}
                            {(dataContext.Site.Accounts.IsValid ? string.Empty : GetTabItemXaml<Account>("Accounts", "Accounts"))}
                            {(dataContext.Site.Contacts.IsValid ? string.Empty : GetTabItemXaml<Contact>("Contacts", "Contacts"))}
                            {(dataContext.Site.Addresses.IsValid ? string.Empty : GetTabItemXaml<Address>("Addresses", "Addresses"))}
                            {(dataContext.Site.Phones.IsValid ? string.Empty : GetTabItemXaml<Phone>("Phones", "Phones"))}
                            {(dataContext.Site.Notes.IsValid ? string.Empty : GetTabItemXaml<Note>("Notes", "Notes"))}
                            {(dataContext.Site.Rentals.IsValid ? string.Empty : GetTabItemXaml<Rental>("Rentals", "Rentals"))}
                            {(dataContext.Site.Pcds.IsValid ? string.Empty : GetTabItemXaml<Pcd>("PCD", "Pcds"))}
                            {(dataContext.Site.AutoPays.IsValid ? string.Empty : GetTabItemXaml<AutoPay>("AutoPays", "AutoPays"))}
                        </TabControl>
                    </Grid>
                </DataTemplate>";

            var context = new ParserContext();

            context.XamlTypeMapper = new XamlTypeMapper(new string[0]);
            // ReSharper disable AssignNullToNotNullAttribute
            context.XamlTypeMapper.AddMappingProcessingInstruction("behaviors", typeof(TextBoxInputBehavior).Namespace, typeof(TextBoxInputBehavior).Assembly.FullName);
            context.XamlTypeMapper.AddMappingProcessingInstruction("c", typeof(DateConverter).Namespace, typeof(DateConverter).Assembly.FullName);
            context.XamlTypeMapper.AddMappingProcessingInstruction("local", typeof(ErrorLogControl).Namespace, typeof(ErrorLogControl).Assembly.FullName);

            context.XmlnsDictionary.Add("", "http://schemas.microsoft.com/winfx/2006/xaml/presentation");
            context.XmlnsDictionary.Add("x", "http://schemas.microsoft.com/winfx/2006/xaml");
            context.XmlnsDictionary.Add("i", "http://schemas.microsoft.com/expression/2010/interactivity");
            context.XmlnsDictionary.Add("behaviors", "behaviors");
            context.XmlnsDictionary.Add("c", "c");
            context.XmlnsDictionary.Add("local", "local");

            var template = (DataTemplate)XamlReader.Parse(xaml, context);
            return template;
        }

        private string GetTabItemXaml<T>(string header, string bindingProperty) where T : CLG_Object
        {
            return string.Format(@"<TabItem Header=""{0}"" >
                                        <TabItem.Style>
                                            <Style TargetType=""TabItem"">
                                                <Setter Property=""FontWeight"" Value=""SemiBold""/>
                                                <Style.Triggers>
                                                    <DataTrigger Binding=""{{Binding Path=DataContext.Site.{1}.IsValid, RelativeSource={{RelativeSource AncestorType=UserControl}}}}""  Value=""False"">
                                                        <Setter Property=""Foreground"" Value=""Red"" />
                                                    </DataTrigger>
                                                </Style.Triggers>
                                            </Style>
                                        </TabItem.Style>
                                        <Grid>
                                            <Grid.RowDefinitions>
                                                <RowDefinition Height=""*""/>
                                                <RowDefinition Height=""18""/>
                                            </Grid.RowDefinitions>
                                            <DataGrid x:Name=""{1}Grid"" Grid.Row=""0"" ItemsSource=""{{Binding Path=DataContext.{1}, RelativeSource={{RelativeSource AncestorType=UserControl}}}}"" Margin=""-4"" CanUserDeleteRows=""False"">
                                                <DataGrid.Columns>
                                                    {2}
                                                </DataGrid.Columns>
                                            </DataGrid> 
                                            <StackPanel Grid.Row=""1"" Orientation=""Horizontal"" Margin=""5,4,0,0"">
                                                <TextBlock Text=""{{Binding Path=DataContext.Site.{1}.InvalidCount, RelativeSource={{RelativeSource AncestorType=UserControl}}}}"" ></TextBlock>
                                                <TextBlock>
                                                    <TextBlock.Style>
                                                        <Style TargetType=""{{x:Type TextBlock}}"">
                                                            <Setter Property=""Text"" Value="" Items"" />
                                                            <Style.Triggers>
                                                                <DataTrigger  Binding=""{{Binding Path=DataContext.Site.{1}.InvalidCount, RelativeSource={{RelativeSource AncestorType=UserControl}}, Converter={{StaticResource Conv}}}}"" Value=""True"">
                                                                    <Setter Property=""Text"" Value="" Item"" />
                                                                </DataTrigger>
                                                            </Style.Triggers>
                                                        </Style>
                                                    </TextBlock.Style>
                                                </TextBlock>
                                            </StackPanel>
                                        </Grid>
                                   </TabItem>", header, bindingProperty, GetColumnsXaml<T>());
        }

        private string GetColumnsXaml<T>() where T : CLG_Object
        {
            var result = string.Empty;
            Type entityType = typeof(T);
            var properties = entityType.GetProperties().Where(p => (p.GetCustomAttributes(typeof(DisplayAttribute), true)).Any())
                                                       .OrderBy(p => ((DisplayAttribute)p.GetCustomAttributes(typeof(DisplayAttribute), true).First()).Order);

            foreach (var prop in properties)
            {
                var displayAttribute = prop.GetCustomAttributes(typeof(DisplayAttribute), true).FirstOrDefault() as DisplayAttribute;

                if (displayAttribute == null)
                    continue;

                var readOnlyAttribute = prop.GetCustomAttributes(typeof(ReadOnlyAttribute), true).FirstOrDefault() as ReadOnlyAttribute;

                if (readOnlyAttribute != null && readOnlyAttribute.IsReadOnly)
                {
                    if (prop.PropertyType.IsDate())
                        result += GetReadOnlyDateColumnXaml(prop.Name, displayAttribute.Name);
                    else if (prop.PropertyType.IsNumeric())
                        result += GetReadOnlyNumericColumnXaml(prop.Name, displayAttribute.Name);
                    else if (prop.PropertyType.IsBoolean())
                        result += GetReadOnlyCheckBoxColumnXaml(prop.Name, displayAttribute.Name);
                    else
                        result += GetReadOnlyTextColumnXaml(prop.Name, displayAttribute.Name);
                }
                else
                {
                    if (prop.PropertyType.IsDate())
                        result += GetDateColumnXaml(prop.Name, displayAttribute.Name);
                    else if (prop.PropertyType.IsNumeric())
                        result += GetNumericColumnXaml(prop.Name, displayAttribute.Name, prop.PropertyType.IsDecimal());
                    else if (prop.PropertyType.IsBoolean())
                        result += GetCheckBoxColumnXaml(prop.Name, displayAttribute.Name);
                    else
                        result += GetTextColumnXaml(prop.Name, displayAttribute.Name);
                }
            }

            return result;
        }

        private string GetTextColumnXaml(string propertyName, string columnName)
        {
            return string.Format(
                    @"<DataGridTemplateColumn Header=""{1}"" SortMemberPath=""{0}"">
                        <DataGridTemplateColumn.CellTemplate>
                            <DataTemplate>
                                <AdornerDecorator>
                                    <TextBlock Text=""{{Binding {0}, ValidatesOnDataErrors=True, ValidatesOnNotifyDataErrors=True}}"" Style=""{{StaticResource controlStyle}}""/>
                                </AdornerDecorator>
                            </DataTemplate>
                        </DataGridTemplateColumn.CellTemplate>
                        <DataGridTemplateColumn.CellEditingTemplate>
                            <DataTemplate>
                                <Grid FocusManager.FocusedElement=""{{Binding ElementName={0}TextBox}}"">
                                    <TextBox Name=""{0}TextBox"" Text=""{{Binding {0}, UpdateSourceTrigger=LostFocus}}"" Style=""{{StaticResource textBoxStyle}}""/>
                                </Grid>
                            </DataTemplate>
                        </DataGridTemplateColumn.CellEditingTemplate>
                    </DataGridTemplateColumn>"
                      , propertyName, columnName);
        }

        private string GetReadOnlyTextColumnXaml(string propertyName, string columnName)
        {
            return string.Format(
                    @"<DataGridTemplateColumn Header=""{1}"" SortMemberPath=""{0}"" IsReadOnly=""True"">
                        <DataGridTemplateColumn.CellTemplate>
                            <DataTemplate>
                                <AdornerDecorator>
                                    <TextBlock Text=""{{Binding {0}, Mode=OneWay}}"" Style=""{{StaticResource controlStyle}}""/>
                                </AdornerDecorator>
                            </DataTemplate>
                        </DataGridTemplateColumn.CellTemplate>
                    </DataGridTemplateColumn>"
                      , propertyName, columnName);
        }

        private string GetDateColumnXaml(string propertyName, string columnName)
        {
            return string.Format(
                    @"<DataGridTemplateColumn Header=""{1}"" SortMemberPath=""{0}"">
                        <DataGridTemplateColumn.CellTemplate>
                            <DataTemplate>
                                <AdornerDecorator>
                                    <TextBlock Text=""{{Binding {0}, ValidatesOnDataErrors=True, ValidatesOnNotifyDataErrors=True, StringFormat=\{{0:MM/dd/yyyy\}}, Converter={{StaticResource dateConverter}}}}"" Style=""{{StaticResource controlStyle}}""/>
                                </AdornerDecorator>
                            </DataTemplate>
                        </DataGridTemplateColumn.CellTemplate>
                        <DataGridTemplateColumn.CellEditingTemplate>
                            <DataTemplate>
                                <Grid FocusManager.FocusedElement=""{{Binding ElementName={0}DatePicker}}"">
                                    <DatePicker Name=""{0}DatePicker"" SelectedDate=""{{Binding {0}, UpdateSourceTrigger=PropertyChanged}}"" Style=""{{StaticResource controlStyle}}"" />
                                </Grid>
                            </DataTemplate>
                        </DataGridTemplateColumn.CellEditingTemplate>
                    </DataGridTemplateColumn>"
                      , propertyName, columnName);
        }

        private string GetReadOnlyDateColumnXaml(string propertyName, string columnName)
        {
            return string.Format(
                    @"<DataGridTemplateColumn Header=""{1}"" SortMemberPath=""{0}"" IsReadOnly=""True"">
                        <DataGridTemplateColumn.CellTemplate>
                            <DataTemplate>
                                <AdornerDecorator>
                                    <TextBlock Text=""{{Binding {0}, StringFormat=\{{0:MM/dd/yyyy\}}, Converter={{StaticResource dateConverter}}, Mode=OneWay}}"" Style=""{{StaticResource controlStyle}}""/>
                                </AdornerDecorator>
                            </DataTemplate>
                        </DataGridTemplateColumn.CellTemplate>
                    </DataGridTemplateColumn>"
                      , propertyName, columnName);
        }

        private string GetNumericColumnXaml(string propertyName, string columnName, bool isDecimal)
        {
            return string.Format(
                    @"<DataGridTemplateColumn Header=""{1}"" SortMemberPath=""{0}"">
                        <DataGridTemplateColumn.CellTemplate>
                            <DataTemplate>
                                <AdornerDecorator>
                                    <TextBlock Text=""{{Binding {0}, ValidatesOnDataErrors=True, ValidatesOnNotifyDataErrors=True}}"" Style=""{{StaticResource controlStyle}}"" TextAlignment=""Right""/>
                                </AdornerDecorator>
                            </DataTemplate>
                        </DataGridTemplateColumn.CellTemplate>
                        <DataGridTemplateColumn.CellEditingTemplate>
                            <DataTemplate>
                                <Grid FocusManager.FocusedElement=""{{Binding ElementName={0}TextBox}}"">
                                    <TextBox Name=""{0}TextBox"" Text=""{{Binding {0}, UpdateSourceTrigger=LostFocus, TargetNullValue=''}}"" Style=""{{StaticResource textBoxStyle}}"">
                                         <i:Interaction.Behaviors>
                                                <behaviors:TextBoxInputBehavior InputMode=""{2}"" />
                                         </i:Interaction.Behaviors>
                                    </TextBox>
                                </Grid>
                            </DataTemplate>
                        </DataGridTemplateColumn.CellEditingTemplate>
                    </DataGridTemplateColumn>"
                    , propertyName, columnName, isDecimal ? "DecimalInput" : "DigitInput");
        }

        private string GetReadOnlyNumericColumnXaml(string propertyName, string columnName)
        {
            return string.Format(
                    @"<DataGridTemplateColumn Header=""{1}"" SortMemberPath=""{0}"" IsReadOnly=""True"">
                        <DataGridTemplateColumn.CellTemplate>
                            <DataTemplate>
                                <AdornerDecorator>
                                    <TextBlock Text=""{{Binding {0}, Mode=OneWay}}"" Style=""{{StaticResource controlStyle}}"" TextAlignment=""Right""/>
                                </AdornerDecorator>
                            </DataTemplate>
                        </DataGridTemplateColumn.CellTemplate>
                    </DataGridTemplateColumn>"
                      , propertyName, columnName);
        }

        private string GetCheckBoxColumnXaml(string propertyName, string columnName)
        {
            return string.Format(
                    @"<DataGridTemplateColumn Header=""{1}"" SortMemberPath=""{0}"">
                        <DataGridTemplateColumn.CellTemplate>
                            <DataTemplate>
                                <AdornerDecorator>
                                    <CheckBox IsChecked=""{{Binding {0}, UpdateSourceTrigger=PropertyChanged, ValidatesOnDataErrors=True, ValidatesOnNotifyDataErrors=True}}"" Style=""{{StaticResource checkBoxStyle}}"" IsThreeState=""True"" />
                                </AdornerDecorator>
                            </DataTemplate>
                        </DataGridTemplateColumn.CellTemplate>
                    </DataGridTemplateColumn>"
                      , propertyName, columnName);
        }

        private string GetReadOnlyCheckBoxColumnXaml(string propertyName, string columnName)
        {
            return string.Format(
                    @"<DataGridTemplateColumn Header=""{1}"" SortMemberPath=""{0}"" IsReadOnly=""True"">
                        <DataGridTemplateColumn.CellTemplate>
                            <DataTemplate>
                                <AdornerDecorator>
                                    <CheckBox IsChecked=""{{Binding {0}, Mode=OneWay}}"" Style=""{{StaticResource checkBoxStyle}}"" IsThreeState=""True"" IsHitTestVisible=""False"" Focusable=""False""/>
                                </AdornerDecorator>
                            </DataTemplate>
                        </DataGridTemplateColumn.CellTemplate>
                    </DataGridTemplateColumn>"
                      , propertyName, columnName);
        }

        private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var dataContext = DataContext as SiteViewModel;
            if (dataContext == null) return;

            this.ContentTemplate = null;
            this.ContentTemplate = CreateTemplate(dataContext);

        }
    }
}

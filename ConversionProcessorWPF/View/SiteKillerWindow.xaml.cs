﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ConversionProcessorWPF.Utilities;

namespace ConversionProcessorWPF.View
{
    /// <summary>
    /// Interaction logic for SiteKillerWindow.xaml
    /// </summary>
    public partial class SiteKillerWindow : Window
    {
        public SiteKillerWindow()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox == null) return;

            if (textBox.IsScrolledToBottom())
                textBox.ScrollToEnd();
        }

        private void Window_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }
    }
}

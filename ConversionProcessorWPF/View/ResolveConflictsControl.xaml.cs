﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using ConversionProcessorWPF.Model;
using ConversionProcessorWPF.Utilities;
using ConversionProcessorWPF.View.Behaviors;
using ConversionProcessorWPF.View.Converters;
using ConversionProcessorWPF.ViewModel;

namespace ConversionProcessorWPF.View
{
    /// <summary>
    /// Interaction logic for ResultsControl.xaml
    /// </summary>
    public partial class ResolveConflictsControl : UserControl
    {
        public ResolveConflictsControl(SitesViewModel model)
        {
            Contract.Requires(model != null);

            this.DataContext = model;
            InitializeComponent();
        }
    }
}

﻿using System.Collections.Generic;
using ConversionProcessorWPF.Model;

namespace ConversionProcessorWPF.Export
{
    public interface IExporter
    {
        bool ExportRunning { get; set; }
        bool ExportFailed { get; set; }
        string Output { get; set; }
        void Export(long siteId, IEnumerable<ICLG_Collection> data, bool cstageOnly);
    }
}
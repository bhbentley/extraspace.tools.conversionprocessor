﻿using System.Collections.Generic;
using System.Data;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Threading.Tasks;
using ConversionProcessorWPF.Properties;
using ConversionProcessorWPF.Utilities;
using Oracle.DataAccess.Client;

namespace ConversionProcessorWPF.Export
{
    public static class DataBaseInsert
    {
        private static readonly LimitedConcurrencyLevelTaskScheduler Lcts =
            new LimitedConcurrencyLevelTaskScheduler(Settings.Default.MaxThreads);

        private static readonly TaskFactory Factory = new TaskFactory(Lcts);

        static DataBaseInsert()
        {
            var minThreads = Settings.Default.MaxThreads;
            ThreadPool.SetMinThreads(minThreads, minThreads);
        }

        public static void Insert(DataSet data)
        {
            Contract.Requires(data != null);

            UpdateCStage(data);

            var tasks = new List<Task>();

            foreach (DataTable table in data.Tables)
            {
                tasks.Add(Factory.StartNew(() => Insert(table)));
            }

            Task.WaitAll(tasks.ToArray());
        }

        private static void Insert(DataTable data)
        {
            using (var conn = DbConnections.GetOracleConnection())
            {
                if (ConnectionState.Closed == conn.State)
                {
                    conn.Open();
                }

                using (var bulkCopy = new OracleBulkCopy(conn))
                {
                    bulkCopy.DestinationTableName = data.TableName;
                    bulkCopy.WriteToServer(data);
                    bulkCopy.Close();
                    bulkCopy.Dispose();
                }
            }
        }

        private static void Insert(OracleParameter[] oracleParameters, string insertStatement)
        {
            using (var conn = DbConnections.GetOracleConnection())
            {
                var cmd = conn.CreateCommand();
                if (ConnectionState.Closed == conn.State)
                {
                    conn.Open();
                }
                cmd.CommandText = insertStatement;
                cmd.Parameters.AddRange(oracleParameters);
                cmd.BindByName = true;
                cmd.ArrayBindCount = cmd.Parameters[0].Size;
                cmd.ExecuteNonQuery();
            }
        }

        private static void UpdateCStage(DataSet data)
        {
            Contract.Requires(data != null);

            var currentCStage = GetCStage();

            foreach (DataTable table in data.Tables)
            {
                foreach (DataRow dr in table.Rows)
                {
                    dr["CSTAGE"] = currentCStage;
                }
            }
        }

        private static long GetCStage()
        {
            var cstage = new long();
            var sql = "appl.conversion_toolkit.generate_stagingid";

            using (var conn = DbConnections.GetOracleConnection())
            {
                var cmd = conn.CreateCommand();
                if (ConnectionState.Closed == conn.State)
                {
                    conn.Open();
                }

                cmd.BindByName = true;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = sql;

                var retval = new OracleParameter();

                retval.Direction = ParameterDirection.ReturnValue;
                retval.DbType = DbType.Int64;
                retval.ParameterName = "cstage";

                cmd.Parameters.Add(retval);

                cmd.ExecuteNonQuery();

                cstage = (long) cmd.Parameters["cstage"].Value;

                conn.Close();
                conn.Dispose();
            }

            return cstage;
        }
    }
}
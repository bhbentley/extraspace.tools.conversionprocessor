﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ConversionProcessorWPF.Model;
using ConversionProcessorWPF.Properties;
using ConversionProcessorWPF.Utilities;
using PropertyChanged;

namespace ConversionProcessorWPF.Export
{
    [ImplementPropertyChanged]
    [ContractClass(typeof(BaseExporterContracts))]
    public abstract class BaseExporter : IExporter
    {
        private readonly object _objLock = new object();

        static BaseExporter()
        {
            var minThreads = Settings.Default.MaxThreads;
            ThreadPool.SetMinThreads(minThreads, minThreads);
        }
        private static readonly LimitedConcurrencyLevelTaskScheduler Lcts = new LimitedConcurrencyLevelTaskScheduler(Settings.Default.MaxThreads);
        protected static readonly TaskFactory Factory = new TaskFactory(Lcts);

        public bool ExportRunning { get; set; }
        public bool ExportFailed { get; set; }
        public string Output { get; set; }

        public void Export(long siteId, IEnumerable<ICLG_Collection> data, bool cstageOnly)
        {
            Output = string.Empty;
            ExportRunning = true;
            ExportFailed = false;

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            try
            {
                var tasks = new List<Task>();
                var exportData = data.ToList();

                UpdateData(exportData);

                foreach (var export in exportData)
                {
                    tasks.Add(Factory.StartNew(() => Export(export)));
                }
                Task.WaitAll(tasks.ToArray());

                if (!cstageOnly)
                    RunPostExport(siteId);
            }
            catch (Exception ex)
            {
                ExportFailed = true;
                WriteToOutput("Export Failed: " + ex.GetFullExceptionMessage());
                throw;
            }
            finally
            {
                ExportRunning = false;
                stopwatch.Stop();
                WriteToOutput($"\r\nExport Completed in: {stopwatch.Elapsed.ToString(@"mm\:ss\.fff")}");
            }
        }

        protected abstract void UpdateData(IEnumerable<ICLG_Collection> data);

        protected abstract void Export(ICLG_Collection data);

        protected virtual void RunPostExport(long siteId) { }

        protected void WriteToOutput(string value)
        {
            lock (_objLock)
            {
                Output += value + Environment.NewLine;
            }
        }
    }

    [ContractClassFor(typeof(BaseExporter))]
    public abstract class BaseExporterContracts : BaseExporter
    {
        protected override void UpdateData(IEnumerable<ICLG_Collection> data)
        {
            Contract.Requires(data != null);
            throw new NotImplementedException();
        }

        protected override void Export(ICLG_Collection data)
        {
            throw new NotImplementedException();
        }
    }
}
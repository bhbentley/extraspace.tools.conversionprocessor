﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ConversionProcessorWPF.Model;
using ConversionProcessorWPF.Properties;
using ConversionProcessorWPF.Utilities;
using Oracle.DataAccess.Client;

namespace ConversionProcessorWPF.Export
{
    public class OracleExporter : BaseExporter
    {
        private bool _siphonCompleted;
        private decimal _cStage;

        protected override void UpdateData(IEnumerable<ICLG_Collection> data)
        {
            _cStage = GetCStage();
            WriteToOutput($"CStage: {_cStage}" + Environment.NewLine);

            foreach (var collection in data)
            {
                var prop = collection.GetItems().FirstOrDefault()?.GetType().GetProperties().FirstOrDefault(x => x.Name == "Cstage");
                foreach (var row in collection.GetItems())
                {
                    prop?.SetValue(row, _cStage);
                }
            }
        }

        private static OracleParameter[] BuildOracleParameters(List<CLG_Object> collection)
        {
            var retval = new List<OracleParameter>();
            var entityType = collection.FirstOrDefault();
            if (entityType == null)
                return retval.ToArray();

            var properties = entityType.GetType().GetProperties();
            var count = collection.Count;

            foreach (var prop in properties)
            {
                var dataColumnAttribute = prop.GetCustomAttributes(typeof(DataColumnAttribute), false).FirstOrDefault() as DataColumnAttribute;
                if (dataColumnAttribute == null) continue;

                retval.Add(new OracleParameter(dataColumnAttribute.ColumnName, prop.PropertyType.ToOracleDbType(), count, collection.Select(x => prop.GetCustomValue(x)).ToArray(), ParameterDirection.Input));
            }

            return retval.ToArray();
        }

        protected override void Export(ICLG_Collection data)
        {
            if (!data.GetItems().Any()) return;

            WriteToOutput($"Exporting {data.TableName} ...");

            using (var conn = OracleConnectionProvider.GetConnection())
            {
                var cmd = conn.CreateCommand();
                if (ConnectionState.Closed == conn.State)
                {
                    conn.Open();
                }
                cmd.CommandText = data.InsertStatement;
                cmd.Parameters.AddRange(BuildOracleParameters(data.GetItems()));
                cmd.BindByName = true;
                cmd.ArrayBindCount = data.GetItems().Count;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        private static decimal GetCStage()
        {
            decimal cstage;
            const string sql = "appl.conversion_toolkit.generate_stagingid";

            using (var conn = OracleConnectionProvider.GetConnection())
            {
                var cmd = conn.CreateCommand();
                if (ConnectionState.Closed == conn.State)
                {
                    conn.Open();
                }

                cmd.BindByName = true;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = sql;

                var retval = new OracleParameter
                {
                    Direction = ParameterDirection.ReturnValue,
                    DbType = DbType.Int64,
                    ParameterName = "cstage"
                };
                cmd.Parameters.Add(retval);
                cmd.ExecuteNonQuery();
                cstage = decimal.Parse(cmd.Parameters["cstage"].Value.ToString());
                conn.Close();
            }

            return cstage;
        }

        protected override void RunPostExport(long siteId)
        {
            WriteToOutput(Environment.NewLine);

            var startTime = DateTime.Now;

            RunSiphon(siteId);
            MonitorSiphon(siteId, startTime);
            UpdateRetail(siteId);

            ExportFailed = !string.IsNullOrEmpty(Output) && !Output.Contains("Commit WAS USED!!!!!!!!!!!!!!!!");

            WriteToOutput(ExportFailed ? "Export Failed!!" : "Export Succeeded!!");
        }

        private async void RunSiphon(long siteId)
        {
            _siphonCompleted = false;

            try
            {
                await Task.Factory.StartNew(() =>
                {
                    using (var conn = OracleConnectionProvider.GetConnection())
                    {
                        var cmd = conn.CreateCommand();
                        if (ConnectionState.Closed == conn.State)
                        {
                            conn.Open();
                        }
                        cmd.CommandText = @"BEGIN
                                        APPL.CONVERSION_TOOLKIT.CONVERT_SITE(
                                        SOURCEDATABASE => 'LOCAL',
                                        TARGETORGANIZATIONID => 1000,
                                        TARGETSITEID => :targetSiteId,
                                        SOURCESITEID => :sourceSiteId,
                                        USER_YOU_ID => :personId,
                                        SHOW_ALL_OUTPUT => TRUE,
                                        PRESTAGING_ID => :cStage,
                                        MAKEDATADISTINCT => FALSE,
                                        CC_OUTPUTEMAIL_TO => NULL,
                                        RETAIL_MAP_ID => 0,
                                        INSURANCE_MAP_ID => 0,
                                        PRELIENDELSTATUS => 9,
                                        SANITIZE_DATA => FALSE
                                      );
                                    END;";

                        cmd.Parameters.Add(new OracleParameter("cStage", OracleDbType.Decimal, _cStage, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("targetSiteId", OracleDbType.Int64, siteId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("sourceSiteId", OracleDbType.Int64, siteId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("personId", OracleDbType.Int64, Settings.Default.PersonId, ParameterDirection.Input));
                        cmd.BindByName = true;
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                });
            }
            catch (Exception ex)
            {
                WriteToOutput(ex.Message);
            }
            finally
            {
                _siphonCompleted = true;
            }
        }

        private void MonitorSiphon(long siteId, DateTime startTime)
        {
            try
            {

                long convId = 0;
                long maxDetailId = 0;

                Thread.Sleep(15000);

                do
                {
                    Thread.Sleep(2000);

                    if (convId == 0)
                    {
                        convId = GetConversionID(siteId, startTime);
                        if (convId == 0) continue;
                        WriteToOutput($"Conversion ID: {convId}" + Environment.NewLine);
                    }

                    maxDetailId = GetSiphonOutput(convId, maxDetailId);
                } while (!_siphonCompleted);

                Thread.Sleep(2000);
                GetSiphonOutput(convId, maxDetailId);

            }
            catch (Exception ex)
            {
                WriteToOutput(ex.Message);
            }
        }

        private long GetSiphonOutput(long convId, long maxDetailId)
        {
            using (var conn = OracleConnectionProvider.GetConnection())
            {
                var cmd = conn.CreateCommand();
                if (ConnectionState.Closed == conn.State)
                    conn.Open();

                cmd.CommandText = @"select detail_id, detail from cs.cs_conv_details where conv_id = :conv_id and detail_id > :detail_id 
                                    and detail_type <> 36 order by detail_id";
                cmd.BindByName = true;
                cmd.Parameters.Add(new OracleParameter("conv_id", OracleDbType.Int64, convId, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("detail_id", OracleDbType.Int64, maxDetailId, ParameterDirection.Input));

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var values = new object[reader.FieldCount];
                    reader.GetValues(values);

                    WriteToOutput(values[1].ToString());
                    var detailId = long.Parse(values[0].ToString());
                    if (detailId > maxDetailId)
                        maxDetailId = detailId;
                }

                conn.Close();
                return maxDetailId;
            }
        }

        private long GetConversionID(long siteId, DateTime startTime)
        {
            using (var conn = OracleConnectionProvider.GetConnection())
            {
                var cmd = conn.CreateCommand();
                if (ConnectionState.Closed == conn.State)
                {
                    conn.Open();
                }
                cmd.CommandText = @"select max(conv_id) from cs.cs_conv_log where conv_site_id = :site_id and conv_notes = 'SIPHON' and created >= :start_time";
                cmd.Parameters.Add(new OracleParameter("site_id", OracleDbType.Int64, siteId, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("start_time", OracleDbType.Date, startTime, ParameterDirection.Input));
                cmd.BindByName = true;
                var convId = cmd.ExecuteScalar()?.ToString();

                long result;
                long.TryParse(convId, out result);

                conn.Close();
                return result;
            }
        }

        private void UpdateRetail(long siteId)
        {
            WriteToOutput("\r\nUpdating Retail...\r\n");

            using (var conn = OracleConnectionProvider.GetConnection())
            {
                var cmd = conn.CreateCommand();
                if (ConnectionState.Closed == conn.State)
                {
                    conn.Open();
                }

                cmd.CommandText = @"INSERT INTO org.org_site_retail_objects
                                    (SELECT
                                      NULL AS retail_object_id
                                    , src.retail_class_id
                                    , ori.retail_id
                                    , src.tax_group_id
                                    , ori.suggested_price AS site_price
                                    , 0 AS qty
                                    , ori.qty_per AS reorder_qty
                                    , ori.qty_per AS reorder_at
                                    , SYSDATE AS last_inv_date
                                    , 'Y' AS active
                                    , SYSDATE AS created
                                    , :personId AS created_by
                                    , SYSDATE AS updated
                                    , :personId AS updated_by
                                    , NULL AS db_create
                                    , NULL AS db_update
                                    FROM org.org_objects_class ooc
                                    INNER JOIN org.org_site_retail_class src ON ooc.org_class_id = src.org_class_id AND src.site_id = :siteId
                                    INNER JOIN org.org_retail_items ori ON ooc.org_class_id = ori.org_class_id AND LOWER(ori.item_name) NOT LIKE 'zzz%'
                                    WHERE NOT EXISTS(
                                    SELECT 1 
                                    FROM org.org_site_retail_objects sro2
                                    WHERE sro2.retail_class_id = src.retail_class_id
                                    AND sro2.retail_id = ori.retail_id))";

                cmd.Parameters.Add(new OracleParameter("siteId", OracleDbType.Int64, siteId, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("personId", OracleDbType.Int64, Settings.Default.PersonId, ParameterDirection.Input));

                cmd.BindByName = true;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}
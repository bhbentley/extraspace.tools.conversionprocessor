﻿using System;
using System.Diagnostics.Contracts;
using System.Threading.Tasks;
using System.Windows;
using ConversionProcessorWPF.Properties;

namespace ConversionProcessorWPF.Export
{
    public class ExporterFactory
    {
        public IExporter GetExporter()
        {
            Contract.Ensures(Settings.Default.ExportType != null);
            try
            {
                return Activator.CreateInstance(Type.GetType(Settings.Default.ExportType)) as IExporter;
            }
            catch (Exception ex)
            {
                Task.Factory.StartNew(() => MessageBox.Show(ex.Message));
                throw;
            }
        }
    }
}